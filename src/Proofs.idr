module Proofs

import public Proofs.UIP
import public Proofs.Extensionality
import public Proofs.Congruence
import public Proofs.Void
import public Proofs.Unit
import public Proofs.Preorder
import public Proofs.Sigma
import public Proofs.Product

export infix 0 ≡

public export
(≡) : {a : Type} -> (e1, e2 : a) -> Type
(≡) = (===)


