module Proofs.UIP

public export
UIP : (0 p, q : x = y) -> p = q
UIP Refl Refl = Refl

public export
UIP' : (p : x ~=~ y) -> (q : x === y) -> p === q
UIP' Refl Refl = Refl

