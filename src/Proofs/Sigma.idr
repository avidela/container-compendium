module Proofs.Sigma

import public Data.Sigma
import Data.Ops
import Proofs.Congruence

public export
sigmalemma :
  (0 start, end : Type) ->
  (0 index1 : start -> Type) ->
  (0 index2 : end -> Type) ->
  (dpair : Σ start (\x => index1 x -> end)) ->
  (elem1 : start) ->
  (elem2 : index1 elem1 -> end) ->
  (prf : dpair === (elem1 ## elem2)) ->
  Σ (index1 (dpair.π1)) (\x => index2 (dpair.π2 x))
  === Σ (index1 elem1) (\x => index2 (elem2 x))
sigmalemma _ _ _ _ ((_ ## _)) a b Refl = Refl

public export
inj : (a ## a') === (b ## b') -> a === b
inj Refl = Refl

public export
inj2 : (a ## a') === (b ## b') -> a' ~=~ b'
inj2 Refl = Refl

public export
proj1 : x === (c ## d) -> x.π1 === c
proj1 Refl = Refl

public export
proj2 : x === (c ## d) -> x.π2 ~=~ d
proj2 Refl = Refl

public export
dpairUniq : {0 a : Type} -> {0 b : a -> Type} -> (x : Σ a b) -> (x.π1 ## x.π2) === x
dpairUniq ((f ## s)) = Refl

public export
record SigEq (a, b : Σ f s) where
  constructor MkSigEq
  fstEq : a.π1 === b.π1
  sndEq : a.π2 === replace {p = s} (sym fstEq) b.π2

export
0 sigEqToEq : SigEq a b -> a === b
sigEqToEq {a = _ ## _} {b = _ ## _} (MkSigEq Refl Refl) = Refl

