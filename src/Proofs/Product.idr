module Proofs.Product

import Data.Product

export
prodUniq : (x : a * b) -> x.π1 && x.π2 = x
prodUniq (p1 && p2) = Refl

