
## Pointful lenses

```idris
module Optics.Pointful
```

Pointful lenses derive from lenses using a series of isomorphisms:


This transformation works for Van-Laarhoven lenses but also for plain lenses:

This definition is great to manipulating lens computation with binders. Here is
an example.

The above can be adapted to dependent lenses but the shape might look familiar:

This is because dependent pointful lenses and [closed](../Data/Container/Morphism/Closed.idr.md)
lenses are the same! This also means that, for any dependent lens we have, we can
use its closed/pointful version to write morphisms with the DSl described above.

What is more, using our monads on containers, we can inherit the monadic binding
structure of the morphisms we are working in, to write complex programs.
