## Van Laarhoven lenses

The most successful representation of lenses must be the Van Laarhoven encoding
of lenses in Haskell. This representation makes use of advanced features of haskell
like impredicativity of a functor instance to represent a large family of lens-like datastructures. In our dependently-typed world, this
is nothing special, and we can achieve the same type signature easily:

```idris
module Optics.VanLaarhoven

import Data.Container
import Data.Container.Kleene
import Data.Container.Morphism
import Data.Category
import Proofs.Extensionality

0 RefF : Type -> Type -> Type
RefF a b = forall f. Functor f => (b -> f b) -> (a -> f a)
```

What makes this representation shine is how flexible it is when combined with
other kinds of optics, including prisms and traversals.


Like the previous definition of lenses, we can generalise the definition to support
_polymorphic updates_
```idris
0 LensFamily : (a, b, c, d : Type) -> Type
LensFamily a b c d =
  forall f. Functor f =>
  (c -> f d) -> a -> f b
```

This is taken from http://comonad.com/reader/2012/mirrored-lenses/.

## Correspondances between VLH lenses and dependent lenses

```idris
0 Traversal : (a, b, c, d : Type) -> Type
Traversal a b c d =
  forall f. Applicative f =>
  (c -> f d) -> a -> f b
```

```idris
interface Functor f => Pointed f where
  point : a -> f a

0 AffineTraversal : (a, b, c, d : Type) -> Type
AffineTraversal a b c d =
  forall f. Pointed f =>
  (c -> f d) -> a -> f b
```

```idris
0 MonadicTraversal : (a, b, c, d : Type) -> Type
MonadicTraversal a b c d =
  forall f. Monad f =>
  (c -> f d) -> a -> f b

monadicToDLens : MonadicTraversal a b c d -> c :- b =%> Star (a :- d)
monadicToDLens f = ?aa <! ?monadicToDLens_rhs
```

## Dependent VanLaarhoven lenses

Something we cannot do in Haskell easily is give this a dependent version, and we do so
by turning each pair of input-outputs into a container:

```
0 DVLH : (a, b : Container) -> Type
DVLH a b =
  forall f. Functor f =>
  ((x : a.req) -> f (a.res x)) -> ((x : b.req) -> f (b.res x))
```
To make things a bit more clear when proving properties of dependent van-laarhoven
lenses, I am going to use a version where all the arguments are explicit:

```idris
0 DVLH : (a, b : Container) -> Type
DVLH a b =
  (0 f : Type -> Type) -> Functor f ->
  ((x : a.req) -> f (a.res x)) -> ((x : b.req) -> f (b.res x))
```

As expected we can compose those dependen van-laarhoven lenses:

```idris
compose : DVLH a b -> DVLH b c -> DVLH a c
compose g1 g2 f c x = g2 f c (g1 f c x)
```

Just like their haskell counterpart, composition is just function composition.

Thanks to this fact, we know that dependent van-laarhoven lenses form a category
since function composition is associative and respects identities.

```idris
identity : DVLH a a
identity f c = id

VLHCat : Category Container
VLHCat = MkCategory
   DVLH
   (\cont => identity)
   compose
   (\_, _, lens => funExtDep0 $ \v1 => funExt $ \v2 => funExt $ \v3 => Refl)
   (\_, _, lens => funExtDep0 $ \v1 => funExt $ \v2 => funExt $ \v3 => Refl)
   (\_, _, _, _, f, g, h => funExtDep0 $ \v1 => funExt $ \v2 => funExt $ \v3 => Refl)
```


## Pointful

Another way to define Van-Laarhoven lenses is to use a return `Value` that
suspends the computation of the lens until it is composed with something else.

```idris
FValue : (Type -> Type) -> (s, t, r : Type) -> Type
FValue f s t r = (s -> f t) -> f r

```

With this we can now define lens as a function returning an `FValue`:

```idris
0 FLens : (s, t, a, b : Type) -> Type
FLens s t a b = forall f . Functor f => s -> FValue f a b t

DFValue : (Type -> Type) -> Container -> (r : Type) -> Type
DFValue f c r = ((x : c.req) -> f (c.res x)) -> f r

0 DFLens : (a, b : Container) -> Type
DFLens a b = forall f . (func : Functor f) => (x : a.req) -> DFValue f b (a.res x)

0 ParaDFLens : (a, p, b : Container) -> Type
ParaDFLens a p b = DFLens (a ⊗ p) b

```

The `FValue` definition relies on a functor to match the van-laarhoven
encoding, but we can also define `Value` for regular lenses using an isomorphism:

```idris
Value : (s, t, r : Type) -> Type
Value s t r = (s, t -> r)
```

This value represents the costate comonad

```idris
counit : Value x x r -> r
counit (v, f) = f v

comult : Value x x r -> Value x x (Value x x r)
comult (v, f) = (v, \xv => (xv, f))
```

Anyways, that's great, and well known, but a cool little fact is that we can make the `Value` dependent, which brings us to…

## Dependent Pointful lenses

In the definition of `Value` above, `s` and `t` refer to the right boundary (or codomain) of the lens. Making the lens dependent replaces those two types by a container. The product needs to become a Σ-type to appropriately carry the type dependency:

```idris
DValue : Container -> Type -> Type
DValue c r = Σ c.req (\x => c.res x -> r)
```

Once this is done, we can see that the dependent Value is in fact our well known
extension on containers!

```
Ex : Container -> Type -> Type
Ex c t = Σ c.req (\x => c.res x -> t)
```

Because the original motivation of this was to define lenses, we can use this new dependent
value to build "pointful" lenses with a suspended continuation:

```idris
Pointful : Container -> Container -> Type
Pointful c d = (x : c.req) -> DValue d (c.res x)
```
This structure is so interesting that it will get its own little chapter.
