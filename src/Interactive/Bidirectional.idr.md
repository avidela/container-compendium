<!-- idris
module Interactive.Bidirectional

import Data.Container
import Data.Container.Morphism
import Data.Container.Morphism.Closed
import Data.Container.Lift
import Data.Container.Kleene
import Data.Sigma
import Data.List

import Derive.Prelude

%language ElabReflection
%hide Language.Reflection.TTImp.Mode

-->
```toc
```
This section was first written as a blog post for the [cybercat institute](https://cybercat.institute/blog/), the version you see here goes into more details about why it works, rather than only showing _that it >

The core idea is an implementation of lenses-as-tactics to build a typechecker. Each inference rule is a tactic and in turn each tactic is represented as a lens. First we review the bidirectional rules for typech>

## Bidirectional: Up and down

The simply typed lambda calculus can be checked using a _bidirectional typechecking algorithm_, that is to say, each jugdement is one of two forms:
- $t ∈ A$ : A synthesizing rule, saying that from term $t$ we can produce, or deduce type $A$
- $A \ni t$: A checking rule, asserting that term $t$ should have type $A$

Now it remains to fit a rule for each term, first, we properly define our lambda calculus, first we need types:
```
ty := name | ty "->" ty | 1
```

That is, each type is either a variable name, or a function, or the unit type. We represent this with a simple data type.

```idris
data Ty = Var String | Function Ty Ty | One

%runElab derive "Ty" [Eq]

showTy : Bool -> Ty -> String
showTy parens (Var str) = str
showTy parens (Function f@(Function _ _) y) = showParens parens "\{showTy True f} -> \{showTy False y}"
showTy parens (Function x y) = showParens parens "\{showTy False x} -> \{showTy False y}"
showTy parens One = "1"

export
Show Ty where
  show = showTy False

Context : Type
Context = List (String, Ty)
```

<!-- idris
-- %runElab derive "Ty" [Eq]
-->

Now for the terms, we have either variable references, lambda expressions or function application:

```
stlc := stlc stc | λ name . stlc | name
```

However, we're missing types, which means we need to pick which one of the two judgement forms each term is. Thankfully we can fairly easily reason them out.

### App rule
In an application of the form `fn arg`, the term `fn` _must_ be a function type `->`, it would be a type error if it wasn't, therefore the term `fn` is synthesizable into `a -> b`. If we assume it's a function `a`

$$
\begin{prooftree}
\AxiomC{f $\in A \to B$}
\AxiomC{$A \ni$ x}
\BinaryInfC{f x $\in B$} APP
\end{prooftree}
$$

This rule however does not read top-to-bottom. Unlike other inference rules, reading from the premises down to the conclusion does not tell the story in the correct order. Instead one starts at the bottom-left wi>
- What does it take to synthesize the type of $\text{f x}$?
- Move top-left and read $\text{f} ∈ A \to B$, this means that we first need to obtain the type $A \to B$ for $\text{f}$
- Move top-right and read $A \ni \text{x}$, this means that we then need to check that $A$ accepts term $\text{x}$
- Then move _back down_ and read $B$, we've successfully synthesized $B$ from $\text{f x}$.

This dance clockwise around the inference line is where _bidirectionality_ comes from, we don't just read top to bottom, we read in both directions.

### Lambda rule


$$
\begin{prooftree}
\AxiomC{$\text{n} : A \vdash B \ni \text{t} $}
\UnaryInfC{$ A \to B \ni \lambda \text{n}. \text{t}$} LAM
\end{prooftree}
$$

- Start at the bottom-left, given $A \to B$.
- Move up, if by extending the context with $\text{n} : A$ if we can ensure $\text{t}$ has type $B$
- Move back down, then we can ensure that $\lambda\text{n}. \text{t}$ has type $A\to B$

### Var Rule

The `Var` rue does not delegate any work because it only does a context lookup.

$$
\begin{prooftree}
\AxiomC{}
\UnaryInfC{$t : A \vdash t \in A$} VAR
\end{prooftree}
$$


### Annotate

One can annotate a term with the type it is meant to have, we then check that it matches the type we are given and if so we can synthesize the type for it.

$$
\begin{prooftree}
\AxiomC{$A \ni x$}
\UnaryInfC{$t : A \in A$} \text{Annotate}
\end{prooftree}
$$

### Assume

Any type can accept a term of that type, the `Assume` rule makes sure what we expect is what we have.

$$
\begin{prooftree}
\AxiomC{$t \in B$}
\AxiomC{$A = B$}
\BinaryInfC{$A \ni t$} \text{Assume}
\end{prooftree}
$$

- Given a type $A$
- If we can synthesize type $B$ from term $\text{t}$
- and $A$ is equal to $B$
- Then $A$ accepts $\text{t}$

### Bidirectional Simply Typed Lambda Calculus

From this we defined the bidirectional simply typed lambda calculus
as a data type

```idris

data Mode = Synthesizable | Checkable

data Term : Mode -> Type where
  Variable : String -> Term Synthesizable
  App : (fn : Term Synthesizable) -> (arg : Term Checkable) -> Term Synthesizable
  Annotate : Ty -> Term Checkable -> Term Synthesizable
  Lam : (name : String) -> Term Checkable -> Term Checkable
  Assume : Term Synthesizable -> Term Checkable

Show (Term n) where
  showPrec prec (Variable str) = str
  showPrec prec (App fn arg) = showParens (prec == App) (showPrec (User 0) fn ++ " " ++ showPrec App arg)
  showPrec prec (Annotate ty tm) = showParens (prec > Open) (show tm ++ " : " ++ show ty)
  showPrec prec (Lam name x) = showParens (prec > Open) "λ\{name}. \{show x}"
  showPrec prec (Assume x) = showPrec prec x
```

The data type is indexed by the mode, either `Synthesizable` or `Checkable`.

## Bidirectional: Sideways

This summary of bidirectional typechecking offers the necessary background to understand how to implement them as lenses. In that scenario, lenses's bidirectional works _sideways_ converting questions to further questions, and specific answers to more general ones.

For bidirectional typechecking we have to categories of statements:
- Given this _trusted_ type, and looking at a piece of syntax. Can we ensure that it is a term of the given type?
- Looking at a piece of syntax, can we generated a trusted type for it?

In the first case, we are given an input type, in the context of a term, and we generate a witness of its truth, the question/answer pair looks like `Context × Term !> ()`. In the second case, we only know about a term but we need to produce a trusted type, therefore the question/answer pair looks like `Term !> Ty`

With this knowledge, we formulate questions and answers as their own data types. First we write the data necessary to ask both `check` and `synth` questions.

```idris
data Question
  = SynQ Context (Term Synthesizable)
  | CheckQ Context Ty (Term Checkable)
```

Each question will have a corresponding answer, answers to checking question are only
a confirmation that everything went well, answers to synthesizing questions contain
the type that was synthesized.

```idris
data Answer : Question -> Type where
  SynA : Ty -> Answer (SynQ ctx tm)
  CheckA : Answer (CheckQ ctx ty tm)
```

Using those two types we build the typechecking container with `Question` as queries
and `Answer` as responses.

```idris
Typecheck : Container
Typecheck = (x : Question) !> Answer x
```


A lens for an inference rule will take a question-answer pair and produce
0 or more delegate question-answer pairs. For example the `Var` rule does not delegate
any work, beyond checking the variable is in scope. So its type should be
`Typecheck =%> CUnit`. The `App` rule however does delegate two tasks,
synthesize the type of the function, and check the type of the argument. So its
type is `Typecheck =%> Typecheck ○ Typecheck`.

Because of this variable arity in the codomain, the typechecker will have type
`Typechecker =%> Star Typechecker` to capture both the fact that some rules delegate
nothing, and some delegate multiple tasks.

Additionally, we lift the output of the backward part with `Either String` to
handle errors. The type of the typechecking lens becomes:

```idris
typecheck : Either String • Typecheck  =%> Star Typecheck
```

Implementing it means to analyse the input and find out what question
is asked, and from that question, what further questions to ask.

```idris
typecheck = fromClosed $ MkClosed lamRules
  where
    lamRules : (x : Question) -> Σ (StarShp Typecheck) (\y => StarPos Typecheck y -> Either String (Answer x))
    -- Var rule
    lamRules (SynQ ctx (Variable str))
      = Done -- There are no further premises to solve
      ## case lookup str ctx of
             Just ty => (const (pure (SynA ty)))
             Nothing => (const (Left "Undeclared variable \{str}"))

    -- App rule
    lamRules (SynQ ctx (App fn arg))
      = More (SynQ ctx fn) -- first we synthesize the type of fn
          (\case { (SynA (Function a b)) =>
                    More (CheckQ ctx a arg) -- then we check the argument
                         (const Done)
                 ; y => Done
                 })
          ## \case (StarM (SynA (Function a b)) (StarM CheckA StarU)) =>
                       pure (SynA b)
                   (StarM (SynA t) _) =>
                       Left "Expecting \{show fn} to be a function, instead it has type \{show t}"

    -- Up rule
    lamRules (CheckQ ctx ty (Assume term))
     = singleton (SynQ ctx term) -- There is one premise to discharge
     ##
       (\case (StarM (SynA ty') _)
              => if ty == ty'
                    then pure CheckA
                    else Left """
                      Expecting \{show term} to have the type \{show ty}
                      instead I found it has type \{show ty'}
                      """)
    -- Down rule
    lamRules (SynQ ctx (Annotate ty val))
      = singleton (CheckQ ctx ty val) -- we need to check the type
      ## \(StarM CheckA StarU) => pure (SynA ty)
    -- Lambda rule
    lamRules (CheckQ ctx (Function a b) (Lam nm term))
      = singleton (CheckQ ((nm, a) :: ctx) b term) -- we need to check the type of the body with the argument in context
      ## \(StarM CheckA StarU) => pure CheckA
    lamRules _ = Done ## const (Left "oops")

```

Type checking is then an iterative process where a term is then split into smaller premises that need to be typechecked as well
