

<!-- idris
module Interactive.Tactics

import Data.Container
import Data.Container.Category
import Data.Category.Functor
import Data.Container.Descriptions.Maybe
import Data.Container.Morphism
import Data.Container.Morphism.Linear
import Data.Product
import Data.Sigma
import Data.Maybe
import Proofs.Sigma

import Decidable.Equality

import Data.Linear
import Data.Linear.LVect

%hide Prelude.(&&)

%default total
-->


## A Tactical System for Equations

We demonstrate the effectiveness containers as a framework for tactics by implementing tactics for a simple equational system with natural numbers as terms.

Numbers in this system can be built in three ways:

- We can directly build the number for `zero`.
- We can build the successor of a number given any other number.
- We can add two numbers together.

We represent those three cases via the constructors `Zero`, `Succ` and `(+)` for `Term`.

```idris
data Term : Type where
  Zero : Term
  Succ : Term -> Term
  (+) : Term -> Term -> Term
```

An equality of terms is given by the following data with values for every way to prove that two numbers are equal:

```idris
data Eq : Term -> Term -> Type where
```

- Every number is equal to itself.

```idris
  Refl : Eq t t
```
- Proofs of equalities are transitive, that is given $a = b$ and $b = c$ we get $a = c$.
```idris
  Trans : Eq r s -> Eq s t -> Eq r t
```
- Proofs are congruent with regard to the `Succ` constructor.
```idris
  Cong : Eq t s -> Eq (Succ t) (Succ s)
```
- Given two proofs of equality, their sum is also an equality.
```idris
  CPlus : Eq t s -> Eq x y -> Eq (t + x) (s + y)
```
- Proofs of equality are symmetric.
```idris
  Sym : Eq t s -> Eq s t
```
- Given three proofs of equality, their sum is associative.
```idris
  Assoc : Eq t1 t2 -> Eq r1 r2 -> Eq s1 s2 ->
          Eq ( t1 + (r1  + s1))
             ((t2 +  r2) + s2)
```

### Primitive Tactics

With the above definition we can write the container that represents an equational problem. An equational problem has pairs of terms as _questions_ and proofs of equality as _responses_.

```idris
EqProblem : Container
EqProblem = (t : Term * Term) !> uncurry Eq t
```

Using this container, we can build tactics that will attempt to resolve the problem, or break it down into subproblems. Starting easy, the `sym` tactic says that to solve a problem, it is enough to solve its symmetric.

```idris
sym : EqProblem =%> EqProblem
sym = swap <! (\_ => Sym)
```

The above is an example of a tactic that always succeeds, it can always be applied to any problem. But most commonly, tactics can fail if the problem does not have the right shape. For this we use the _Maybe_ monad on containers.

This is done for the `refl` tactic, this tactic attempts to apply the `Refl` proof to the problem, attempting to solve it immediately. Of course, this tactic only succeeds if both sides of the equations are the definitionally equal. We give the type for this tactic as a morphism into `Maybe End`
```
refl : EqProblem =%> MaybeAny End
```

To implement this tactic we need a couple of helper functions, first we need a semi-decidable property for `Term`.

```idris
eq : (t, s : Term) -> Maybe (t === s)
eq Zero Zero = Just Refl
eq (Succ x) (Succ y) = map (\w => cong Succ w) (eq x y)
eq (x + y) (a + b) = do Refl <- eq x a
                        Refl <- eq y b
                        Just Refl
eq _ _ = Nothing
```

Then we need some way to extract proofs of equality from the forward part.

```idris
isEq : (t, s : Term) -> Bool
isEq t s = isJust (eq t s)

decCheck : Term -> Term -> MaybeType ()
decCheck a b with (isEq a b)
  decCheck a b | False = Nothing
  decCheck a b | True = Just ()
```

Once the busywork is done we can write the tactic by running a test of equality on the
forward part, and then rebuild the proof in the backward part. Note that, thanks to the nature of the `Maybe` monad on containers, we only ever worry about the case where the two terms have a proof that they are equal.

```idris
refl : EqProblem =%> MaybeAny End
refl = (uncurry decCheck) <! (\v => backward v.π1 v.π2)
where
  backward : (x, y : Term) ->
             -- Can we replace this by `isJust`?
             Σ (IsTrue ((decCheck x y).ex1)) (\_ => ()) ->
             Eq x y
  backward x y z with (eq x y)
    backward x y z | Nothing = absurd z.π1
    backward x x z | (Just Refl) = Refl
```

The transitive property of equations can be used to build a tactic that will attempt to break down an equation $a = c$ into two equations $a = b$ and $b = c$ as long as
$b$ is provided in advance. If the two subproblems $a = b$ and $b = c$ can be resolved, then we can build the larger proof $a = c$ via transitivity. This is a prime example of a tactic that breaks down a problem into two subproblems.

```idris
trans : (term : Term) -> EqProblem =%> (EqProblem ⊗ EqProblem)
trans term =
    (\x => (x.π1 && term) && (term && x.π2)) <!
    (\_ => uncurry Trans)
```

The `assoc` tactic also breaks down an existing problem into subproblems but it can itself fail. This is because the associativity tactic can only work in a problem with the shape $a + (b + c) = (d + e) + f$. If that is the case, then the problem can be resolved if we obtains proofs $a = d$, $b = e$ and $c = f$.

The type of this tactic is then:

```idris
assoc : EqProblem =%> MaybeAny (EqProblem ⊗ (EqProblem ⊗ EqProblem))
assoc = isAssocCandidate <! isAssocSolution
```

Just like `refl` it requires a bit of setup to get going. This is because we first need to ensure the shape of the problem is appropriate for the tactic. If it is, we build evidence that we can recover in the backward part of the tactic that will rebuild the proof of the original problem using the solutions of each sub-problem.

First we write a semi-decidable procedure for checking that two terms have the appropriate shape to apply the associativity proof.

```idris
  where
    isAssoc : (t, s : Term) -> Maybe ((t1 : Term **  t2 : Term ** t3 : Term **
                                  (t1 + (t2 + t3) === t)) *
                                  (t1 : Term **  t2 : Term ** t3 : Term **
                                  (((t1 + t2) + t3) === s)))
    isAssoc (x + (y + z)) ((a + b) + c)
      = Just ((x ** y ** z ** Refl) && (a ** b ** c ** Refl))
    isAssoc _ _ = Nothing
```

With it we can build the forward part that will check if the given problem is a candidate for applying associativity.

```idris

    isAssocCandidate : Term * Term -> MaybeType ((Term * Term) * ((Term * Term) * (Term * Term)))
    isAssocCandidate x with (isAssoc x.π1 x.π2)
      isAssocCandidate x | Nothing = Nothing
      isAssocCandidate x | (Just ((t1 ** t2 ** t3 ** p) && (s1 ** s2 ** s3 ** q)))
        = Just ((t1 && s1) && ((t2 && s2) && (t3 && s3)))
```

The backward part will then reuse this piece of evidence to build back the proof that the two terms are equal via transitivity.

```idris
    isAssocSolution : (s : Term * Term) ->
      (MaybeCont ○ (EqProblem ⊗ (EqProblem ⊗ EqProblem))).res (isAssocCandidate s) ->
      (response EqProblem s)
    isAssocSolution s x with (isAssoc s.π1 s.π2)
      isAssocSolution s x | Nothing = absurd x.π1
      isAssocSolution (s && s') x | (Just ((t1 ** t2 ** t3 ** p) && (s1 ** s2 ** s3 ** q)))
        = replace {p = Eq s} q
        $ rewrite sym p in Assoc x.π2.π1 x.π2.π2.π1 x.π2.π2.π2
```


```idris
-- move this somewhere else, it's nothing to do with tactics
combineUnits : MaybeAny End ⊗ MaybeAny End =%> MaybeAny End
combineUnits = strength {x = End, y = MaybeAny End} ⨾ assocCont
  where
    assocCont : (MaybeAny (End ⊗ (MaybeAny End))) =%> MaybeAny End
    assocCont = let sw : End ⊗ (MaybeAny End) =%> (MaybeAny End) ⊗ End
                    sw = (swap {x = End, y = MaybeAny End})

                    str : ((MaybeAny End) ⊗ End) =%> MaybeAny (End ⊗ End)
                    str = strength {x = End, y = End}

                    mp : MaybeAny (End ⊗ (MaybeCont ○ End)) =%>
                          MaybeAny (MaybeAny (End ⊗ End))
                    mp = composeFunctor {a = MaybeCont
                                         , x = (End ⊗ (MaybeCont ○ End))
                                         , y = MaybeAny (End ⊗ End)}
                                         (sw ⨾ str)
                 in mp
                 ⨾ join {x = End ⊗ End}
                 ⨾ composeFunctor {a = MaybeCont, x = End ⊗ End, y = End} joinUnit

tripleRight : a ⊗ a =%> a -> a ⊗ (a ⊗ a) =%> a
tripleRight x = parallel (identity a) x ⨾ x

```

### User Defined Tactics

The rich monoidal structure on containers and their morphisms forms combinators that can be used to build larger tactics from smaller ones. For example, we can build
a tactic that will attempt to solve the following equation, and only this one: $(x + y) + z = x + (y + z)$, for all $x, y, z$.

This is done by applying the symmetry tactic, then following it up by the associativity tactic. The associativity tactic generates three sub-problems, so we need to solve each one of them we do this by attempting to apply the reflexivity tactic, assuming that we are dealing with three variables in the same order but with a different bracketing.

The following program implements the above procedure:

```idris
assocCompose : EqProblem =%> (MaybeCont ○ End)
assocCompose
    = sym
    ⨾ assoc
    ⨾ composeFunctor { a = MaybeCont }
            ((refl `parallel` (refl `parallel` refl))
                ⨾ tripleRight combineUnits)
    ⨾ join {x = End}
```

It might help to visualise the diagram that represents this computation

*Draw the diagram here*
