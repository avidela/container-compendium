## Tracking Error Locations
Parsing and error reporting are also two processes that ought to be related to each other, but the correspondance is hard to make formal. In this example, I show how to use the `Some` Monad on containers to collect partial failures of sub-processes and re-interpret them in a larger context.

### The `Some` relation on lists

We've seen how the List container with `#>` results in a type that is isomorphic to `All` and `○` is isomorphic to `Any`, but is there something in between? `All` ensures a predicate is true for all elements of a given list, and `Any` ensures exactly one fits the bill. But is there a way to say only _some_ of the elements satisfy the preducate and others do not? Well we can write this type first and then find out what it means:

<!-- idris
module Interactive.ErrorLocation

import Data.Product
import Data.Container
import Data.Container.Morphism
import Data.Container.Descriptions.List
import Data.Container.Descriptions.Maybe

import Data.Iso
import Data.List.Quantifiers
import Proofs
-->

```idris
data Some : (a -> Type) -> List a -> Type where
  End : {0 a : Type} -> {0 p : a -> Type} ->  Some p []
  Take : {0 a : Type} -> {0 p : a -> Type} -> {0 xs : List a} -> {0 x : a} -> p x -> Some p xs -> Some p (x :: xs)
  Drop : Some p xs -> Some p (x :: xs)
```

This data definition is surprisingly close to the definition of a _thinning_.

```
data (<=) : List a -> List a -> Type where
  None : [] <= xs
  Take : xs <= ys -> x :: xs <= x :: ys
  Drop : xs <= ys -> xs <= x :: ys
```

The difference being that a thinning does not carry values, only a "selection". But this is already helpful, as we can write our `Some` datatype using a thinning and the existing `All` relation:

```
Some' : (p : a -> Type) -> List a -> Type
Some' p ls = Σ (xs : List a) | xs <= ls * All p xs
```

In prose, it says that given a predicate and a list, we can create the subset of all values satisfying the predicate using a thinning.

### The `Some` Functor on Container

Regardless of how we define it, we still end up with a functor on containers defined by:

```idris
SomeC : Container -> Container
SomeC c = (xs : List c.req) !> Some c.res xs
```

This transforms an 'input/response' pair into "multiple inputs, some of which can fail in their responses"

### The truth behind `Some`

In the above, we've only worked with `Some` defined as a datatype in Idris, but previously,
we established a correspondance between containers using idris datatype definitions and
maps of containers using different monoidal products and actions. If we assume that every
map on container we can define in idris has a construction in terms of existing monoidal
products. What would it be for `Some`?

A good candidate would be `SomeC = AllC (MaybeC a)` since we have seen previously that
`AllC (MaybeAny a)` can also represent partial results. However those two types are not
equal. To see this, we attempt to implement the map `AllC (MaybeAnyIdris a)`. If they are the
same, this morphism should be equivalent to the identity.

```idris
attempt1 : AllC (MaybeAnyIdris a) =%> SomeC a
attempt1 = ?Problem <! ?attempt1_rhs_1
```

However, problems start right away, since the forward map must be of type
`List (Maybe a.req) -> List a.req` and there is no way to do this while keeping around
all the elements of the input list. For this to work, we need an identity on the
shapes. Ideally we want those signatures:

- `fwd : List a.req -> List a.req`
- `bwd : (xs : List a.req) -> All (?thing .  a.res) xs -> Some a.res xs`

Such that `All (?thing .  a.res) xs -> Some a.res xs` is also an identity. If we were to
find what `thing` is, then we could represent `SomeC` in terms of `AllC`. This is the
insight we were missing, because another way to understand `Some` is as a predicate
on all elements of a list, some of which might fail. This possiblity of failure can be
represented with `Maybe`. So if we can prove that `Some p xs` is the same as
`All (Maybe . p) xs` then we will have found a way to write `Some` in terms of `All`.

```idris
rebuild1 : (xs : List a) -> All (\x => Maybe (b x)) xs -> Some b xs
rebuild1 [] [] = End
rebuild1 (x :: xs) (Nothing :: z) = Drop (rebuild1 xs z)
rebuild1 (x :: xs) ((Just y) :: z) = Take y (rebuild1 xs z)

rebuild2 : (xs : List a) -> Some b xs -> All (\x => Maybe (b x)) xs
rebuild2 (x :: ls) (Take y z) = Just y :: rebuild2 ls z
rebuild2 (x :: ls) (Drop y) = Nothing :: rebuild2 ls y
rebuild2 [] End = []

rebuildPrf : {0 p : a -> Type} -> (xs : List a) -> (x : All (\x => Maybe (p x)) xs) ->
             rebuild2 xs (rebuild1 xs x) = x
rebuildPrf [] [] = Refl
rebuildPrf (x :: xs) (Nothing :: ys) = cong2 (::) Refl (rebuildPrf xs ys)
rebuildPrf (x :: xs) ((Just y) :: ys) = cong2 (::) Refl (rebuildPrf xs ys)

rebuildPrf2 : {0 p : a -> Type} -> (xs : List a) -> (x : Some (\arg => p arg) xs) -> rebuild1 xs (rebuild2 xs x) = x
rebuildPrf2 [] End = Refl
rebuildPrf2 (y :: xs) (Take x z) = cong (Take x) (rebuildPrf2 xs z)
rebuildPrf2 (y :: xs) (Drop x) = cong Drop (rebuildPrf2 xs x)

SomeAllIso : {xs : _} -> Some p xs `Iso` Quantifiers.All.All (Maybe . p) xs
SomeAllIso = MkIso
    (rebuild2 xs)
    (rebuild1 xs)
    (rebuildPrf xs)
    (rebuildPrf2 xs)
```
From this we can also build the isomorphism with the container map `SomeC`.

```idris
allToSome : AllC ((x : a) !> Maybe (b x)) =%> SomeC ((!>) a b)
allToSome = id <! rebuild2

someToAll : SomeC ((!>) a b) =%> AllC ((x : a) !> Maybe (b x))
someToAll = id <! rebuild1
```

The isomorphism is a success. We now just need to find a monoidal operation that performs
something that is isomorphic to `List a` on the shapes and isomorphic to `Maybe a` on the position.
The dirichelet product is exactly this monoidal product:

```idris
SomeC' : Container -> Container
SomeC' c = AllC (End * c)
```

Knowing that `AllC _` is a monad and `1 + _` is a monad, it would be plausible that `1 * _` is also
a monad.
However, when we try, we see that it is not, in particular, we cannot implement a unit for `1 * _`.

```idris
dirUnit : a =%> End * a
dirUnit = (() &&) <! (\x, y => ?oops)
```

The hole `oops` cannot be inhabited, this is because we cannot rebuild a value of `a.res` when
`y` is a left injection. We see that if we display the context at that hole.

```
 0 a : Container
   x : a .req
   y : () + a .res x
------------------------------
oops : a .res x
```

We've now established that `Some` is a useful definition. It allows to describe processes
that are split appart into sub-processes, but each of those sub-processes might fail in
some way. We were able to use it in our program in a similar way as `All` by leveraging
its functor definition. It also has a `join` operation that combines nested partial results
into a single one. The natural question is: Is it a monad? Surprisingly, it is not! And that
is because it does not have a _unit_: `a =%> SomeC a`. The reason becomes clear when we
attempt to define it:

```idris
SomePure : a =%> SomeC a
SomePure = pure <! cannot
  where
    cannot : (x : a .req) -> Some (a .res) [x] -> a .res x
    cannot x (Take y z) = y
    cannot x (Drop y) = ?oops_again
```

We cannot rebuild the positions of `a` from a potential failing result. But there is another
way to look at this. `1 * _` is not a monad because it itself does not have a unit:

```idris
oneUnit : a =%> End * a
oneUnit = (() &&) <! (\x, y => ?oops3)
```

If we look at the type of the hole `?oops3` we see the context:

```
 0 a : Container
   x : a .req
   y : () + a .res x
------------------------------
oops3 : a .res x
```

From which we cannot provide a value of type `a.res x` because in the case where `y` is
a left-injection, then we do not have anything to return.

```idris
flatUnit : End * End =%> End
flatUnit = (const ()) <! (\_ => (+>))

flatUnit' : (End * End) * a =%> End * a
flatUnit' = ((() &&) . π2) <! ?b
```
