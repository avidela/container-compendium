module Interactive.Meteorology.Locale

public export
data Locale
  = Albanian
  | Afrikaans
  | Arabic
  | Azerbaijani
  | Basque
  | Belarusian
  | Bulgarian
  | Catalan
  | ChineseSimplified
  | ChineseTraditional
  | Croatian
  | Czech
  | Danish
  | Dutch
  | English
  | Finnish
  | French
  | Galician
  | German
  | Greek
  | Hebrew
  | Hindi
  | Hungarian
  | Icelandic
  | Indonesian
  | Italian
  | Japanese
  | Korean
  | Kurmanji
  | Kurdish
  | Latvian
  | Lithuanian
  | Macedonian
  | Norwegian
  | Farsi
  | Persian
  | Polish
  | Portuguese
  | Portugues_Brasil
  | Romanian
  | Russian
  | Serbian
  | Slovak
  | Slovenian
  | Spanish
  | Swedish
  | Thai
  | Turkish
  | Ukrainian
  | Vietnamese
  | Zulu

export
countryCode : Locale -> String
countryCode Albanian = "sq"
countryCode Afrikaans = "af"
countryCode Arabic = "ar"
countryCode Azerbaijani = "az"
countryCode Basque = "eu"
countryCode Belarusian = "be"
countryCode Bulgarian = "bg"
countryCode Catalan = "ca"
countryCode ChineseSimplified = "zh_cn"
countryCode ChineseTraditional = "zh_tw"
countryCode Croatian = "hr"
countryCode Czech = "cz"
countryCode Danish = "da"
countryCode Dutch = "nl"
countryCode English = "en"
countryCode Finnish = "fi"
countryCode French = "fr"
countryCode Galician = "gl"
countryCode German = "de"
countryCode Greek = "el"
countryCode Hebrew = "he"
countryCode Hindi = "hi"
countryCode Hungarian = "hu"
countryCode Icelandic = "is"
countryCode Indonesian = "id"
countryCode Italian = "it"
countryCode Japanese = "ja"
countryCode Korean = "kr"
countryCode Kurdish = "ku"
countryCode Kurmanji = "ku"
countryCode Latvian = "la"
countryCode Lithuanian = "lt"
countryCode Macedonian = "mk"
countryCode Norwegian = "no"
countryCode Farsi = "fa"
countryCode Persian = "fa"
countryCode Polish = "pl"
countryCode Portuguese = "pt"
countryCode Portugues_Brasil = "pt_br"
countryCode Romanian = "ro"
countryCode Russian = "ru"
countryCode Serbian = "sr"
countryCode Slovak = "sk"
countryCode Slovenian = "sl"
countryCode Spanish = "sp"
countryCode Swedish = "sv"
countryCode Thai = "th"
countryCode Turkish = "tr"
countryCode Ukrainian = "ua"
countryCode Vietnamese = "vi"
countryCode Zulu = "zu"

