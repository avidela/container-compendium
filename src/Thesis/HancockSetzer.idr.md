## Hancock & Setzer

Hancock & Setzer have done extensive work toward the use of dependent type
theory for real-world application. In their paper

```idris
module Thesis.HancockSetzer

import Data.Container


```

## Hancock & Hyvenat

In a subsequent work [[@hancockProgrammingInterfacesBasic2009|hancockProgrammingInterfacesBasic2009]]
Hancock & Hyvenat presented a general theory of interfaces and their
relations.
