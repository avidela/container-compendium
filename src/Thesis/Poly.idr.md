
## Poly, the Category of Polynomial functors

The study of containers is closely linked to the study of polynomial functors
while the category $\mathcal{Cont}$ and $\mathcal{Poly}$ are the same,  their
roots and motivations are different. Some insight comes from combining both
interpretation, and so it is important to know the difference, especially when
it comes to interpreting each as the foundation for interactive programs.

Indeed, $\mathcal{Poly}$, just like $\mathcal{Cont}$ is also used in the context
of interactive programs. The most exciting work is currently put forward by The
Topos institute and the chief publication in that regard is the _Poly book_ By
David Spivak and Nelson Niu.

In this book, the category $\Poly$ is presented as a tool to represent wiring
diagrams, in turn, those diagrams can be instanciated into dynamical systems
that can change across their lifetime and faithfully represent real-world
scenarios.

The category of polynomial functors is given by a set of positions, and a set of
directions, from which we can move.

```idris
module Thesis.Poly

import Data.Product
import Data.Coproduct

record Poly where
  constructor MkPoly
  pos : Type
  dir : pos -> Type

record PolyMap (p1, p2 : Poly) where
  constructor MkPolyMap
  -- map positions
  mpos : p1.pos -> p2.pos
  -- map directions
  mdir : (x : p1.pos) -> p2.dir (fwd x) -> p1.dir x
```

One intuition that could be given for the name would be to borrow from a
physical simulation system that can produce a new position
resulting from applying the direction vector at the given position.

There are other names for those concepts that come from other intuitions,
for example, in a game-theoretic interpretation, the positions would be the
_arena_ and the directions are the _moves_. The arena gives the set of possible
states that an agent can be in and from a given state, the agent has a set of
possible _moves_ it can go towards. Like before, the system becomes dynamical
when there is a way to recover a new state after selecting a move. We call this
way of turning a position/direction pair into one dynamical system "giving
dynamics to the string diagram"

### Operations on Poly

We can make use of Poly's multiple monoidal product to combine dynamical systems
in different ways, for example, it ought to be that we can represent two disconnected
systems that run in parallel without influencing each other

```idris
parallel : Poly -> Poly -> Poly
parallel (MkPoly p1 d1) (MkPoly p2 d2) =
    MkPoly (p1 * p2) (\x => d1 x.π1 * d2 x.π2)
```

One limitation with such system is that we need to provide two directions in order
to obtain a new state, instead, if we could provide either direction and obtain
the new state from it, we would not have to come up with both inputs as the same time

```idris
product : Poly -> Poly -> Poly
product (MkPoly p1 d1) (MkPoly p2 d2) =
    MkPoly (p1 * p2) (\x => d1 x.π1 + d2 x.π2)
```

This gives the choice of what input to give but we always obtain both outputs at one
if we could instead give an input and only obtain the corresponding output, it would
save us from running the system we are not interacting with.

```idris
coproduct : Poly -> Poly -> Poly
coproduct (MkPoly p1 d1) (MkPoly p2 d2) =
    MkPoly (p1 + p2) (choice d1 d2)
```

There are other combinators but I thought those gave a good overall view of what is possible
to do with polynomial functors as an interpretation of "interactive systems". Let's move on
and study an example.


### Dynamical systems - Moore & Mealy Machines

An example of a dynamical system are finite state machines. They store an internal
state, behave differently on input depending on that internal state, and some inputs
can change that internal state. Additionally, finite state machines can be composed
in multiple ways by running in parallel or in sequence for example. All those
behaviours can be captured with a term in $\Poly$ and it is the topic of this section.

Moore machines are given by three sets and two functions:

- $S$ the set of states.
- $O$ the set of outputs.
- $I$ the set of inputs.
- $\mathit{return} : S → O$ the function extracting the current output from the state
- $\mathit{update} : S × I → S$ the transition function from a current state and an
  input to a new state.

By taking the functions $\mathit{return}$ and $\mathit{update}$ as values to the
`MkPolyMap` constructor we can build the dependent lens $Poly((S, S), (O, I))$.

```idris
record Mealy (s, i, o : Type) where
  constructor MkMealy
  return : s -> o
  update : s -> i -> s

MealyLens : Mealy s i o -> MkPoly s (const s) `PolyMap` MkPoly o (const i)
MealyLens (MkMealy r u) = MkPolyMap r u
```

In this definition, the codomain of the lens is the interface to the mealy machine.
What's more, the first projection of the polynomial is the output and the second is the input.
This relation is flips the intput/output relation from my approach to dependent lenses as interactive
systems.

The basic state machine above can be represented with the container $(Nat,Move)$
but this description is not enough to obtain the execution profile of the state
machine, we still need to _give it dynamics_. We do this by building a morphism
of containers $(State, State) \Rightarrow (Nat, Move)$, it is defined by two
functions $State \to Nat$ and $State \times Move \to State$. Those two functions
also determine a state machine with state $State$, input $Move$ and outputs
$Nat$. Therefore, this encoding of dynamical system plases the input of the
system in the _direction_ (or the _indexed_ part of the container), and the
output in the _positions_ (or the _base_ of the container).
