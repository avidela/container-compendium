module Data.Sigma

import Data.Ops

||| Dependent pairs
public export
record Σ (a : Type) (b : a -> Type) where
  constructor (##)
  ||| First projection of sigma
  π1 : a
  ||| Second projection of sigma
  π2 : b π1

%pair Σ π1 π2

public export
projBoth : (x : Σ a b) -> x.π1 ## x.π2 = x
projBoth (p1 ## p2) = Refl

