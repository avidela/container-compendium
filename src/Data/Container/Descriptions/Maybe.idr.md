
preamble:[[preamble.sty]]

---

```table-of-contents
```

# The Maybe container

In categories of containers by Abbott etc, Containers are used
as _descriptions_ for data structures that carry values. The `Maybe`
type is one of them and can be described as a container.
This works because containers give rise to a functor thanks to
_the extension on containers_ `Ex : Container -> Type -> Type`.

Using this intuition, we can build a container that describes `Maybe`
in such a way that its extension is isomorphic to the `Maybe` type
we are used to. `Ex MaybeCont ~ Maybe`

```idris
module Data.Container.Descriptions.Maybe

import Data.Container
import Data.Container.Morphism
import Data.Container.Morphism.Eq
import Data.Product
import Data.Sigma
import Data.Iso
import Data.Maybe
import Proofs

%default total
```

To define the `Maybe` container, we need to define a boolean predicate. This is nothing more than a dependent type that is indexed over a boolean value, and that is inhabited with a trivial value when the boolean is true.
```idris
public export
IsTrue : Bool -> Type
IsTrue False = Void
IsTrue True = Unit
```

Using this predicate, we build the `Maybe` container by setting the shapes as `Bool` and the positions with our `IsTrue` predicate.
```idris
public export
MaybeCont : Container
MaybeCont = (!>) Bool IsTrue
```

The `Maybe` functor can now be defined as the extension on the `Maybe` container. We do this by using `Ex` from [Data.Container](../../Container.idr.md). To avoid clashing with the `Maybe` type from idris, to which it is isomorphic, we name it `MaybeType`.

```idris
public export
MaybeType : Type -> Type
MaybeType = Ex MaybeCont

public export
Just : (x : a) -> MaybeType a
Just x = MkEx True (\_ => x)

public export
Nothing : MaybeType a
Nothing = MkEx False absurd

export
toIdris : MaybeType a -> Maybe a
toIdris (MkEx False p) = Nothing
toIdris (MkEx True p) = Just (p ())

public export
fromIdris : Maybe a -> MaybeType a
fromIdris Nothing = Nothing
fromIdris (Just x) = Just x

%unbound_implicits off
toFromEq : {0 a : Type} -> (x : Maybe a) -> toIdris (fromIdris x) === x
toFromEq Nothing = Refl
toFromEq (Just x) = Refl

0 fromToEq : {0 a : Type} -> (x : MaybeType a) -> fromIdris (toIdris x) === x
fromToEq (MkEx True p) = cong (MkEx True) (funExt $ \() => Refl)
fromToEq (MkEx False p) = cong (MkEx False) (allUninhabited _ _)

%unbound_implicits on

public export
MaybeTypeIso : MaybeType a `Iso` Maybe a
MaybeTypeIso = MkIso
  toIdris
  fromIdris
  toFromEq
  fromToEq
```
## Maybe monad on containers

Equipped with our `Maybe` container, we can define the "Maybe monad on container" by partially applying `MaybeCont` to $\circ : Container → Container → Container$ which will result in a mapping on containers.

```idris
public export
MaybeAny : Container -> Container
MaybeAny = (MaybeCont ○)
```

Using `MaybeAny`[^1] we can build mapping that are similar to the ones we have for the traditional `Maybe` type: `Just`, `Nothing`, `join` and `strength`:

```idris
||| `just` maps a container to it's Maybe monad
public export
just : x =%> MaybeAny x
just = Just <! (\a, b => b.π2)

||| Nothing returns a Maybe container containing nothing
public export
nothing : End =%> MaybeAny x
nothing = (const Nothing) <! (\_ => absurd . π1)

||| This is a very complicated way to write `join` on Maybe
public export
joinFwd : (MaybeAny (MaybeAny x)).req ->
          (MaybeAny x).req
joinFwd (MkEx True fn) = fn ()
joinFwd (MkEx False fn) = (MkEx False absurd)

||| Given (Maybe (Maybe a)) and an indexed reply that works on `Maybe a` after it's been
||| joined by `joinFwd`, reconstruct an answer `Maybe (Maybe a)` depending on the original
||| argument.
%inline
public export
joinBwd : {0 x : Container} ->
          (s : (MaybeAny (MaybeAny x)).req) ->
          ((MaybeAny x).res (joinFwd {x} s)) ->
          (MaybeAny (MaybeAny x)).res s
joinBwd (MkEx True fn) (x1 ## x2) = () ## x1 ## x2
joinBwd (MkEx False fn) (x1 ## x2) = absurd x1

||| Collapse two Maybe monads into one
public export
join : {0 x : Container} -> (MaybeAny (MaybeAny x)) =%> MaybeAny x
join = joinFwd <! joinBwd

||| Maybe is a strong monad in containers
||| We don't end up using this but it was important for open games
public export
strength : ((MaybeAny x) ⊗ y) =%> (MaybeAny (x ⊗ y))
strength = fwd <! bwd
  where
    fwd : (MaybeAny x ⊗ y).req -> (MaybeAny (x ⊗ y)).req
    fwd arg = MkEx arg.π1.ex1 (\k => arg.π1.ex2 k && arg.π2)

    bwd : (s : (MaybeAny x).req * y.req) ->
          (p : (MaybeAny (x ⊗ y)).res (fwd s)) ->
          ((MaybeAny x) ⊗ y).res s
    bwd s p = (p.π1 ## p.π2.π1) && p.π2.π2
```

There are two questions that emerge from this defintion.

- What is the idris equivalent of the `MaybeAny` definition and what does it mean?

- If we perform this construction with `○` can we do it with `#>`? And if so, what does it mean?

The first question has its root in the fact that we now have two way of representing `Maybe`: either as the built-in
data definition, or as the extension on the `Maybe` container. Arguably, they are the same, however, this fact would
not be so obvious to someone simply staring at the definition of `MaybeCont`. Learning the correspondence with exsiting
programming structures gives us a wealth of information relating to its existing and future use that we could not glean
from its original definition. Since this exercise is so fruitful, it would be great to find out what it means, in
the real of dependently-types programming, to deal with a data structure like `MaybeAny`. This is the topic of the next
section.

The second question will be answered in the section after that.

## MaybeAny as error propagation

To answer the question "what is the programming equivalent of `MaybeCont ○`" we study the definition of a morphism `a =%> MaybeCont ○ b`. This morphism will tell us what `MaybeCont ○` is supposed to mean _computationally_, and from this interpretation, we will derive a data-type that
will play the same role. Remember that it has two maps:
```
fwd : a.req -> Ex MaybeCont b.req
bwd : (x : a.req) -> (Σ (y : IsTrue ((fwd x).ex1)) | b.res ((fwd x).ex2 y)) -> a .res x
```

This type requires a great deal of squinting to decipher so we're going to analyse it step-by-step here:

- In`fwd : a.req -> Ex MaybeCont b.req`, because `Ex MaybeCont x` is isomorphic to `Maybe x` we can interpret this as `fwd : a.req -> Maybe b.req`, in other words, this mapping maps shapes of `a` to shapes of `b` but it might fail in the process.
- In `bwd : (x : a.req) -> (Σ (y : IsTrue ((fwd x).ex1)) | b.res ((fwd x).ex2 y)) -> a .res x` the middle part is the hardest to read, `(IsTrue ((fwd x).ex1))` essentially checks the tag of the data, if the maybe represents a `Just` constructor, the `True` case of the tag, the type of the first projection of the `Σ` will be `()`. Otherwise, it will be `Void`, an uninhabited type.
- In `(b.res . (fwd x).ex2)` we produce a type of responses indexed over the forward mapping to `b`.

In this way, the second argument of `bwd` is the type of valid responses in `b` whenever the question made sense according to the mapping of the _question_ `fwd`.

One could also interpret this as a dependent version of `IsJust`. Which we define next:

```idris
||| Types of indexed maybe values that are always present
public export
data Any : (x -> Type) -> Maybe x -> Type where
  Yep : {v : x} -> pred v -> Any pred (Just v)

||| An always present value can be extracted safely
public export
(.unwrap) : Any p (Just x) -> p x
(.unwrap) (Yep y) = y

||| Any is never inhabited when we don't have a value
public export
Uninhabited (Any p Nothing) where
  uninhabited _ impossible
```

The `Any` type takes a predicate and tells us it is true whenever `Maybe` is `Just`. It is called like so because it is never the case that the predicate is wrong, since we don't get a value when it's not.

Now that we know that the shapes of `MaybeCont ○` map to `Maybe` and the positions map to `Any` we can write a definition of our container using idris-hosted data structures:

```idris
public export
MaybeAnyIdris : Container -> Container
MaybeAnyIdris c = (!>) (Maybe c.req)
                       (Any c.res)
```

To prove the claim that they are the same, I here is an isomorphism:

```idris
||| We can always convert from the `MaybeCont ○` monad to the Idris definition
public export
MaybeToAny : MaybeAny x =%> MaybeAnyIdris x
MaybeToAny =
    toIdris <! bwd
    where
      bwd : (v : MaybeType x.req) ->
            Any x.res (toIdris v) -> Σ (IsTrue v.ex1) (\y => x.res (v.ex2 y))
      bwd (MkEx True p) (Yep x) = () ## x
      bwd (MkEx False p) _ impossible

||| We can always convert from the Idris definition to the `MaybeCont ○` monad
public export
AnyToMaybe : MaybeAnyIdris x =%> MaybeAny x
AnyToMaybe =
    fromIdris <! bwd
    where
      bwd : (v : Maybe x.req) -> (MaybeAny x).res (fromIdris v) -> Any x.res v
      bwd Nothing x = absurd x.π1
      bwd (Just y) x = Yep x.π2

||| Going from Idris to `MaybeCont ○` and back to Idris is like doing nothing
AnyToMaybeToAny : {0 x : Container} -> (AnyToMaybe {x} ⨾ MaybeToAny {x}) <%≡%> (identity {a = MaybeAnyIdris x})
AnyToMaybeToAny = MkDepLensEq
  toFromEq
  (\case Nothing => \x => absurd x
         (Just y) => \(Yep x) => Refl)

||| Going from `MaybeCont ○` to Idris and back is like doing nothing
MaybeToAnyToMaybe : {0 x : Container} -> (MaybeToAny{x} ⨾ AnyToMaybe  {x}) <%≡%> (identity {a = MaybeAny x})
MaybeToAnyToMaybe = MkDepLensEq
  fromToEq
  bwdEq
  where
    0 bwdEq : (v : MaybeType x.req) -> (y : (MaybeAny x).res (fromIdris (toIdris v))) ->
            (MaybeToAny{x} ⨾ AnyToMaybe  {x}).bwd v y === replace {p = (MaybeAny x).res} (fromToEq v) y
    bwdEq (MkEx False ex2) y = absurd y.π1
    bwdEq (MkEx True ex2) y = sigEqToEq $ MkSigEq (unitUniq _) Refl

||| Putting it all together
public export
AnyMaybeIso : ContIso (MaybeAnyIdris x) (MaybeAny x)
AnyMaybeIso = MkGenIso
  AnyToMaybe
  MaybeToAny
  AnyToMaybeToAny
  MaybeToAnyToMaybe
```

For completion, the idris version also supports the same functorial & monadic structure. It turns out, those are useful to write proofs about `MaybeCont ○` as a monad since the types a much easier to decipher than their analogous encodings as descriptions.

```idris
public export
maybeMap : (a -> b) -> Maybe a -> Maybe b
maybeMap f (Just x) = Just (f x)
maybeMap f Nothing = Nothing

public export
bwdMaybeAnyF: {0 x, y : Container} -> {mor : x =%> y} -> (v : Maybe x.req) ->
              Any y.res (maybeMap mor.fwd v) -> Any x.res v
bwdMaybeAnyF Nothing x = absurd x
bwdMaybeAnyF (Just v) w = Yep (mor.bwd v w.unwrap)

public export
MaybeAnyIdrisFunctor : x =%> y -> MaybeAnyIdris x =%> MaybeAnyIdris y
MaybeAnyIdrisFunctor mor =
    (maybeMap mor.fwd) <! bwdMaybeAnyF

public export
maybeAnyPure : (x : Container) -> x =%> MaybeAnyIdris x
maybeAnyPure x = Just <! (\_, v => v.unwrap)

public export
maybeJoin : Maybe (Maybe x) -> Maybe x
maybeJoin (Just (Just v)) = Just v
maybeJoin _ = Nothing

public export
maybeJoinBwd : (x : Maybe (Maybe a)) -> Any f (Maybe.maybeJoin x) -> Any (Any f) x
maybeJoinBwd (Just (Just v)) (Yep y) = Yep (Yep y)

public export
maybeAnyJoin : (x : Container) -> MaybeAnyIdris (MaybeAnyIdris x) =%> MaybeAnyIdris x
maybeAnyJoin _ =
    maybeJoin <! maybeJoinBwd
```

## MaybeAll as error handling

Now that we know what `MaybeCont ○` means, we study what `MaybeCont #>` would be, the definition is straighforward.

```idris
public export
MaybeAll : Container -> Container
MaybeAll = (MaybeCont #>)
```

However its interpretation is not. We've seen `MaybeAny` as a map on container giving error semantics to its morphisms. Analogously to `MaybeCont ○`, we're going to analyse the type of mappings `a =%> MaybeCont #> b` to inform our intuition:

```

fwd : a.req -> Ex MaybeCont b.req ~ Maybe b.req
bwd : (x : a.req) -> ((y : IsTrue ((fwd x).ex1)) -> b.res ((fwd x).ex2 y)) -> a .res x
```

Just like before, the forward map converts _questions of `a` into questions of `b`_ with the added possibility of failure, as represented by `Maybe`. The backward map is different in its second argument, instead of providing a product of evidence that the computation went through, and a matching response, it gives us a function that will always return a valid response `b.res`.

```idris
namespace Maybe
  public export
  data All : (x -> Type) -> Maybe x -> Type where
    Nah : All pred Nothing
    Aye : {v : x} -> pred v -> All pred (Just v)

public export
obtain : All p (Just a) -> p a
obtain (Aye x) = x
```

Equivalently, this could write this as a function, which may illustrate the behavior
more clearly for some:
```idris
0
Couldbe : (a -> Type) -> Maybe a -> Type
Couldbe p Nothing = ()
Couldbe p (Just x) = p x

-- This to ensure it's an isomoprhism with the `All` data
ToMaybe : {0 m : Maybe a} -> {0 p : a -> Type} -> All p m -> Couldbe p m
ToMaybe Nah = ()
ToMaybe (Aye x) = x

fromMaybe : {m : Maybe a} -> {0 p : a -> Type} -> Couldbe p m -> All p m
fromMaybe {m = Nothing} x = Nah
fromMaybe {m = (Just y)} x = Aye x
```

Using `All` we can build a container using only idris types, rather than encodings, for our new monad on containers.

```idris
public export
MaybeAllIdris : Container -> Container
MaybeAllIdris c = (!>) (Maybe c.req) (All c.res)
```

In the following we ensure the isomorphism between our two representations
```idris

toIdrisBwd : (v : MaybeType x.req) ->
             All x.res (toIdris v) -> (val : IsTrue v.ex1) -> x.res (v.ex2 val)
toIdrisBwd (MkEx False v2) x val = absurd val
toIdrisBwd (MkEx True v2) x () = obtain x

public export
MaybeToAll : MaybeAll x =%> MaybeAllIdris x
MaybeToAll =
    toIdris <! toIdrisBwd

fromIdrisBwd : (v : Maybe x.req) -> ((val : IsTrue ((fromIdris v).ex1)) -> x.res ((fromIdris v).ex2 val)) -> All x.res v
fromIdrisBwd Nothing f = Nah
fromIdrisBwd (Just y) f = Aye (f ())

public export
AllToMaybe : MaybeAllIdris x =%> MaybeAll x
AllToMaybe =
    fromIdris <! fromIdrisBwd

AllMaybeIso : {x : Container} ->
    MaybeToAll {x} ⨾ AllToMaybe {x} <%≡%> identity {a = MaybeAll x}
AllMaybeIso = MkDepLensEq
    fwdEq
    bwdEq
    where
      fwdEq : (v : MaybeType x.req) -> fromIdris (toIdris v) = v
      fwdEq (MkEx True p) = cong (MkEx True) (funExt $ \() => Refl)
      fwdEq (MkEx False p) = cong (MkEx False) (allUninhabited _ _)

      0 bwdEq : (v : MaybeType x.req) ->
              (y : ((val : IsTrue ((fromIdris (toIdris v)).ex1)) ->
                   x .res ((fromIdris (toIdris v)).ex2 val))) ->
          let 0 p1 : (MaybeAll x).res v
              p1 = (MaybeToAll {x} ⨾ AllToMaybe {x}).bwd v y
              0 p2 : (MaybeAll x).res v
              p2 = (identity {a = MaybeAll x}).bwd v (replace {p = (MaybeAll x).res} (fwdEq v) y)
          in p1 === p2
      bwdEq (MkEx True v) y = funExtDep $ \() => Refl
      bwdEq (MkEx False v) y = funExtDep $ \x => absurd x

MaybeAllIdristimesIso : {x : Container} ->
    AllToMaybe {x} ⨾ MaybeToAll {x} <%≡%> identity {a = MaybeAllIdris x}
MaybeAllIdristimesIso = MkDepLensEq
    fwdEq
    bwdEq
    where
      fwdEq : (v : Maybe (x .req)) -> toIdris (fromIdris v) = v
      fwdEq Nothing = Refl
      fwdEq (Just y) = Refl
      bwdEq : (v : Maybe (x .req)) ->
              (y : All (x .res) (toIdris (fromIdris v))) ->
              let 0 p1 : (MaybeAllIdris x).res v
                  p1 = (AllToMaybe {x} ⨾ MaybeToAll {x}).bwd v y
                  0 p2 : (MaybeAllIdris x).res v
                  p2 = (identity {a = MaybeAllIdris x}).bwd v (replace {p = (MaybeAllIdris x).res} (fwdEq v) y)
              in p1 === p2
      bwdEq Nothing Nah = Refl
      bwdEq (Just z) (Aye y) = Refl

MaybeAllContIso : ContIso (MaybeAll x) (MaybeAllIdris x)
MaybeAllContIso = MkGenIso
    MaybeToAll
    AllToMaybe
    AllMaybeIso
    MaybeAllIdristimesIso
```

```idris

public export
maybeAllPure : (x : Container) -> x =%> MaybeAllIdris x
maybeAllPure _ = Just <! (\x, y => obtain y)

public export
maybeAllJoinBwd : (x : Maybe (Maybe a)) -> All f (Maybe.maybeJoin x) -> All (All f) x
maybeAllJoinBwd (Just (Just v)) (Aye y) = Aye (Aye y)
maybeAllJoinBwd (Just Nothing) b = Aye Nah
maybeAllJoinBwd Nothing b = Nah

public export
maybeAllJoin : (x : Container) -> MaybeAllIdris (MaybeAllIdris x) =%> MaybeAllIdris x
maybeAllJoin _ =
    maybeJoin <!
    maybeAllJoinBwd

public export
bwdMaybeAllF: {0 x, y : Container} -> {mor : x =%> y} -> (v : Maybe x.req) ->
              All y.res (maybeMap mor.fwd v) -> All x.res v
bwdMaybeAllF Nothing Nah = Nah
bwdMaybeAllF (Just v) (Aye z) = Aye (mor.bwd v z)

public export
MaybeAllIdrisFunctor : a =%> b -> MaybeAllIdris a =%> MaybeAllIdris b
MaybeAllIdrisFunctor x =
    (maybeMap x.fwd) <! bwdMaybeAllF
```


[^1]: The reason why it is called `MaybeAny` will become clear later,
