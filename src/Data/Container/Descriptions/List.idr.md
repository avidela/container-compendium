```table-of-contents
```
## Lists as type descriptions

Containers can be used as descriptors for data types. This module implements the `List` container as well as its usage as a monad on containers.

```idris
module Data.Container.Descriptions.List

import Data.Container
import Data.Container.Morphism
import Data.Container.Category
import Data.Container.Cartesian
import Data.Container.Morphism.Cartesian.Bifunctor.Cont
import Data.Category.Bifunctor
import Data.Fin
import Data.List
import Data.Sigma
import Data.List.Quantifiers
import Data.Iso

import Proofs

%default total
```
Containers, when interpreted as type descriptors, hold a _shape_ for data in their first argument, and _positions_ inside the aforementionned shape. In the case of list, the shape is given by a natural number which describes its length. The positions are all the possibles values for a given shape. Since the shape is the number of elements, the positions are given by a type that has the same number of inhabitants as the length of the list.
This way, a list of `3` elements, hosts `3` values.

```idris
public export
ListCont : Container
ListCont = (!>) Nat Fin
```

A container is not a data type, nor is it a value for this data type. To create datatypes, we enlist the _extension_ of containers, described [previously](../../Container.idr.md).

```idris
public export
TyList : Type -> Type
TyList = Ex ListCont
```

The above entirely defines the `List` functor, and inherits its functoriality from `Ex`. We can now construct inhabitants for our bespoke list type, for example, the constructors for the empty list, and the constructor for `cons`:

```idris
public export
Nil : TyList a
Nil = MkEx Z absurd

public export
Cons : a -> TyList a -> TyList a
Cons x ls = MkEx (S ls.ex1) (\case (FS n) => ls.ex2 n
                                 ; FZ => x)
```

The thesis of this work is to show how to use containers and their monads to build a certain category of programs. The `List` container plays an instrumental role in enabling complex program which generate multiple inputs of variable length. This is done by partially applying monoidal products and actions on containers to obtain monads on container. The two operation we use here are `○` and `#>`.

To understand why those operators and what they mean, we need to depart from "containers as data descriptor" intuition, and venture into "containers as query/response mechanisms". In that context, the first argument of a container represents a _query_ or a _question_ and the second is the _response_ to that query or the _answer_ to the question. The response is appropriately indexed over the question such that all response are related to the question. This way, one cannot answer "yes" to the question "how many computer scientists does it take to change a lightbulb?".

Equipped with our new intuition for containers, as `(question, answer)` we would like to be able to deal with a specific kind of question/answer mechanism, one that be split into multiple sub-questions, and for which we need to provide an appropriate answer.

Equipped with our new intuition for containers, as `(question, answer)` we would like to be able to deal with a specific kind of question/answer mechanism, one that be split into multiple sub-questions, and for which we need to provide an appropriate answer.

We can see this as a morphism `(question, answer) => (List question, ??? answer)`. It's not immediately clear what to use as the responses for a list of questions because there are at least 2 obvious candidates:

- We need to answer all sub-questions
- We need to answer any 1 of the sub-questions

Interestingly enough, in dependently-typed programming, we have two types that do exactly that : `All : (p : a -> Type) -> List a -> Type` and `Any : (p : a -> Type) -> List a -> Type`. The first one ensures that a predicate `p` is true for all elements of the list given, and the second one requires exactly one of them is true.

With those two types, given a container `(question, answer)`, we can construct the container that represents "lists of questsion with matching answers" as `(List question, All answer)` and `(List question, Any answer)` for the container that provides answers to _all_ questions, and the container that provides only 1 answer for a given list of questions.

```idris
public export
AllC : Container -> Container
AllC c = (x : List c.req) !> All c.res x

public export
AnyC : Container -> Container
AnyC c = (x : List c.req) !> Any c.res x
```

Interestingly enough, those two containers can also be obtained _from_ our list containers. If we make the following observations:

- `List a ~ Ex ListCont a`
- `Any p xs ~ Σ (x : a) . x `Elem` xs -> p x `
- `All p xs ~ (x : a) -> x `Elem` xs -> p x`


The first equation is given by the extension of the list container applied to a type `a`. The second one is given by the `○` applied to the list container and another container `(a, p)`:

- `List ○ (a, p)`
- `(Nat, Fin) ○ (a, p)`
- `(xs : Ex (Nat, Fin) a, Σ (y : Fin xs.ex1) (c2.res (x.ex2 y))`


```idris
public export
ListAny : Container -> Container
ListAny = (○) ListCont

public export
ListAll : Container -> Container
ListAll = (ListCont #>)

public export
tyListToList : TyList x -> List x
tyListToList (MkEx Z y) = []
tyListToList (MkEx (S n) y) = y FZ :: assert_total (tyListToList (MkEx n (y . FS)))

public export
listToTyList : List x -> TyList x
listToTyList [] = Nil
listToTyList (y :: xs) = Cons y (listToTyList xs)

public export
0 exEta : MkEx (recd.ex1) (\x => recd.ex2 x) = recd
exEta {recd = MkEx r1 r2} = Refl

public export
0 exP1 : MkEx a b = c -> a = c.ex1
exP1 {c = (MkEx ex1 ex2)} Refl = Refl

public export
prf1 : {0 a : Type} -> (x : List a) -> tyListToList (listToTyList x) = x
prf1 [] = Refl
prf1 (x :: xs) = cong (x ::) (let ppp = prf1 xs in cong tyListToList exEta `trans` ppp)

public export
0 prf2 : (v : TyList x) -> listToTyList (tyListToList v) = v
prf2 (MkEx Z f) = cong2Dep' MkEx Refl (allUninhabited _ _)
prf2 (MkEx (S n) f) with (assert_total $ prf2 (MkEx n (f . FS)))
  prf2 (MkEx (S n) f) | pat = cong2Dep' MkEx (cong S $ sym $ exP1 $ sym pat)
                                  (funExt $ \case FZ => rewrite pat in Refl
                                                  (FS y) => rewrite pat in Refl)

public export
tyListIso : TyList x `Iso` List x
tyListIso = MkIso
  tyListToList
  listToTyList
  prf1
  prf2

public export
concat : TyList a -> TyList a -> TyList a
concat (MkEx Z dd) y = y
concat (MkEx (S n) dd) y = assert_total $ Cons (dd FZ) (concat (MkEx n (dd . FS)) y)

public export
singleton : a -> TyList a
singleton x = Cons x Nil

public export
pure : a =%> ListAll a
pure =
    singleton <! bwd
    where
      bwd : (x : a.req) -> (ListAll a).res (singleton x) -> a.res x
      bwd x f = f FZ

public export
listAppend : List a -> List a -> List a
listAppend [] ys = ys
listAppend (x :: xs) ys = x :: xs ++ ys

public export
flatList : List (List x) -> List x
flatList [] = []
flatList (y :: xs) = y ++ flatList xs

public export
bwdFromListAll : (v : TyList x .req) ->
      All x.res (tyListToList v) ->
      (val : Fin (v.ex1)) -> x.res (v.ex2 val)
bwdFromListAll (MkEx 0 p2) _ val impossible
bwdFromListAll (MkEx (S k) p2) (x :: xs) FZ = x
bwdFromListAll (MkEx (S k) p2) (x :: xs) (FS v) = bwdFromListAll (MkEx k (p2 . FS)) xs v

public export
fromListAll : ListAll x =%> AllC x
fromListAll =
    tyListIso.to <!
    bwdFromListAll

public export
bwdToListAll : (v : List x.req) -> ((val : Fin ((listToTyList v).ex1)) -> x .res ((listToTyList v).ex2 val)) -> All x.res v
bwdToListAll [] f = []
bwdToListAll (y :: (xs)) f = f FZ :: bwdToListAll xs (\x => f (FS x))

public export
toListAll : AllC x =%> ListAll x
toListAll =
    tyListIso.from <!
    bwdToListAll

public export
map1 : (a -> b) -> (a, x) -> (b, x)
map1 f y = (f (fst y), snd y)

public export
splitLocal : (xs : List a) -> All p (xs ++ ys) -> (All p xs, All p ys)
splitLocal [] pxs = ([], pxs)
splitLocal (_ :: xs) (px :: pxs) = map1 (px ::) (splitLocal xs pxs)

public export
allHead : All f (x :: xs) -> f x
allHead (y :: z) = y

public export
pure' : x =%> AllC x
pure' = (:: []) <! (\x, y => allHead y)

public export
joinBwd : (val : List (List (x .req))) -> All (x .res) (flatList val) -> All (All (x .res)) val
joinBwd [] x = []
joinBwd (y :: xs) arg = let k : (All x.res y, All x.res (flatList xs))
                            k = splitLocal y arg
                         in fst k :: joinBwd xs (snd k)
public export
join' : AllC (AllC x) =%> AllC x
join' = flatList <! joinBwd

public export
join : ListAll (ListAll x) =%> ListAll x
join = let
      pre2 : ListAll (AllC x) =%> AllC (AllC x)
      pre2 = fromListAll {x = AllC x}

      pre1 : ListAll  (ListAll x) =%> ListAll (AllC x)
      pre1 = contFunctor {a = ListCont} fromListAll

    in pre1 ⨾ pre2 ⨾ join' ⨾ toListAll

public export
listMap : (a -> b) -> List a -> List b
listMap f [] = []
listMap f (x :: xs) = f x :: listMap f xs

public export
listMapAppend : {xs : _} -> listMap f (xs ++ ys) === listMap f xs ++ listMap f ys
listMapAppend {xs = []} = Refl
listMapAppend {xs = (x :: xs)} = cong (f x ::) (listMapAppend {xs})

public export
mapProp : {f : a -> b} -> {0 p : b -> Type} -> {0 q : a -> Type} ->
          (fn : (x : a) -> p (f x) -> q x) ->
          (v : List a) ->
          All p (listMap f v) ->
          All q v
mapProp fn [] x = []
mapProp fn (x :: xs) (y :: ys) = fn x y :: mapProp fn xs ys

public export
AllCFunctor : {0 x, y : Container} -> x =%> y -> AllC x =%> AllC y
AllCFunctor mor =
    (listMap mor.fwd) <!
    (mapProp {f = mor.fwd, p = y.res, q = x.res} mor.bwd)

public export 0
listMapId : (x : List a) -> List.listMap Prelude.id x = Prelude.id x
listMapId [] = Refl
listMapId (x :: xs) = cong (x ::) (listMapId xs)

public export 0
mapPropId : {p : a -> Type} -> (x : List a) -> (y : All p x) ->
            mapProp {f = Prelude.id} (\_ => Prelude.id) x (rewrite__impl (All p) (listMapId x) y) === y
mapPropId [] [] = Refl
mapPropId (x :: xs) (y :: ys) = cong (y ::) (mapPropId xs ys)

public export
allCompFwd : (h : b -> c) -> (g : a -> b) -> (xs : List a) -> listMap (h . g) xs = (listMap h (listMap g xs))
allCompFwd h g [] = Refl
allCompFwd h g (x :: xs) = cong (h (g x) ::) (allCompFwd h g xs)

%unbound_implicits off
allCompBwd : {0 a, b, c : Type} ->
    {gf : a -> b} ->
    {hg : b -> c} ->
    {p : b -> Type} ->
    {q : a -> Type} ->
    {r : c -> Type} ->
    {g : (v : a) -> p (gf v) -> q v} ->
    {h : (x : b) -> r (hg x) -> p x} ->
    (x : List a) ->
    (y : All r (listMap (hg . gf) x)) ->
    let
      0 ll : All p (listMap gf x)
      ll = mapProp {f = hg, p = r, q = p} h (listMap gf x) (rewrite__impl (All r) (sym $ allCompFwd hg gf x) y)
      0 tt : All q x
      tt = mapProp {f = gf, p, q} g x ll
      rr : All q x
      rr = mapProp (\z, w => g z (h (gf z) w)) x y
    in rr === tt
allCompBwd [] y = Refl
allCompBwd (x :: xs) (y :: ys) = cong (g x (h (gf x) y) ::) (allCompBwd xs ys)


%unbound_implicits on
public export
AllCFunctorFunc : Functor Cont Cont
AllCFunctorFunc = MkFunctor
  AllC
  (\_, _ => AllCFunctor)
  (\c => cong2Dep'
    (<!)
    (funExt listMapId)
    (funExtDep $ \x => funExt $ \y => mapPropId x (rewrite__impl (All c.res) (sym $ listMapId x) y)))
  (\a, b, c, g, h => cong2Dep' (<!)
      (funExt $ \x => allCompFwd h.fwd g.fwd x)
      (funExtDep $ \x => funExt $ \y =>
        allCompBwd {hg = h.fwd, p = b.res, g = g.bwd, h = h.bwd} x y))

public export
data ListAllEq : {0 a : Type} -> {0 p : a -> Type} -> {0 q : a -> Type} ->
                 {ls : List a} -> {0 prf : p ≡ q} ->
                 All p ls -> All q ls -> Type where
  AllEmpty : ListAllEq [] []
  AllCons : {0 a : Type} -> {0 p : a -> Type} -> {0 q : a -> Type} ->
            {ls : List a} -> {v1 : a} -> (x : p v1) -> (y : q v1) ->
            {xs : All p ls} -> {ys : All q ls} ->
            {prf : p ≡ q} ->
            {prf2 : x ≡ replace {p = \f => f v1} (sym prf) y} ->
            ListAllEq {a, p, q, ls, prf} xs ys ->
            ListAllEq {a, p, q, ls = v1 :: ls, prf} (x :: xs) (y :: ys)
-- data ListAllEq : {0 a : Type} -> {0 p : a -> Type} ->
--                  (l1, l2 : List a) -> All p l1 -> All p l2 -> Type where
--   AllEmpty : ListAllEq [] [] [] []
--   AllCons : (v : a) -> (w1, w2 : p v) -> w1 ≡ w2 ->
--             ListAllEq l1 l2 p1 p2 -> ListAllEq {p} (v :: l1) (v :: l2) (w1 :: p1) (w2 :: p2)

listAllEqToEq : {0 a : Type} -> {0 p : a -> Type} -> {0 q : a -> Type} ->
                {prf : p ≡ q} ->
                {ls : List a} -> {xs : All p ls} -> {ys : All q ls} ->
                ListAllEq {ls, prf} xs ys -> ys ≡ replace {p = \vx => All vx ls} prf xs
listAllEqToEq {ls = [], prf = Refl} AllEmpty = Refl
listAllEqToEq {ls = y :: zs} (AllCons {prf = Refl, prf2 = Refl} z z w) = cong (z ::) (listAllEqToEq w)

public export
data ListAllAllEq : {0 a : Type} ->
                 {0 p : a -> Type} ->
                 {0 q : a -> Type} ->
                 (l1 : List (List a)) ->
                 All (All.All p) l1 ->
                 All (All.All q) l1 -> Type where
                   -- listMap (m .fwd) (flatList v) and flatList (left .fwd v)
  AllAllEmpty : {0 a : Type} ->
             {0 p : a -> Type} ->
             {0 q : a -> Type} -> ListAllAllEq {a, p, q} [] [] []
  -- AllAllCons : {0 a : Type} ->
  --           {0 p : a -> Type} ->
  --           {0 q : a -> Type} ->
  --           {l1 : List (List a)} ->
  --           {p1 : All (All p) l1} ->
  --           {p2 : All (All q) l1} ->
  --           (v : List a) ->
  --           (w1 : All p v) ->
  --           (w2 : All q v) ->
  --           (ford : ListAllEq w1 w2) ->
  --           ListAllAllEq {a, p, q} l1 p1 p2 ->
  --           ListAllAllEq {a, p, q} (v :: l1) (w1 :: p1) (w2 :: p2)

-- public export
-- listEqToEq : {0 a : Type} -> {0 p : a -> Type} -> {l1, l2 : List a} -> {a1 : All p l1} -> {a2 : All p l2} ->
--              ListAllEq l1 l2 a1 a2 -> l1 === l2
-- listEqToEq AllEmpty = Refl
-- listEqToEq (AllCons v w w Refl x) = cong (v ::) (listEqToEq x)
--
-- export
-- listEqToEq' : {0 a : Type} -> {0 p : a -> Type} -> {l1, l2 : List a} -> {a1 : All p l1} -> {a2 : All p l2} ->
--              (prf : ListAllEq l1 l2 a1 a2) -> a1 === replace {p = All p} (sym $ listEqToEq prf) a2
-- listEqToEq' AllEmpty = Refl
-- listEqToEq' (AllCons v w w Refl x) = ?Hue
-- listEqToEq' (AllConcat w x) = ?Hue2
-- export
-- listEqToEq : {0 a : Type} -> {0 p : a -> Type} -> {l1, l2 : List a} -> {a1 : All p l1} -> {a2 : All p l2} ->
--              ListAllEq l1 l2 a1 a2 -> l1 === l2
-- listEqToEq AllEmpty = Refl
-- listEqToEq (AllCons v w1 w1 Refl x) = cong (v ::) (listEqToEq x)
--
-- export
-- listEqToEq' : {0 a : Type} -> {0 p : a -> Type} -> {l1, l2 : List a} -> {a1 : All p l1} -> {a2 : All p l2} ->
--              (prf : ListAllEq l1 l2 a1 a2) -> a1 === rewrite__impl (All p) (listEqToEq prf) a2
-- listEqToEq' AllEmpty = Refl
-- listEqToEq' (AllCons v w1 w1 Refl x) with (listEqToEq x)
--   _ | Refl = cong (w1 ::) (listEqToEq' x)
--
-- data ListFlatMapTwice : {a, b : Type} -> {ls : List (List a)} -> {p : b -> Type} ->
--     {f : a -> b} -> All p (listMap f (flatList ls)) -> All p (flatList (listMap (listMap f) ls)) -> Type where
--       FNil : ListFlatMapTwice {ls = []} [] []
--       FCons :


public export
ListAllFunctor : Functor Cont Cont
-- ListAllFunctor = applyBifunctor {a = ContCartCat, b = Cont} ListCont univCompBifunctor

----------------------------------------------------------------------------------
-- Any Lists
----------------------------------------------------------------------------------


AnyToList : AnyC x =%> ListAny x
AnyToList =
    tyListIso.from <! bwd
    where
      bwd : (v : List (x.req)) ->
            Σ (Fin ((listToTyList v).ex1)) (\y => x.res ((listToTyList v).ex2 y)) ->
            Any (x.res) v
      bwd [] x = absurd x.π1
      bwd (x :: xs) (FZ ## p2) = Here p2
      bwd (x :: xs) (FS p1 ## p2) = There (bwd xs (p1 ## p2))

ListToAny : ListAny x =%> AnyC x
ListToAny =
    tyListIso.to <! bwd
    where
      bwd : (v : TyList x.req) ->
            Any (x .res) (tyListToList v) ->
            Σ (Fin (v.ex1)) (\y => x .res (v.ex2 y))
      bwd (MkEx 0 v2) x = absurd x
      bwd (MkEx (S k) v2) (Here x) = FZ ## x
      bwd (MkEx (S k) v2) (There x) = let rec = bwd (MkEx k (v2 . FS)) x in FS rec.π1 ## rec.π2

public export
ListAnyFunctor : Functor Cont Cont
ListAnyFunctor = applyBifunctor {a = Cont, b = Cont} ListCont ComposeContBifunctor

public export
flatListMap : (f : a -> b) -> (v : List (List a)) -> (listMap f (flatList v)) === (flatList (listMap (listMap f) v))
flatListMap f [] = Refl
flatListMap f (x :: xs) = trans (listMapAppend) (cong (listMap f x ++) (flatListMap f xs))


public export
mapPropAppend : {0 a, b : Type} ->
                {0 f : a -> b} -> {0 p : b -> Type} -> {0 q : a -> Type} ->
                (xs : List a) -> (vs : All p (listMap f xs)) ->
                (ys : List a) -> (ws : All p (listMap f ys)) ->
                (gn : (x : a) -> p (f x) -> q x) ->
                mapProp gn (xs ++ ys) (rewrite__impl (All p) ((List.listMapAppend {f, xs, ys})) (vs ++ ws)) ===
                  mapProp gn xs vs ++ mapProp gn ys ws
mapPropAppend [] [] ys ws gn = Refl
mapPropAppend (x :: xs) (v :: vs) ys ws gn = cong (gn x v :: ) (mapPropAppend xs vs ys ws gn)

public export
fromList : ((x : a) -> p x) -> (xs : List a) -> All p xs
fromList _ [] = []
fromList e (x :: xs) = e x :: fromList e xs

```

## `All` lists

```idris
public export
ListAllIdris : Container -> Container
ListAllIdris c = (!>) (List c.req) (All c.res)

{-
allToIdris : ListAll x =%> ListAllIdris x
allToIdris =
    tyListIso.to <!
    bwd
    where
      bwd : (v : Σ Nat (\y => Fin y -> x .req)) -> All x.res (tyListToList v) -> (val : Fin (v.π1)) -> x.res (v.π2 val)
      bwd (0 ## p2) _ val impossible
      bwd ((S k) ## p2) (x :: xs) FZ = x
      bwd ((S k) ## p2) (x :: xs) (FS v) = bwd (k ## p2 . FS) xs v

toListAll : ListAllIdris x =%> ListAll x
toListAll =
    tyListIso.from <!
    bwd
    where
      bwd : (v : List x.req) -> ((val : Fin ((listToTyList v).π1)) -> x .res ((listToTyList v).π2 val)) -> All x.res v
      bwd [] f = []
      bwd (y :: (xs)) f = f FZ :: bwd xs (\x => f (FS x))

public export
join' : ListAllIdris (ListAllIdris x) =%> ListAllIdris x
join' =
    flatList <!
    bwd
    where
      bwd : (val : List (List (x .req))) -> All (x .res) (flatList val) -> All (All (x .res)) val
      bwd [] x = []
      bwd (y :: xs) arg = let k : (All x.res y, All x.res (flatList xs))
                              k = splitAt y arg in fst k :: bwd xs (snd k)

public export
join : ListAll (ListAll x) =%> ListAll x
join = let
      pre2 : ListAll (ListAllIdris x) =%> ListAllIdris (ListAllIdris x)
      pre2 = fromListAll {x = ListAllIdris x}

      pre1 : ListAll  (ListAll x) =%> ListAll (ListAllIdris x)
      pre1 = univFunctor {a = ListCont} fromListAll

    in pre1 |> pre2 |> join' |> toListAll

public export
ListAllIdrisFunctor : {x, y : Container} -> x =%> y -> ListAllIdris x =%> ListAllIdris y
ListAllIdrisFunctor mor =
    (map mor.fwd) <! bwd
    where
      bwd : (v : List (x .req)) -> All (y .res) (mapImpl (mor.fwd) v) -> All (x .res) v
      bwd [] x = []
      bwd (x :: xs) (y :: ys) = mor.bwd x y :: bwd xs ys

```
