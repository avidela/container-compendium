<!-- idris
module Data.Container.Coproduct

import Data.Container.Definition
import Data.Container.Category
import Data.Container.Morphism.Definition
import Data.Container.Morphism.Eq

import public Data.Coproduct

import Data.Category.Bifunctor

import Proofs

||| The coproduct on containers
public export
-->

### The coproduct of containers

When given two container that represent query and responses,
we can build a new container that represents the choice of either
picking the first interaction mode, or the second. This way of
combining two containers is the essence of the coproduct.

```idris
(+) : (c1, c2 : Container) -> Container
(+) c1 c2 =
  (q : c1.request + c2.request) !> choice c1.response c2.response q
```

The choice of two containers is functorial, that is, given two
lenses `a =%> a'` and `b =% b', we can build the lens
`a + b =%> a' + b'`

<!-- idris
public export
-->
```idris
(~+~) : a =%> a' -> b =%> b' -> (a + b) =%> (a' + b')
(~+~) x y =
  (bimap x.fwd y.fwd) <!
  (\case (<+ w) => x.bwd w
         (+> w) => y.bwd w)
```

The above operators can be read outloud with the names
`alternative` and `alternatively`.

<!-- idris
public export
-->
```idris
alternative : (c1, c2 : Container) -> Container
alternative c1 c2 = c1 + c2
```
<!-- idris
public export
-->
```idris
alternatively  : a =%> a' -> b =%> b' -> (a + b) =%> (a' + b')
alternatively = (~+~)
```

Coproducts form a bifunctor on the category `Cont` using the
above map on objects and on morphisms

<!-- idris
public export
-->
```idris
CoprodContBifunctor : Bifunctor Cont Cont Cont
CoprodContBifunctor = MkFunctor
  (Product.uncurry (+))
  (\_, _, m => m.π1 ~+~ m.π2)
  (\(x1 && x2) => depLensEqToEq $ MkDepLensEq bifunctorId'
      (\case (+> v) => \_ => Refl
             (<+ v) => \_ => Refl
      )
  )
  (\a, b, c, (f1 && f2), (g1 && g2) => depLensEqToEq $ MkDepLensEq
      (bimapCompose)
      (\case (<+ x) => \_ => Refl
             (+> x) => \_ => Refl))
```

Coproducts admit a diagonal map

<!-- idris
public export
-->
```idris
dia : a + a =%> a
dia =
  dia <!
  (\case (+> r) => id
         (<+ l) => id)

public export
dia3 : a + a + a =%> a
dia3 = dia ~+~ identity a ⨾ dia

public export
One : Container
One = Unit :- Void
```
