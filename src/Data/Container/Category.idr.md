
## Categorical structures on containers

Containers and their morphisms form different categorical structures, this section will
show them and also display their proofs. First we ensure that containers and the containers
morphisms we defined form a category. Then we prove that the operations on containers
we're interested in form bifunctors and monoids in the category of containers,
finally I repeat the same work for the category of containers and cartesian morphisms.

<!-- idris
module Data.Container.Category

import public Data.Category
import Data.Category.Product
import Data.Category.Monoid
import Data.Category.Bifunctor
import Data.Category.NaturalTransformation
import public Data.Container.Definition
import public Data.Container.Morphism.Definition
import public Data.Container.Morphism.Eq
import Data.Sigma

import Proofs

import Syntax.PreorderReasoning

%hide Prelude.(&&)
%hide Sigma.proj1
%hide Sigma.proj2
%hide Prelude.Ops.infixl.(*>)
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)
-->

### Containers and their morphisms form a category

The first property about containers we need to establish is that they form a category $Cont$ where
the objects are containers, and the morphisms are [container morphisms](../Morphism.idr.md)
given by the forward and backward map.

```idris
composeIdRight : (f : a =%> b) -> f ⨾ identity b ≡ f
composeIdRight (fwd <! bwd) = Refl

composeIdLeft : (f : a =%> b) -> identity a ⨾ f ≡ f
composeIdLeft (fwd <! bwd) = Refl

proveAssoc : (f : a =%> b) -> (g : b =%> c) -> (h : c =%> d) ->
             f ⨾ (g ⨾ h) ≡ (f ⨾ g) ⨾ h
proveAssoc (fwd0 <! bwd0) (fwd1 <! bwd1) (fwd2 <! bwd2) = Refl

-- The category of containers with dependent lenses as morphisms
public export
Cont : Category Container
Cont = NewCat
  { objects = Container
  , morphisms = (=%>)
  , identity = (\x => identity x)
  , composition = (⨾)
  , identity_right = composeIdRight
  , identity_left = composeIdLeft
  , compose_assoc = proveAssoc
  }
```



