## The list monad on containers

List as a container is also a monad on containers. We ensure this fact by providing proof that it forms a monad in the category of containers.

```idris
module Data.Container.List.Monad

import Data.Category
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.Monad
import Data.Category.NaturalTransformation

import Data.Container
import Data.Container.Distrib
import Data.Container.Category
import Data.Container.Morphism
import Data.Container.Cartesian
import Data.Container.Descriptions.Maybe
import Data.Container.Descriptions.List

import Data.Fin
import Data.List
import Data.Sigma
import Data.List.Quantifiers
import Data.Iso

import Proofs

import Syntax.PreorderReasoning

%hide Prelude.Ops.infixl.(*>)
%default total
```

The first step is to define the unit of the monad. This will be a container morphism `x =%> List x`. In what follows, I will be using the `AllC` functor since it uses a plain inductive definition which amends itself well to proofs. The same could be done with the plain list container applied to the `#>` action but the proofs would be more tedious, and since we have proven they are the same, there is no reason to make our lives harder than they need to be.
```idris
-- pure but for the List monad on containers
listUnit : (x : Container) -> x =%> AllC x
listUnit x = pure'


0 unitCommutes : (0 x, y : Container) -> (m : x =%> y) ->
      let 0 top : x =%> AllC x
          top = listUnit x

          0 bot : y =%> AllC y
          bot = listUnit y

          0 left : x =%> y
          left = m

          0 right : AllC x =%> AllC y
          right = (AllCFunctorFunc).mapHom _ _ m

          0 comp1 : x =%> AllC y
          comp1 = top ⨾ right

          0 comp2 : x =%> AllC y
          comp2 = left ⨾ bot

      in comp1 === comp2
unitCommutes x y m =
  cong2Dep'
    (<!)
    Refl
    (funExtDep $ \vc => funExt $ \[xc] => Refl)

pureNT : Functor.idF Cont =>> AllCFunctorFunc
pureNT = MkNT
  listUnit
  unitCommutes
```

The program above gives us a natural transformation between the identity
functor and the list functor, using the unit defined here. This first
natural transformation gives us the `pure` part of the monad. Next is `join`.

```idris
listJoin : (x : Container) -> AllC (AllC x) =%> AllC x
listJoin x = join'

parameters (0 x, y : Container) (m : x =%> y)

  LL : Container -> Container
  LL = (AllCFunctorFunc !*> AllCFunctorFunc).mapObj

  0 top : LL x =%> AllC x
  top = listJoin x

  0 bot : LL y =%> AllC y
  bot = listJoin y

  0 left : LL x =%> LL y
  left = (AllCFunctorFunc !*> AllCFunctorFunc).mapHom _ _ m

  0 right : AllC x =%> AllC y
  right = AllCFunctor m

  0
  comp1 : LL x =%> AllC y
  comp1 = top ⨾ right

  0
  comp2 : LL x =%> AllC y
  comp2 = left ⨾ bot

  %unbound_implicits off
  fwdEq : (val : List (List x.req)) ->
          comp1.fwd val === comp2.fwd val
  fwdEq [] = Refl
  fwdEq (z :: xs) =
        Calc $
          |~ listMap (m .fwd) (z ++ flatList xs)
          ~~ listMap (m .fwd) z ++ listMap m.fwd (flatList xs)
          ...(listMapAppend)
          ~~ listMap (m .fwd) z ++ flatList (listMap (listMap (m .fwd)) xs)
          ...(cong (listMap m.fwd z ++ ) (fwdEq xs))


  -- 0
  -- bwdEq : (v : List (List x.req)) ->
  --         (w : All y.res (listMap m.fwd (flatList v))) ->
  --          ListAllEq _ _ (comp1.bwd v w) (comp2.bwd v w)
  -- bwdEq [] w = AllEmpty
  -- bwdEq (v :: xs) w =
  --   let aaaa =
  --       AllCons v {l1 = xs, l2 = xs} {p = All x.res}
  --         (fst (splitLocal v (mapProp (m .bwd) (v ++ flatList xs) w)))
  --         ?ccc
  --         ?ddd
  --         ?eee
  --         {p1 = joinBwd xs (snd (splitLocal v (mapProp (m .bwd) (v ++ flatList xs) w)))}
  --         -- {p2 = mapProp (mapProp (m .bwd)) xs (joinBwd (listMap (listMap (m .fwd)) xs) (snd (splitLocal (listMap (m .fwd) v) w)))}
  --       0 bb = (comp1.bwd (v :: xs) w)
  --       0 cc = (comp2.bwd (v :: xs) )
  --   in ?ending


-- ListAllEq
--     (x :: xs) (x :: xs)
--     (fst (splitLocal x (mapProp (m .bwd) (x ++ flatList xs) w)) :: joinBwd xs (snd (splitLocal x (mapProp (m .bwd) (x ++ flatList xs) w))))
--     (mapProp (m .bwd) x (fst (splitLocal (listMap (m .fwd) x) w)) :: mapProp (mapProp (m .bwd)) xs (joinBwd (listMap (listMap (m .fwd)) xs) (snd (splitLocal (listMap (m .fwd) x) w))))

  -- bwdEq [] w = Refl
  -- bwdEq ([] :: xs) w = ?aeg -- cong ([] ::) (bwdEq xs w)
  -- bwdEq ((z :: ys) :: xs) (w1 :: w2) =
  --   let z1 := m.bwd z w1
  --       zn := fst (splitLocal ys (mapProp (m .bwd) (ys ++ flatList xs) w2))
  --       zm := joinBwd xs (snd (splitLocal ys (mapProp (m .bwd) (ys ++ flatList xs) w2)))
  --       zr := mapProp {p = y.res} m.bwd ys (fst (splitLocal (listMap (m .fwd) ys) {ys = listMap m.fwd (flatList xs)} ?looool))
  --       rw : All (y .res) (listMap (m .fwd) ys ++ listMap (m .fwd) (flatList xs))
  --       rw = rewrite__impl (All y.res) (sym $ listMapAppend {f = m.fwd}) w2
  --       wm : (All (y .res) (listMap (m .fwd) ys), All (y .res) (listMap (m .fwd) (flatList xs)))
  --       wm = splitLocal _ rw
  --       rec := bwdEq xs (snd wm)
  --       prfff := mapPropAppend ys (fst wm) (flatList xs) {p = y.res, f = m.fwd} ?ahu m.bwd
  --       wEnd : All (y .res) (listMap (m .fwd) ys ++ flatList (listMap (listMap (m .fwd)) xs))
  --       wEnd = rewrite__impl (All y.res) (trans (cong (listMap (m .fwd) ys ++) (sym $ flatListMap m.fwd _)) (sym listMapAppend)) w2
  --   in cong2 (::) (cong (m.bwd z w1 :: ) (Calc $
  --               |~ fst (splitLocal ys (mapProp (m .bwd) (ys ++ flatList xs) w2))
  --               -- ~~ fst (splitLocal ys ?)
  --               -- ... (cong (fst . splitLocal ys . mapProp (m .bwd) (ys ++ flatList xs)) ?error2)
  --               ~~ mapProp (m .bwd) ys (fst (splitLocal (listMap (m .fwd) ys) wEnd))
  --               ... (?error)
  --             ))
  --  --  (cong (m.bwd z w1 ::) (Calc $ |~
  --  --    (m .bwd z w1 :: fst (splitLocal ys (mapProp (m .bwd) (ys ++ flatList xs) w2))) :: joinBwd xs (snd (splitLocal ys (mapProp (m .bwd) (ys ++ flatList xs) w2)))
  --  --  ~~ ?bbb
  --  --  ...(?ccc)))
  --   ?endd

--   bwdEq : (a : List (List x.req)) ->
--           (b : All y.res (listMap m.fwd (flatList a))) ->
--           ListAllEq a a (joinBwd {x} a
--               (mapProp {a = x.req, b = y.req, q = x.res, p = y.res, f = m.fwd} m.bwd (flatList a) b))
--               (let kk = (mapProp {a = List x.req}
--                                  ?fnbwd -- (mapProp {a = x.req, b = y.req, f = m.fwd, p = y.res, q = x.res} (m .bwd))
--                                  a (joinBwd (listMap (listMap (m .fwd)) a) b))
--                in ?bu)


  0
  joinCommutes :
      comp1 === comp2
  joinCommutes = depLensEqToEq $ MkDepLensEq
    fwdEq
    (\a, b =>
        let lhs = joinBwd a (mapProp (m .bwd) (flatList a) b)
            rhs = mapProp (mapProp (m .bwd)) a (joinBwd (listMap (listMap (m .fwd)) a) ?arg24444)
        in ?arggg)

joinNT : (AllCFunctorFunc !*> AllCFunctorFunc) =>> AllCFunctorFunc
joinNT = MkNT
  listJoin
  ?joinCommuteSqua
```

The proof is a bit more complicated but the result is a natural transformation $List ∘ List \Rightarrow List$ which represents the multiplication of the monad.

Equipped with those two natural transformation we build the monad on containers by building a value of [`Monad`](../../Category/Monad.idr.md)

```idris
ListMaybeMonad : Monad Cont
ListMaybeMonad = MkMonad
    AllCFunctorFunc
    pureNT
    joinNT

```
