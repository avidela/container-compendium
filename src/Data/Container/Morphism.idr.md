
```idris
module Data.Container.Morphism

import public Data.Container.Definition
import public Data.Container.Tensor
import public Data.Container.Sequence
import public Data.Container.Extension
import public Data.Container.Morphism.Definition
import public Data.Container.Morphism.Eq
import public Data.Ops
import public Data.Category.Ops
import Data.Coproduct
import Data.Product
import Data.Alg
import Data.Sigma

import Control.Order
import Control.Relation

```

Using the monoidal operations on containers we can build complex morphisms by combining them
together. For example, there is a parallel composition of container morphisms that makes use
of the $⊗$-product on containers for its domain and codomain. Another way of looking at this
morphism is that it's the bifunctor related to the $⊗$ monoidal product.

Because $Cont$ has a lot of monoidal products, we get a bifunctor for all of them, here is the
one for the product on containers.
A fun fact about lenses and their dependent counterparts, you can build a lens out of a plain
function `f : a -> b`. For this, consider the morphism `(a !> b) =%> End`, it has a forward
map `a -> ()` and a backward map `(x : a) -> () -> b x`, both of which are easy to build from
our initial function `f`. And we can do the reverse as well, given a morphism into the unit,
we can extract a function that goes from the shape to the positions of the container in its
domain.

```idris
public export
getbwd : (lens : a =%> b) -> (x : a.req) -> b.res (lens.fwd x) -> a.res x
getbwd lens = lens.bwd

public export
embed : {0 a : Type} -> {0 b : a -> Type} -> (f : (x : a) -> b x) ->
    ((!>) a b) =%> End
embed f = (\_ => ()) <! (\x, _ => f x)

public export
extract : ((!>) a b) =%> End -> (x : a) -> b x
extract c x = c.bwd x ()
```

Given all of the above, there is a number of fun and interesting operations we can
perform involving container morphisms that are similar to operations on more
conventional morphisms like functions on sets.


#### `○` distributes over `⊗`

This is going to come in handy for composing interactive systems.

```idris

-- ○ distributes over ⊗
public export
distribProd : (a1 ○ a2) ⊗ (b1 ○ b2) =%> (a1 ⊗ b1) ○ (a2 ⊗ b2)
distribProd =
    (\x => MkEx (x.π1.ex1 && x.π2.ex1) (\vx => x.π1.ex2 vx.π1 && x.π2.ex2 vx.π2 )) <!
    (\x, y => y.π1.π1 ## y.π2.π1 && y.π1.π2 ## y.π2.π2)
```
#### `2 * x = x + x` isomorphism

I'm not going to the implement the entire isomorphism but it works™.
This is useful for the demos of morphisms as servers.

```
-- x + x -> 2 * x
twice : (Bool :- ()) ⊗ x =%> x + x
twice = (\v => if v.π1 then <+ v.π2 else +> v.π2) <!
                   (\case (True && x) => (() &&)
                          (False && x) => (() &&))
```

## Conclusion

I have not written every single monoidal operation on containers, nor all of their properties, but only the ones that I have come across and used for the rest of this work. The realm of containers and their related categories seem to be infinite and there has been decades of mathematical research studying their different aspects. The one thing to take away is that this is just the start and there is already a lot we can do. The natural next step is to look at the [categorical properties of containers](Category.idr.md). If you are more interested in applications, maybe you can skip to how I use containers to write [servers](../../Interactive/Server.idr.md).
