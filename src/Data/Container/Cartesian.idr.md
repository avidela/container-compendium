<!-- idris
module Data.Container.Cartesian

import Data.Container.Category
import Data.Container.Extension
import Data.Container.Definition

import Data.Coproduct
import Data.Iso
import Data.Sigma

import Control.Relation

import Proofs

%default total

||| Cartesian dependent lenses. Their forward part is the same as a dependent lens
||| But their backward part is an isomorphism.
public export
-->
### Cartesian Container Morphisms

Cartesian container morphisms come from Abbott (do they?) where the name was given by the fact that the something something cartesian morphisms pullbacks.

In the programming practice, this means that the backward map of lenses is an _isomorphism_ rather than a one-way morphism. All the naming conventions for cartesian morphisms will make use of a `#` symbol, as a symbol reminiscent of a the geometrical representation of the cartesian plane. While there is no direct relation between the two, I thought it would make for a helpful mnemonic.

```idris
record (=#>) (a, b : Container) where
  constructor MkCartDepLens
  -- `c` for cartesian forward map
  cfwd : a.req -> b.req
  -- `c` for cartesian backward map
  cbwd : (x : a.req) -> Iso (b.res (cfwd x)) (a.res x)
```

Because the backward map is now an isomorphism, I define some helper functions to extract the morphisms each way.

```idris
public export
(.cbwdTo) : (m : a =#> b) -> (x : a.req) -> b.res (m.cfwd x) -> a.res x
(.cbwdTo) m x y = (m.cbwd x).to y

public export
(.cbwdFrom) : (m : a =#> b) -> (x : a.req) -> a.res x -> b.res (m.cfwd x)
(.cbwdFrom) m x y = (m.cbwd x).from y
```


Like all other structures, I define a bespoke record that keeps track of what makes two cartesian morphism equal, to help with the writing of proofs about them.

```idris
public export
record CartDepLensEq (a, b : dom =#> cod) where
  constructor MkCartDepLensEq
  fwdEq : (x : dom.req) -> a.cfwd x === b.cfwd x
  bwdEq : (x : dom.req) ->
          let 0 p1 : Iso (cod.res (a.cfwd x)) (dom.res x)
              p1 = a.cbwd x
              0 p2 : Iso (cod.res (a.cfwd x)) (dom.res x)
              p2 = replace {p = \arg => Iso (cod.res arg) (dom.res x)} (sym $ fwdEq x) (b.cbwd x)
          in IsoEq p1 p2

export infix 0 ≡#>≡

public export
(≡#>≡) : (a, b : dom =#> cod) -> Type
(≡#>≡) = CartDepLensEq

export
0 cartEqToEq : CartDepLensEq a b -> a === b
cartEqToEq {a = (MkCartDepLens fwd1 bwd1)} {b = (MkCartDepLens fwd2 bwd2)} (MkCartDepLensEq fwdEq bwdEq) =
  cong2Dep' MkCartDepLens (funExt fwdEq) (funExtDep $ \x => fromIsoEq _ _ (bwdEq x))
```

Next, we should also make sure that our definition of cartesian container forms a category, for this we define identity and composition.

```idris
||| Composition of cartesian lenses
public export
(|#>) : {0 a, b, c : Container} -> a =#> b -> b =#> c -> a =#> c
(|#>) m1 m2 =
  let f1 : a.req -> b.req
      f1 = m1.cfwd
      b1 : (x : a.req) -> Iso (b.res (f1 x)) (a.res x)
      b1 = m1.cbwd
      f2 : b.req -> c.req
      f2 = m2.cfwd
      b2 : (x : b.req) -> Iso (c.res (f2 x)) (b.res x)
      b2 = m2.cbwd
  in MkCartDepLens (f2 . f1)
    (\x => transIso (b2 (f1 x)) (b1 x))

public export
identity : (0 v : Container) -> v =#> v
identity v = MkCartDepLens id (\x => identityIso (v.res x))
```

The following lemmas are used to prove that identity respects composition

```idris
export
cartFwdEq : (f : a =#> b) -> (g : b =#> c) -> (x : Ex a a'.req) ->
        (f |#> g) .cfwd (x .ex1) = g .cfwd (f .cfwd (x .ex1))
cartFwdEq f g x = Refl

export
cartBwdEq : (f : a =#> b) -> (g : b =#> c) -> (x : Ex a a'.req) ->
        (y : c .res ((f |#> g) .cfwd (x .ex1))) ->
        ((f |#> g).cbwd (x .ex1)).to y === (f.cbwd x.ex1).to ((g.cbwd (f.cfwd x.ex1)).to y)
cartBwdEq f g x y = Refl
```
