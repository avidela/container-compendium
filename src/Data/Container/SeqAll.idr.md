<!-- idris
module Data.Container.SeqAll

import Data.Container.Definition
import Data.Container.Morphism.Definition
import Data.Container.Extension

-- The new one
public export
-->

### Universal composition

Existential composition results in a container which responses are `Any`s, but there
is another operation where the responses are `All`s as in `List.All`, a predicate
that holds for all values of its index. This operator is defined in the same way as
`c1 ○ c2` but uses a Π-type instead of Σ. While existential composition is a
monoidal product, universal composition is an
[action](src/Data/Category.Action.idr.md).

```idris
(#>) : Container -> Container -> Container
(#>) c1 c2 = (x : Ex c1 c2.req) !>
             ((val : c1.res x.ex1) -> c2.res (x.ex2 val))
```
