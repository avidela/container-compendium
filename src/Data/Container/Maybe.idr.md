# Properties on the Maybe container

The `Maybe` container has been
defined in the [Data.Container.Descriptions.Maybe](src/Data/Container/Descriptions/Maybe.idr.md)
module. This module will make use of it to prove that, even in the world of containers, it
is true that `Maybe x ~ 1 + x`.

```idris
module Data.Container.Maybe

import public Data.Container.Category
import Data.Container
import Data.Container.Cartesian
import Data.Container.Cartesian.Category
import Data.Container.Descriptions.Maybe
import Data.Category.Bifunctor
import Data.Category.Functor
import Data.Category.NaturalTransformation
import Data.Sigma
import Data.Coproduct
import Data.Iso

import Proofs

%default total
```

## Some context

In regular programming, the `Maybe` type defines program that may fail, and provides a way to handle errors when they arise. `Maybe` used as a monad, provides lots of ways to combine multiple failing programs and combine with other monads such as `List` thanks to their distributive property.

The `Maybe` type is traditionally defined as `data Maybe x = Nothing | Just x`, it is often also called `Option` in other languages. As the definition suggests, a value of `Maybe x` can either be `Nothing`, a value with no content, or `Just x`, a value that stores an `x`.

Crucially, this type is _isomorphic_ to the type `1 + x` where `+` is the coproduct on types, and `1` the unit type. They are called `Either` and `Unit` in Idris, respectively. Put together, it means `Maybe x ~ Either Unit x`.

Back to our world of containers, in addition to our `Maybe` container, we also have a coproduct on containers and a unit. Is it true that `Maybe x ~ 1 + x` when x is a container, `Maybe` is
a functor on containers, `1` is the unit on containers and `+` is the coproduct on containers?

Thankfully, out intuition is rewarded and the isomorphism exists. Because now both `Maybe` and
`1 +` are functors on container, the relationship between them is a _natural isomorphism_. To
reach it, we first need to define our two functors:

```idris
-- The functor Maybe ○ x
public export
MaybeFunctor : Functor Cont Cont
MaybeFunctor = applyBifunctor {a = Cont, b = Cont} MaybeCont ComposeContBifunctor

-- The functor 1 + x
public export
OnePlusFunctor : Functor Cont Cont
OnePlusFunctor = applyBifunctor {a = Cont, b = Cont} One CoprodContBifunctor
```

Here, I chose to define them as partially applied bifunctors since both `+` and `○` are bifunctors.
The `Maybe` functor is the maybe _container_ partially applied to `○` and the `1 +` functor
is the identity container (`One = MkCont Unit (const Void)`) partially applied to the `+`
bifunctor.

Next, we write a couple of lemma that we already know about from the corresponding isomorphism on types:

```idris
-- Maybe x -> 1 + x
maybeCoprod : MaybeType x -> () + x
maybeCoprod (MkEx False p2) = <+ ()
maybeCoprod (MkEx True p2) = +> p2 ()

-- 1 + x -> Maybe x
coprodMaybe : () + x -> MaybeType x
coprodMaybe (<+ y) = Nothing
coprodMaybe (+> y) = Just y

coMayInverse : (vx : Ex MaybeCont v) -> (coprodMaybe (maybeCoprod vx)) === vx
coMayInverse (MkEx False p) = cong (MkEx False) (allUninhabited _ _)
coMayInverse (MkEx True p) = cong (MkEx True) (funExt $ \() => Refl)

mayCoInverse : (vx : _) -> (maybeCoprod (coprodMaybe vx)) === vx
mayCoInverse (<+ ()) = Refl
mayCoInverse (+> x) = Refl
```

Those four definitions define the isomorphism between the `Maybe` functor on types and the
`1 +` functor on types. We are going to use them for the container counterparts.

Like for types, we write two morphisms `Maybe x -> 1 + x` and `1 + x -> Maybe x`, but since
our morphisms are in the category of containers the arrow is now a dependent lens:

```idris
-- Maybe x -> 1 + x as a dependent lens
conv1 : (x : Container) -> MaybeCont ○ x =%> One + x
conv1 x =
    (maybeCoprod {x = x.req}) <!
    (\case (MkEx False y) => \x => void x
           (MkEx True y) => \z => () ## z)

-- 1 + x -> Maybe x as a dependent lens
conv2 : (x : Container) -> One + x =%> MaybeCont ○ x
conv2 x =
  (coprodMaybe {x=x.req}) <!
  (\case (<+ x) => absurd . π1
         (+> x) => π2)
```

With our two morphisms we now need to write the natural isomorphism. To do so, we need
to provide a natural transformation, and prove that its _component_ forms an isomorphism.

I chose to define the `Maybe x -> 1 + x` natural transformation on containers morphisms
but the proof could have worked by using the other one as well.

```idris
theThing : {0 a, b : Container} -> (m : a =%> b) -> (v : Ex MaybeCont a.req) ->
           (conv1 a ⨾ mapHom OnePlusFunctor _ _ m).fwd v
           ===
           (mapHom MaybeFunctor a b m ⨾ conv1 b).fwd v
theThing m (MkEx False ex2) = Refl
theThing m (MkEx True ex2) = Refl

theThingBwd : {0 a, b : Container} -> (m : a =%> b) ->
              (v : Ex MaybeCont a.req) ->
              (y : (One + b).res ((conv1 a ⨾ mapHom OnePlusFunctor _ _ m).fwd v)) ->
              let 0 p1, p2 : (MaybeCont ○ a).res v
                  -- p1 = (conv1 a ⨾ mapHom OnePlusFunctor a b m).bwd v y
                  -- p2 = (mapHom MaybeFunctor a b m ⨾ conv1 b).bwd v
                  --          (replace {p = (One + b).res} (theThing m v) y)
              in Type -- p1 === p2
                              {-
theThingBwd m (MkEx False ex2) y = absurd y
theThingBwd m (MkEx True ex2) y = Refl

onePlusSquare : (m : a =%> b) ->
    conv1 a ⨾ mapHom OnePlusFunctor _ _ m
    <%≡%>
    mapHom MaybeFunctor a b m ⨾ conv1 b
onePlusSquare m = MkDepLensEq (theThing m) (theThingBwd m)

onePlusMaybe : MaybeFunctor ==>> OnePlusFunctor
onePlusMaybe = MkNT
  conv1
  (\a, b, m => depLensEqToEq (onePlusSquare m))


{-
     0 bwdEq : {a, b : Container} -> {m : a =%> b} -> (v : (MaybeCont ○ a).req) -> let
              m1 : MaybeCont ○ a =%> One + b
              m1 = (MaybeFunctor).mapHom _ _ m ⨾ conv1 b
              m2 : MaybeCont ○ a =%> One + b
              m2 = conv1 a ⨾ (OnePlusFunctor).mapHom _ _ m
           in (y : (One + b).res (m1.fwd v)) -> let
              0 p1 : (MaybeCont ○ a).res v
              p1 = m1.bwd v y
              0 p2 : (MaybeCont ○ a).res v
              p2 = m2.bwd v (replace {p = (One + b).res}
                                     (sym $ fwdEq v) y)
           in p1 === p2
     bwdEq (MkEx True ex2) y = ?naa
     {-
```

Now we need to check that `conv1` and `conv2` form an isomorphism. The proofs are a bit hard to read but we can be assured that we didn't do anything wrong by creating a value of type `=~=`, the type for natural isomorphisms. To construct it, we need to supply the 4 things
we mentioned above, a natural transformation (`onePlusMaybe`), a morphism going the other way as the component (`conv2`), and two proofs that the component and its inverse are
identities `conv2 ⨾ conv1 = id` and `conv1 ⨾ conv2 = id`.

```idris
-- The natural isomorphism between 1 + x and Maybe ○ x in the category of containers
OnePlusMaybeIso : MaybeFunctor =~= OnePlusFunctor
OnePlusMaybeIso = MkNaturalIsomorphism
  onePlusMaybe
  conv2
  (\x => depLensEqToEq (conv21 x))
  (\x => depLensEqToEq (conv12 x))
  where
    0 conv12 : (x : Container) -> conv2 x ⨾ conv1 x <%≡%> identity (One + x)
    conv12 x = MkDepLensEq mayCoInverse
                           (\case (<+ v) => \y => absurd y
                                ; (+> v) => \_ => Refl)

    0 trueApp : {0 a : Type} -> {0 f : IsTrue True -> a} ->
              (\x : IsTrue True => f x) === (\_ : IsTrue True => f T)
    trueApp = funExt $ \T => Refl

    0 eqBwd : {c : Container} -> (v : Ex MaybeCont c.req) ->
              (y : (MaybeCont ○ c).res (coprodMaybe (maybeCoprod v))) ->
          let 0 p1 : (MaybeCont ○ c).res v
              p1 = ((conv1 c) ⨾ (conv2 c)).bwd v y
              0 p2 : (MaybeCont ○ c).res v
              p2 = (replace {p = (MaybeCont ○ c).res} (coMayInverse v) y)
          in p1 === p2
    eqBwd (MkEx False ex2) y = absurd y.π1
    eqBwd (MkEx True ex2) (T ## y) = rewrite trueApp {f = ex2} in Refl

    0 conv21 : (c : Container) ->
               conv1 c ⨾ conv2 c <%≡%> identity (MaybeCont ○ c)
    conv21 c = MkDepLensEq coMayInverse eqBwd

```

## Wait but there are two Maybe on containers

```idris
-- The functor Maybe ○ x
public export
MaybeFunctorAll : Functor Cont Cont
MaybeFunctorAll = applyBifunctor {a = ContCartCat, b = Cont} MaybeCont ContinuationBifunctor
```
