<!-- idris
module Data.Container.Definition

import Data.Boundary
import Data.Category.Ops
import Data.Sigma

public export
-->
## Containers

^e44b25

The name "container" might be a bit misleading. For programmers, it refers to a data structure
that carries other elements. A tree, a list, a dictionnary, all those are "containers", but
what we are looking at here are _polynomial functors_ in the category of types. Concretely,
this means that they _describe_ data structures such as lists and dictionaries. But they
aren't lists or dictionaries themselves.

The definition of containers is deceptively simple. It's a pair of a type and an other
type indexed by the first. Abbott writes $(S \rhd P)$ for the container with shapes $S : Set$ and positions $P : S \to Set$, in this work, because we focus on the interactive nature of containers, we are going to call the indexing part the _request_ and the indexed part the _response_.

```idris
record Container where
  constructor (!>)
  ||| Shapes
  request : Type
  ||| Positions
  response : request -> Type
```

We use a visually similar operator `!>` as a constructor and even allow it to be binding so that we can write `(s : S) !> P s`.
<!-- idris

%pair Container request response

public export
(.message) : Container -> Type
(.message) c = c.request
public export
(.msg) : Container -> Type
(.msg) c = c.request
public export
(.req) : Container -> Type
(.req) c = c.request
public export
(.res) : (c : Container) -> c.req -> Type
(.res) c = c.response
-->

We also define a couple of smart constructors that simplify the declaration of containers.
The first one allows to define a container with a set of responses that does not depend on
the request.

<!-- idris
public export
-->
```idris
(:-) : Type -> Type -> Container
(:-) req res = (x : req) !> res
```

There are also containers that not only have a second projection that does not depend on the
first, they even have both component be the same.

<!-- idris
public export
-->
```idris
Const : Type -> Container
Const ty = ty :- ty
```

There is a number of container with special meanings, the `End` container is a monoidal
unit, and we use it to terminate sequences of operations. The reason for using it will
show up in the chapter about writing programs with dependent lenses.

<!-- idris
public export
-->
```idris
End : Container
End = Const Unit
```
