```idris
module Data.Container.Lift

import Data.Container
import Data.Container.Monad
import Data.Container.Category
import Data.Container.Morphism
import Data.Container.Morphism.Eq

import Data.Category
import Data.Category.Functor
import Data.Category.Monad
import Data.Category.NaturalTransformation

%hide Prelude.Ops.infixl.(|>)
```
## Properties of Lifts

This is about `Lift` a combinator on containers that lifts any functor in Set into a functor in
the category of containers. Given a functor in the category of types and functions, we obtain
a functor in the category of containers.

```idris

-- functorial Lift
public export
(•) : (f : Type -> Type) -> Container -> Container
(•) f c = (x : c.req) !> f (c.res x)

public export
Lift : (f : Type -> Type) -> Container -> Container
Lift = (•)

parameters (f : Functor Set Set)

  public export
  f_hom : {a, b : Container} -> a =%> b ->  Lift f.mapObj a =%> Lift f.mapObj b
  f_hom c = c.fwd <! (\x => f.mapHom _ _ (c.bwd x))

  public export 0
  f_pres_id : (a : Container) -> f_hom (identity {a}) <%≡%> identity {a = Lift f.mapObj a}
  f_pres_id a = MkDepLensEq (\_ => Refl) (\x, y => let pp = f.presId (a.res x) in app _ _ pp y)

  public export 0
  f_pres_comp : (a, b, c : Container) -> (f1 : a =%> b) -> (f2 : b =%> c) ->
                  f_hom ((f1 ⨾ f2) {a, b, c}) <%≡%>
                  (f_hom f1 ⨾ f_hom f2)
                    {a = Lift f.mapObj a, b = Lift f.mapObj b, c = Lift f.mapObj c}
  f_pres_comp a b c f1 f2 = MkDepLensEq
      (\_ => Refl)
      (\v, y => let hh = f.presComp _ _ _ (f2.bwd ((f_hom f1).fwd v)) (f1.bwd v) in app _ _ hh y)

  public export
  LiftIsFunctor : Functor Cont Cont
  LiftIsFunctor = MkFunctor
    (Lift f.mapObj)
    (\_, _, m => f_hom m)
    (\x => depLensEqToEq $ f_pres_id x)
    (\a, b, c, f, g => depLensEqToEq $ f_pres_comp a b c f g)

parameters (m : Monad' Set)

  LiftFunctor' : Functor (Cont).op (Cont).op
  LiftFunctor' = MkFunctor
      (Lift m.endo.mapObj)
      (\a, b => f_hom m.endo)
      (\x => depLensEqToEq $ f_pres_id m.endo x)
      (\a, b, c, f, g => depLensEqToEq $ f_pres_comp m.endo c b a g f)

  base_counit : (v : Container) -> Lift m.endo.mapObj v =%> v
  -- base_counit v = id <! (\x, y => m.component.comp (v.res x) y )

  base_comult : (v : Container) -> Lift m.endo.mapObj v =%> Lift m.endo.mapObj (Lift m.endo.mapObj v)
  -- base_comult v = MkMorphism id (\x => m.μ.comp (v.res x))

  identity_left : (v : Container) ->
                  base_comult v ⨾ base_counit (Lift m.endo.mapObj v)
                  <%≡%>
                  identity {a = Lift m.endo.mapObj v}
  -- identity_left v = MkDepLensEq (\_ => Refl) (\vx, y => let
  --                               m' = m.identityLeft (v.res vx)
  --                               in app _ _ m' y)

  identity_right : (v : Container) ->
                   base_comult v ⨾ f_hom m.endo (base_counit v)
                   <%≡%>
                   identity {a = (Lift m.endo.mapObj v)}
--   identity_right v = MkDepLensEq (\_ => Refl) (\vx, vy => let
--                                  m' = m.identityRight (v.res vx)
--                                  in app _ _ m' vy)

  monad_square : (v : Container) ->
                 let Fn : Container -> Container
                     Fn = Lift m.endo.mapObj
                     top : Fn v =%> Fn (Fn v)
                     top = base_comult v
                     right : Fn (Fn v) =%> Fn (Fn (Fn v))
                     right = f_hom m.endo (base_comult v)
                     left : Fn v =%> Fn (Fn v)
                     left = base_comult v
                     bot : Fn (Fn v) =%> Fn (Fn (Fn v))
                     bot = base_comult (Fn v)
                  in top ⨾ right <%≡%> left ⨾ bot
--   monad_square v = MkDepLensEq (\_ => Refl)
--       (\vx, vy => app _ _ (m.square (v.res vx)) vy)

  comonad : ContainerComonad (LiftIsFunctor m.endo)
  comonad = MkContainerComonad
      base_counit
      base_comult
      monad_square
      identity_left
      identity_right
```
