<!-- idris
module Data.Container.Tensor

import Data.Container.Definition
import Data.Container.Category
import Data.Container.Morphism.Definition
import Data.Container.Cartesian

import Data.Product
import Data.Iso

import Data.Category.Bifunctor
import Data.Category.Monoid
import Data.Category.NaturalTransformation
import Data.Category.Bicategory
import Data.Category.Bicategory.Para

import Proofs

%hide Prelude.Ops.infixl.(*>)


||| The tensor on containers
||| Idris does not support unicode operator but we can use unicode identifiers with infix notation
public export
-->

### Tensor product

The tensor product of container combines with the cartesian product both the queries and the responses. This is not to be confused with the
cartesian product on containers \*place link\*

```idris
(⊗) : (c1, c2 : Container) -> Container
(⊗) c1 c2 = (x : c1.req * c2.req) !> c1.res x.π1 * c2.res x.π2
```

<!-- idris
public export
-->

The tensor product enjoys some some quite typical properties of products,
for example we can swap the products.
```idris
swap : (x ⊗ y) =%> (y ⊗ x)
swap =
  swap <!
  (\_ => swap)
```

<!-- idris
public export
-->
It is also a _monoidal_ product with a neutral element, the neutral element is the `End` container. We use the following morphism to help reduce churn
introduced by other combinators.

```idris
joinUnit : End ⊗ End =%> End
joinUnit =
  (\_ => ()) <!
  (\_, _ => () && ())
```

To prove it is a monoidal product, we need some more basic facts, for example
that it is a bifunctor. As customary, we introduce a map on morphisms for
the tensor product. This map will be used as a combinator for running two dependent lenses
in parallel.

<!-- idris
public export
-->
```idris
(~⊗~) : (a =%> b) -> (a' =%> b') -> a ⊗ a' =%> b ⊗ b'
(~⊗~) x y = (bimap x.fwd y.fwd) <! (\v, w => x.bwd v.π1 w.π1 && y.bwd v.π2 w.π2)
```

<!-- idris
public export
-->
We also give is a name that's easier to pronounce than `~⊗~`.
```idris
parallel : (a =%> b) -> (a' =%> b') -> a ⊗ a' =%> b ⊗ b'
parallel = (~⊗~)
```

<!-- idris
public export
-->
From this we can build the proof that it forms a bifunctor in $\Cont$
```idris
TensorBifunctor : Bifunctor Cont Cont Cont
TensorBifunctor = MkFunctor
    (uncurry (⊗))
    (\x, y, m => m.π1 ~⊗~ m.π2)
    (\v => depLensEqToEq $ MkDepLensEq
        (\vx => prodUniq vx) (\vx, vy => prodUniq vy)
    )
    (\a, b, c, f, g => Refl)
```

<!-- idris
public export
-->

From this we can build the proof that it is a monoidal product in $\Cont$.
We use the bifunctor just defined `TensorBifunctor`, we call it $F$ in diagrams for brevity.

```idris {class="definition"}
TensorMonoidal : Monoidal Cont
TensorMonoidal = MkMonoidal
    TensorBifunctor
    (DiscreteObj End Cont)
    alpha
    leftUnit
    rightUnit
  where
```

For the tensor to be monoidal, we need to prove that it is associative. We do this by providing a natural isomorphism between the functors $id×F ; F$ and $F × id ; F$. Due to the nature of programming, we need to add an associator to ensure the functors share the same domain and codomain:

- $f1 : Cont × (Cont × Cont) → Cont = id × F ; F$
- $f2 : Cont × (Cont × Cont) → Cont = \mathit{assocR} ; F × id ; F$

The essence of the natural isomorphism for associativity is captured by the diagram:
```tikz
\usepackage{tikz-cd}
\usepackage{amsfonts}
\begin{document}
\begin{tikzcd}
{(x, (y, z))} && {(x, y \otimes z)} \\ {((x, y), z)} \\ {((x\otimes y), z)} && {x \otimes y \otimes z}
\arrow[from=1-1, to=1-3, "id_x \times F_{y, z}"]
\arrow[from=1-1, to=2-1,"assocR"]
\arrow[from=1-3, to=3-3, "F_{x,y\otimes z}"]
\arrow[from=2-1, to=3-1, "F_{x, y}\times id_z"]
\arrow[from=3-1, to=3-3, "F_{x \otimes y, z}"]
\end{tikzcd}
\end{document}
```

The proof itself is routine:

```idris
  alpha : let f1, f2 : Functor (Cont * (Cont * Cont)) Cont
              f1 = ((idF Cont) `pair` TensorBifunctor) !*> TensorBifunctor
              f2 = assocR {a = Cont, b = Cont, c = Cont}
                   !*> ((TensorBifunctor `pair` idF Cont) !*> TensorBifunctor)
          in f1 =~= f2
  alpha = MkNaturalIsomorphism
        (MkNT
            (\v => assocR <! (\x, y => assocL y))
            (\a, b, m => Refl))
        (\v => assocL <! (\x, y => assocR y))
        (\v => cong2Dep
                 (<!)
                 (funExt $ \(a && (b && c)) => Refl)
                 (funExtDep $ \v => funExt $ \(g1 && (g2 && g3)) => Refl))
        (\v => cong2Dep
                 (<!)
                 (funExt $ \((x1 && x2) && x3) => Refl)
                 (funExtDep $ \v => funExt $ \((x1 && x2) && x3) => Refl))
```

We also need proof that the monoidal unit behaves like a unit on both sides of the bifunctor,
again this is done via a natural isomorphism between the identity functor in $\Cont$ and the
functor $f : \Cont → \Cont  = \mathit{unitL} ; I × id_{\Cont} ; F$

The natural isomorphism for left unit is capuring the information in this diagram:

```tikz
\usepackage{tikz-cd}
\usepackage{amsfonts}
\begin{document}
\begin{tikzcd}
x && {(1 , x)} \\ \\
x && {(\mathit{End}, x)}
\arrow["unitL", from=1-1, to=1-3]
\arrow["id"', from=1-1, to=3-1]
\arrow["{I \times id}", from=1-3, to=3-3]
\arrow["F"', from=3-3, to=3-1]
\end{tikzcd}
\end{document}
```

The proof is routine:

```idris
  leftUnit : (Bifunctor.unitL !*>
             (DiscreteObj End Cont `pair` idF Cont) !*>
              TensorBifunctor)
              =~= idF Cont
  leftUnit = MkNaturalIsomorphism
        (MkNT
          (\_ =>
            π2 <! (\a, b => MkUnit && b))
          (\a, b, m => Refl))
        (\_ =>
          (MkUnit &&) <! (\_ => π2))
        (\c => cong2Dep
          (<!)
          (funExt $ \(() && a) => Refl)
          (funExtDep $ \x => funExt $ \(() && b) => Refl))
        (\_ => Refl)
```

We do the same for the right unit:

```tikz
\usepackage{tikz-cd}
\usepackage{amsfonts}
\begin{document}
\begin{tikzcd}
x && {(x , 1)} \\ \\
x && {(x, \mathit{End})}
\arrow["unitR", from=1-1, to=1-3]
\arrow["id"', from=1-1, to=3-1]
\arrow["{id \times I}", from=1-3, to=3-3]
\arrow["F"', from=3-3, to=3-1]
\end{tikzcd}
\end{document}
```


```idris
  rightUnit : (Bifunctor.unitR !*>
              (idF Cont `pair` DiscreteObj End Cont) !*>
               TensorBifunctor)
               =~= idF Cont
  rightUnit = MkNaturalIsomorphism
      (MkNT
        (\v => π1 <! (\_, x => x && ()))
        (\a, b, m => Refl))
      (\v => (&& ()) <! (\_ => π1))
      (\v => cong2Dep
        (<!)
        (funExt $ \(x1 && ()) => Refl)
        (funExtDep $ \v => funExt $ \(w && ()) => Refl))
      (\_ => Refl)
```

### Parameterised Dependent Lenses

Equipped with the tensor product, we can build parameterised dependent lenses using the _para construction_. The para construction only requires
a monoidal product to build the bicategory of parameterised morphisms, because we have that tensor on containers is a monoidal product, we can build the bicategory automatically:

<!-- idris
public export
-->
```idris
ParaDLens : Bicategory Container
ParaDLens = ParaConstr Cont TensorMonoidal
```

### Tensor is a Bifunctor in $\ContCart$


<!-- idris
public export
-->

```idris
(~⊗#~) : (a =#> b) -> (a' =#> b') -> a ⊗ a' =#> b ⊗ b'
(~⊗#~) x y = MkCartDepLens (bimap x.cfwd y.cfwd)
    (\v => IsoProd (x.cbwd v.π1) (y.cbwd v.π2))

{-
public export
TensorBifunctor : Bifunctor Cont Cont Cont
TensorBifunctor = MkFunctor
    (uncurry (⊗))
    (\x, y, m => m.π1 ~⊗#~ m.π2)
    (\v => cong2Dep MkMorphism (funExt $ \(v1 && v2) => Refl)
                               (funExtDep $ \v => funExt $ \(v1 && v2) => Refl))
    (\a, b, c, f, g => Refl)
```
