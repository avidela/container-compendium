```idris
module Data.Container.Kleene

import Data.Container
import Data.Sigma
import Data.Product

import Data.Vect
import Data.Container.Morphism
import Data.Container.Lift
import Data.Container.Coproduct
import Data.Container.Descriptions.Maybe
import Debug.Trace

%default total

namespace ExistentialComp
  -- λ x : Container -> μy . I + x ○ y
  public export
  data StarShp : Container -> Type where
    Done : StarShp c
    More : (x : c.req) -> (c.res x -> StarShp c) -> StarShp c
    -- Σ (x : c.req) . (c.res x -> StarShp c)

  public export
  singleton : {c : Container} -> c.req -> StarShp c
  singleton x = More x (const Done)

  public export
  data StarPos : (c : Container) -> StarShp c -> Type where
    StarU : StarPos c Done
    StarM : {0 c : Container} -> {0 x1 : c.req} -> {x2 : c.res x1 -> StarShp c} ->
            -- ((c ○ (!>) (StarShp c) (StarPos c)).res (MkEx x1 x2)) ->
            (x : c.res x1) -> (StarPos c (x2 x)) ->
            StarPos c (More x1 x2)

  public export
  (.º) : Container -> Container
  (.º) c = (!>) (StarShp c) (StarPos c)

  public export
  Star : Container -> Container
  Star c = (!>) (StarShp c) (StarPos c)

  public export
  mapStarShp : (a =%> b) -> StarShp a -> StarShp b
  mapStarShp x Done = Done
  mapStarShp x (More p1 p2) =
    More (x.fwd p1) (\y => assert_total $ mapStarShp x (p2 (x.bwd p1 y)))

  public export
  mapStarUntil : (Maybe • a =%> b) -> StarShp a -> StarShp b
  mapStarUntil m Done = Done
  mapStarUntil m (More p1 p2) =
    More (m.fwd p1) (\y => case m.bwd p1 y of
           Just x => assert_total $ mapStarUntil m (p2 x)
         ; Nothing => Done)

  public export
  mapStarPos : (mor : a =%> b) -> (x : StarShp a) -> StarPos b (mapStarShp mor x) -> StarPos a x
  mapStarPos y Done z = StarU
  mapStarPos y (More p1 p2) (StarM z1 z2) =
    StarM (y.bwd p1 z1) (mapStarPos y (p2 (y.bwd p1 z1)) z2)

  public export
  map_kleene : a =%> b -> a.º =%> b.º
  map_kleene mor = mapStarShp mor <! mapStarPos mor

  join_star : forall a. StarShp a.º -> StarShp a

  join_star Done = Done
  join_star (More Done p2) = Done
  join_star (More (More p1 p2) p3) = More p1 p2

  join_kleene : a.º.º =%> a.º
  join_kleene = join_star <! ?why_is_this_impossible

  public export
  single : a.req -> StarShp a
  single x = More x (\_ => Done)

  pure_kleene : a =%> a.º
  pure_kleene =
      (single {a}) <!
      (\x, (StarM y1 y2) => y1)

  public export
  unit_kleene : (End).º =%> End
  unit_kleene = const () <! bwd
    where
      bwd : (x : StarShp End) -> Unit -> StarPos End x
      bwd Done y = StarU
      bwd (More () p2) () = StarM () (bwd (p2 ()) ())

  export
  Show a.req => Show (StarShp a) where
    show Done = "done"
    show (More x f) = "more: " ++ show x

namespace Tensor
  -- λ x : Container -> μy . I + x ⊗ y
  data TensorShp : Container -> Type where
    Done : TensorShp c
    More : c.req * TensorShp c -> TensorShp c

  TensorPos : (c : Container) -> TensorShp c -> Type
  TensorPos c Done = Unit
  TensorPos c (More (head && tail)) = (c.res head) * (TensorPos c tail)

  Tensor : Container -> Container
  Tensor c = (!>) (TensorShp c) (TensorPos c)


public export
f_functor : Functor f => a =%> b -> f • a =%> f • b
f_functor c = c.fwd <! (\x => map (c.bwd x))

-- Absord one layed of composition into a Kleene star
public export
absorb : a ○ Star a =%> Star a
absorb = (\(MkEx x y) => More x y) <! \(MkEx x y), (StarM z w) => z ## w

-- Decompose one layer of the kleene star into either the unit, or more computation
export
decomposeStar : Star a =%> End + (a ○ Star a)
decomposeStar = peel <! peelBack
  where
    peel : StarShp a -> () + Ex a (StarShp a)
    peel Done = <+ ()
    peel (More x f) = +> MkEx x f

    peelBack : (x : StarShp a) -> (End + (a ○ Star a)).res (peel x) -> StarPos a x
    peelBack Done y = StarU
    peelBack (More x f) y = StarM y.π1 y.π2

export
decomposeStar' : Star a =%> End + Star a
decomposeStar' = decomposeStar |%> identity End ~+~ absorb

splitFwd : ((End + a) ○ b).req -> b.req + Ex a b.req
splitFwd (MkEx (<+ x) ex2) = <+ ex2 ()
splitFwd (MkEx (+> x) ex2) = +> MkEx x ex2

splitBwd : (x : ((End + a) ○ b).req) -> (b + (a ○ b)).res (splitFwd {a} {b} x) ->
           ((End + a) ○ b).res x
splitBwd (MkEx (<+ x) ex2) y = () ## y
splitBwd (MkEx (+> x) ex2) y = y

splitA : (End + a) ○ b =%> b + (a ○ b)
splitA = splitFwd
    <! splitBwd

-- run the kleen star until there is no more input
export %inline
traceValMsg : Show a => String -> a -> a
traceValMsg msg = traceValBy ((msg ++) . show)

Trace : Show a.req => a =%> a
Trace = id <! \x, y => trace """
                       fwd input: \{show x}
                       """ y

rec : (x : StarShp End) -> StarPos End x
rec Done = StarU
rec (More x f) = StarM () (rec (f ()))

export
allUnits : Star End =%> End
allUnits = const () <! \x, y => rec x


export
commuteMaybeStar : Maybe • Star a =%> Star (Maybe • a)
commuteMaybeStar = commuteShp <! commutePos
  where
    commuteShp : StarShp a -> StarShp ((x : a.req) !> Maybe (a .res x))
    commuteShp Done = Done
    commuteShp (More x f) = More x $ maybe Done (commuteShp . f)

    commutePos : (x : StarShp a) -> (Star (Maybe • a)).res (commuteShp x) -> Maybe (StarPos a x)
    commutePos Done StarU = Just StarU
    commutePos (More x f) (StarM Nothing StarU) = Nothing
    commutePos (More x f) (StarM (Just y) z) = map (StarM y) (commutePos (f y) z)

export
commuteEitherStar : Either e • Star a =%> Star (Either e • a)
commuteEitherStar = commuteShp <! commutePos
  where
    commuteShp : StarShp a -> StarShp ((x : a.req) !> Either e (a .res x))
    commuteShp Done = Done
    commuteShp (More x f) = More x $ either (const Done) (commuteShp . f)

    commutePos : (x : StarShp a) -> (Star (Either e • a)).res (commuteShp x) -> Either e (StarPos a x)
    commutePos Done StarU = Right StarU
    commutePos (More x f) (StarM (Left e) StarU) = Left e
    commutePos (More x f) (StarM (Right y) z) = map (StarM y) (commutePos (f y) z)

covering
recc : a =%> Star a -> (x : StarShp a) -> StarPos a x
recc m Done = StarU
recc (m1 <! m2) (More x f) =
  let rec1 = recc (m1 <! m2) (m1 x)
      xv = m2 x rec1
      rec2 = recc (m1 <! m2) (f xv)
  in StarM xv rec2

covering export
loop : a =%> Star a -> Star a =%> End
loop m = const () <! \x, _ => recc m x

{-
namespace UniversalComp
  -- λ x : Container -> μy . I + x #> y
  data StarShp : Container -> Type where
    Done : StarShp c
    More : Ex c (StarShp c) -> StarShp c

  StarPos : (c : Container) -> StarShp c -> Type
  StarPos c Done = Unit
  StarPos c (More (x1 ## x2)) = (x : c.res x1) -> StarPos c (x2 x)

  Star : Container -> Container
  Star c = (!>) (StarShp c) (StarPos c)

covering
data StarShpBuilder :
    (act : Container -> Type -> Type) ->
    Container -> Type where
  Done : StarShpBuilder act c
  More : {act : Container -> Type -> Type} ->
         {rest : StarShpBuilder act c} ->
         (act c (StarShpBuilder act c)) -> StarShpBuilder act c


{-
StarBuilder : (act : Container -> Type -> Type) ->
              (bwd : (c : Container) -> {x : Type} -> Act c x) -> Container -> Container
StarBuilder act c = (!>) (StarShpBuilder act c) StarPosBuilder
  where
    StarPosBuilder : StarShpBuilder act c -> Type
    StarPosBuilder arg = ?hol
```
