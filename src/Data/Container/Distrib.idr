module Data.Container.Distrib

import Control.Function
import Data.Maybe
import Data.List
import Data.List.Quantifiers
import Data.Container
import Data.Container.Morphism
import Data.Container.Morphism.Eq
import Data.Container.Descriptions.Maybe
import Data.Container.Descriptions.List
import Data.Sigma
import Data.List.Quantifiers as QL
import Proofs.Sigma
import Proofs.Extensionality
import Proofs.Congruence
import Proofs.Void
import Proofs.UIP

import Syntax.PreorderReasoning

%hide Prelude.Ops.infixl.(|>)
%default total

public export
maybeBind : Maybe a -> (a -> Maybe b) -> Maybe b
maybeBind (Just x) f = f x
maybeBind Nothing x = Nothing

public export
distribListMaybe : List (Maybe a) -> Maybe (List a)
distribListMaybe [] = Just []
distribListMaybe (Just x :: xs) = maybeMap (x ::) (distribListMaybe xs)
distribListMaybe (Nothing :: xs) = Nothing

distribAnyNothing : (xs : List (Maybe a)) -> (p : Any (=== Nothing) xs) -> distribListMaybe xs === Nothing
distribAnyNothing (Nothing :: xs) (Here Refl) = Refl
distribAnyNothing (Nothing :: xs) (There y) = Refl
distribAnyNothing ((Just x) :: xs) (There y) =
  let rec = distribAnyNothing xs y
  in rewrite rec in Refl

distribAnyNothing' : (xs : List (Maybe a)) -> distribListMaybe xs === Nothing -> Any (=== Nothing) xs
distribAnyNothing' [] prf = absurd prf
distribAnyNothing' (Nothing :: xs) prf = Here Refl
distribAnyNothing' ((Just x) :: xs) prf with (distribListMaybe xs) proof p
  distribAnyNothing' ((Just x) :: xs) prf | Nothing = There (distribAnyNothing' xs p)
  distribAnyNothing' ((Just x) :: xs) prf | (Just y) = absurd prf

weakenAnyLeft : {ys : _} -> QL.Any.Any p xs -> Any p (ys ++ xs)
weakenAnyLeft {ys = []} x = x
weakenAnyLeft {ys = (y :: ys)} x = There (weakenAnyLeft {ys} x)

weakenAnyRight : Any p xs -> Any p (xs ++ ys)
weakenAnyRight (Here x) = Here x
weakenAnyRight (There x) = There (weakenAnyRight x)

weakenAnyMiddle : {ys : _} -> Any p (ys ++ xs) -> Any p (ys ++ x :: xs)
weakenAnyMiddle {ys = []} y = There y
weakenAnyMiddle {ys = (z :: ys)} (Here y) = Here y
weakenAnyMiddle {ys = (z :: ys)} (There y) = There (weakenAnyMiddle {ys} y)

anyFlip : {x, y : _} -> Any p (x ++ y) -> Any p (y ++ x)
anyFlip {x = []} (Here x) = Here x
anyFlip {x = []} (There x {xs}) = rewrite appendNilRightNeutral xs in There x
anyFlip {x = (x :: xs)} (Here z) = weakenAnyLeft (Here z)
anyFlip {x = (x :: xs)} (There z) = let rec = anyFlip {x = xs} z in weakenAnyMiddle rec

prf_weak_right : (xs, y : _) -> distribListMaybe y = Nothing -> distribListMaybe (y ++ xs) = Nothing
prf_weak_right xs y prf = distribAnyNothing (y ++ xs) (weakenAnyRight (distribAnyNothing' y prf))

prf_weak_left : (xs, y : _) -> distribListMaybe y = Nothing -> distribListMaybe (xs ++ y) = Nothing
prf_weak_left xs y prf = distribAnyNothing (xs ++ y) (weakenAnyLeft (distribAnyNothing' y prf))

0 lemma_cons : maybeMap (\arg => y :: arg) (distribListMaybe ys) = Just (x :: xs) ->
               y === x
lemma_cons {ys} prf with (distribListMaybe ys)
  lemma_cons {ys = ys} prf | Nothing = absurd prf
  lemma_cons {ys = ys} Refl | (Just z) = Refl

0 lemma_cons2 : {0 x, y : a} -> {0 xs : List a} -> {0 ys : List (Maybe a)} ->
                maybeMap (\arg => y :: arg) (distribListMaybe ys) = Just (x :: xs) ->
                distribListMaybe ys = Just xs
lemma_cons2 prf with (distribListMaybe ys)
  lemma_cons2 prf | Nothing = absurd prf
  lemma_cons2 Refl | (Just z) = Refl

distribBoth : (a, b : List (p)) ->
              (x, y : List (Maybe p)) ->
              distribListMaybe x === Just a ->
              distribListMaybe y === Just b ->
              distribListMaybe (x ++ y) === Just (a ++ b)
distribBoth [] b [] y prf prf1 = prf1
distribBoth [] b (Nothing :: xs) y prf prf1 = absurd prf
distribBoth [] b ((Just x) :: xs) y prf prf1 with (distribListMaybe xs) proof prf'
  distribBoth [] b ((Just x) :: xs) y prf prf1 | Nothing = absurd prf
  distribBoth [] b ((Just x) :: xs) y prf prf1 | (Just z) = let nnn = Function.inj Prelude.Just prf in absurd nnn
distribBoth (z :: xs) b [] y prf prf1 = absurd (inj Just (sym prf))
distribBoth (x :: xs) b (Nothing :: ys) z prf prf1 = absurd prf
distribBoth (x :: xs) b ((Just y) :: ys) z prf prf1 =
  let 0 rec = distribBoth xs b ys z (lemma_cons2 prf) prf1
      0 prf1' : y === x
      prf1' = lemma_cons {y, ys} prf
      rec' := cong (maybeMap (x ::)) rec
  in trans (cong (\x => maybeMap x (distribListMaybe (ys ++ z))) (funExt $ \v => cong (:: v) prf1')) rec'

%unbound_implicits off

distribAny : {x : Container} ->
                (v : List (Maybe x.req)) ->
                Any (All x.res) (distribListMaybe {a=x.req} v) ->
                All (Any x.res) v

distribAnyView : {x : Container} ->
                    (xs : Maybe (List x.req)) ->
                    (z : x.req) ->
                    (v : List (Maybe x.req)) ->
                    (p : xs = distribListMaybe v) ->
                    (_ : Any (All x.res) (maybeMap (z ::) xs)) ->
                    All (Any x.res) (Just z :: v)
distribAnyView (Just w) z v p (Yep (y :: ys)) = Yep y ::
    distribAny v (replace {p = Any (All x.res)} p (Yep ys))

distribAny [] y = []
distribAny (Nothing :: xs) y = absurd y
distribAny (Just z :: xs) y =
  distribAnyView (distribListMaybe xs) z xs Refl y

MaybeListDistrib : (x : Container) ->
    (AllC (MaybeAnyIdris x)) =%> (MaybeAnyIdris (AllC x))
MaybeListDistrib x = distribListMaybe <! distribAny
%unbound_implicits on

----------------------------------------------------------------------------------
-- Distributive diagrams
----------------------------------------------------------------------------------

L : Container -> Container
L = AllC

M : Container -> Container
M = MaybeAnyIdris

%unbound_implicits off

listMapEmpty : {a, b : Type} -> {x : List a} -> {f : a -> b} -> listMap f x === [] -> x === []
listMapEmpty {x = []} prf = Refl
listMapEmpty {x = (x :: xs)} prf = absurd prf

maybeMapCompose : (f, g, x : _) -> maybeMap (f . g) x === maybeMap f (maybeMap g x)
maybeMapCompose f g Nothing = Refl
maybeMapCompose f g (Just x) = Refl

maybeMapId : {xs : _} -> maybeMap (\x => x) xs = xs
maybeMapId {xs = Nothing} = Refl
maybeMapId {xs = (Just x)} = Refl

distribNothingConcat : {a, ys : _} -> (xs : List (Maybe a)) ->
                       distribListMaybe xs = Nothing -> distribListMaybe (xs ++ ys) = Nothing
distribNothingConcat [] prf = absurd prf
distribNothingConcat (Nothing :: xs) prf = Refl
distribNothingConcat ((Just x) :: xs) prf with (distribListMaybe xs) proof p'
  distribNothingConcat ((Just x) :: xs) prf | Nothing = let
      rec = distribNothingConcat xs p'
      in cong (maybeMap (x ::)) rec
  distribNothingConcat ((Just x) :: xs) prf | (Just y) = absurd prf

distribSplit1 : {a : Type} -> {x, y : a} -> {ys : List a} -> {xs : List (Maybe a)} ->
                maybeMap (\arg => x :: arg) (distribListMaybe xs) = Just (y :: ys) -> x === y
distribSplit1 prf with (distribListMaybe xs)
  distribSplit1 prf | Nothing = absurd prf
  distribSplit1 Refl | (Just z) = Refl

distribSplit2 : {a : Type} -> {x, y : a} -> {ys : List a} -> {xs : List (Maybe a)} ->
                maybeMap (\arg => x :: arg) (distribListMaybe xs) = Just (y :: ys) -> distribListMaybe xs === Just ys
distribSplit2 prf with (distribListMaybe xs)
  distribSplit2 prf | Nothing = absurd prf
  distribSplit2 Refl | (Just z) = Refl

distribJustConcat : {y , xs, ys : _} -> distribListMaybe ys = Just y ->
                    distribListMaybe (ys ++ xs) = maybeMap (\arg => y ++ arg) (distribListMaybe xs)
distribJustConcat {ys = []} Refl = sym $ maybeMapId
distribJustConcat {ys = (Nothing :: xs)} prf = absurd prf
distribJustConcat {ys = ((Just x) :: xs)} {y} {xs = zs} prf with (distribListMaybe xs) proof pn
  distribJustConcat {ys = ((Just x) :: xs)} {y = y} {xs = zs} prf | Nothing = absurd prf
  distribJustConcat {ys = ((Just x) :: xs)} {y = (x :: z)} {xs = zs} Refl | (Just z)
  = let rec = distribJustConcat {ys = xs} {xs = zs} {y = z} pn
        ppp = maybeMapCompose (Basics.(::) x) (z ++) (distribListMaybe zs)
    in Calc $ |~ maybeMap (x ::) (distribListMaybe (xs ++ zs))
              ~~ maybeMap (x ::) (maybeMap (\arg => z ++ arg) (distribListMaybe zs))
              ...(cong (maybeMap (x ::)) rec)
              ~~ maybeMap (\arg => x :: (z ++ arg)) (distribListMaybe zs)
              ...(sym ppp)

distribJustMap : {a : Type} -> {y : List a} -> {xs, ys : List (Maybe a)} ->
                 distribListMaybe ys = Just y ->
                 distribListMaybe (ys ++ xs) = maybeMap (y ++) (distribListMaybe xs)
distribJustMap {ys = []} Refl = sym maybeMapId
distribJustMap {ys = (x :: ys)} prf
    = Calc $ |~ distribListMaybe (x :: (ys ++ xs))
             ~~ distribListMaybe ((x :: ys) ++ xs)
             ...(Refl)
             ~~ maybeMap (\arg => y ++ arg) (distribListMaybe xs)
             ...(distribJustConcat prf)

proofDistrib : {a : Type} -> (v : List (List (Maybe a))) ->
             distribListMaybe (flatList v) === maybeMap flatList (distribListMaybe (listMap distribListMaybe v))
proofDistrib [] = Refl
proofDistrib ([] :: xs) with (listMap distribListMaybe xs) proof p'
  proofDistrib ([] :: xs) | [] = let p = listMapEmpty {x = xs} p' in rewrite p in Refl
  proofDistrib ([] :: xs) | (Nothing :: ys) =
        Calc $ |~ distribListMaybe (flatList xs)
               ~~ maybeMap flatList (distribListMaybe (listMap distribListMaybe xs))
               ...(proofDistrib xs)
               ~~ maybeMap flatList (distribListMaybe (Nothing :: ys))
               ...(cong (maybeMap flatList . distribListMaybe) p')
               ~~ Nothing
               ...(Refl)
  proofDistrib ([] :: xs) | (x :: ys) = let rec = proofDistrib xs in
        Calc $ |~ distribListMaybe (flatList xs)
               ~~ maybeMap flatList (distribListMaybe (listMap distribListMaybe xs))
               ...(rec)
               ~~ maybeMap flatList (distribListMaybe (x :: ys))
               ...(cong (maybeMap flatList . distribListMaybe) p')
               ~~ maybeMap (flatList . ([] ::)) (distribListMaybe (x :: ys))
               ...(Refl)
               ~~ maybeMap flatList (maybeMap (\arg => [] :: arg) (distribListMaybe (x :: ys)))
               ...(maybeMapCompose flatList ([] ::) _)
proofDistrib ((Nothing :: ys) :: xs) = Refl
proofDistrib (((Just x) :: ys) :: xs) with (distribListMaybe ys) proof p'
  proofDistrib (((Just x) :: ys) :: xs) | Nothing =
    cong (maybeMap (x::)) (distribNothingConcat ys p' {ys = flatList xs})
  proofDistrib (((Just x) :: ys) :: xs) | (Just y) = let
      mmc = sym $ maybeMapCompose (x ::) (y ++) (distribListMaybe (flatList xs))
      rec = proofDistrib xs in
      Calc $ |~ maybeMap (x ::) (distribListMaybe (ys ++ flatList xs))
             ~~ maybeMap (x ::) (maybeMap (y ++) (distribListMaybe (flatList xs)))
             ...(cong (maybeMap (x::)) (distribJustMap p'))
             ~~ maybeMap ((x ::) . (y ++)) (distribListMaybe (flatList xs))
             ...(sym $ maybeMapCompose _ _ _)
             ~~ maybeMap ((x ::) . (y ++)) (maybeMap flatList (distribListMaybe (listMap distribListMaybe xs)))
             ...(cong (maybeMap ((x ::) . (y ++))) rec)
             ~~ maybeMap ((x ::) . (y ++) . flatList) (distribListMaybe (listMap distribListMaybe xs))
             ...(sym $ maybeMapCompose _ _ _)
             ~~ maybeMap flatList (maybeMap ((x :: y) ::) (distribListMaybe (listMap distribListMaybe xs)))
             ...(maybeMapCompose _ _ _)

namespace LLM2ML
  parameters {x : Container}
    top : L (L (M x)) =%> L (M (L x))
    top = AllCFunctor (MaybeListDistrib x)

    topRight : L (M (L x)) =%> M (L (L x))
    topRight = MaybeListDistrib (AllC x)

    botRight : M (L (L x)) =%> M (L x)
    botRight = MaybeAnyIdrisFunctor Descriptions.List.join'

    left : L (L (M x)) =%> L (M x)
    left = join' {x = MaybeAnyIdris x}

    bot : L (M x) =%> M (L x)
    bot = MaybeListDistrib x

    -- joinMapDistrib : {v : _} -> joinBwd v [] === mapProp {p = ?aaa} distribAny v []

    0 lemma2 : (v : (L (L (M x))).req) ->
             (y : (M (L x)).res ((left ⨾ bot).fwd v)) ->
             let 0 p1 : (L (L (M x))).res v
                 0 p2 : (L (L (M x))).res v
                 p2 = (top ⨾ topRight ⨾ botRight).bwd v
                         (rewrite__impl (M (L x)).res (sym $ proofDistrib v) y)
             in (left ⨾ bot).bwd v y === p2
    lemma2 v y = ?thing
--     lemma2 v y = Calc $
--                  |~ (left |> bot).bwd {c1 = L (L (M x)), c2 = M (L x)} v y
--                  ~~ ((MkMorphism (bot.fwd . left.fwd) (\z => left.bwd z . bot.bwd (left.fwd z))).bwd {c1 = L (L (M x)), c2 = M (L x)} v y)
--                  ...(Refl)
--                  ~~ ((\z => left.bwd z . bot.bwd (left.fwd z)) v y)
--                  ...(Refl)
--                  ~~ (left.bwd v . bot.bwd (flatList v)) y
--                  ...(Refl)
--                  ~~ (joinBwd {x = M x} v (distribAny (flatList v) y))
--                  ...(Refl)
--                  ~~ (mapProp distribAny v
--                     (distribAny {x = L x} (listMap distribListMaybe v)
--                     (bwdMaybeF {x = L (L x)} (distribListMaybe (listMap distribListMaybe v))
--                     (replace {p = (M (L x)).res} (proofDistrib v) y))))
--                  ...(?result)
--                  ~~ ((\z => top.bwd z
--                       . (topRight.bwd (top.fwd z)
--                       . botRight.bwd (topRight.fwd (top.fwd z)))))
--                       v (replace {p = (M (L x)).res} (proofDistrib v) y)
--                  ...(Refl)
--                  ~~ (MkMorphism {c1 = L (L (M x)), c2 = M (L x)}
--                        ((fwd {c1 = M (L (L x)), c2 = M (L x)} botRight .
--                                  fwd {c1 = L (M (L x)), c2 = M (L (L x))} topRight) .
--                                  fwd top)
--                        (\z => top.bwd z . (topRight.bwd (top.fwd z) . botRight.bwd (topRight.fwd (top.fwd z))))
--                        ).bwd v
--                          (replace {p = (M (L x)).res} (proofDistrib v) y)
--                  ...(Refl)
--                  ~~ (top |> (MkMorphism
--                                {c1 = L (M (L x)), c2  = M (L x)}
--                                (fwd {c1 = M (L (L x)), c2 = M (L x)} botRight .
--                                  fwd {c1 = L (M (L x)), c2 = M (L (L x))} topRight)
--                                (\z => topRight.bwd z . botRight.bwd (topRight.fwd z))
--                        )).bwd v
--                          (replace {p = (M (L x)).res} (proofDistrib v) y)
--                  ...(Refl)
--                  ~~ (top |> (topRight |> botRight)).bwd v
--                          (replace {p = (M (L x)).res} (proofDistrib v) y)
--                  ...(Refl)

    -- lemma2 v y with (distribListMaybe (flatList v)) proof p1
    --   lemma2 v y | Nothing = absurd y
    --   lemma2 v (Yep y) | (Just z) with ((top |> topRight |> botRight).bwd v
    --           (replace {p = Any (All (x .res))} (sym p1 `trans` proofDistrib v) (Yep y))) proof p2
    --     lemma2 [] (Yep y) | Just z | [] = Refl
    --     lemma2 ([] :: xs) (Yep y) | Just z | (w :: s) with (flatList xs) proof p3
    --       lemma2 ([] :: xs) (Yep []) | Just [] | ([] :: s) | [] = cong ([] ::) (let px : xs === []
    --                                                                              in rewrite px in ?end_1K2_rhs2_0)
    --       lemma2 ([] :: xs) (Yep y) | Just z | ([] :: s) | (Nothing :: ys) = absurd p1
    --       lemma2 ([] :: xs) (Yep y) | Just z | ([] :: s) | ((Just v) :: ys) with (distribListMaybe ys) proof p4
    --         lemma2 ([] :: xs) (Yep y) | Just z | ([] :: s) | ((Just v) :: ys) | Nothing = absurd p1
    --         lemma2 ([] :: xs) (Yep y) | Just z | ([] :: s) | ((Just v) :: ys) | (Just w)
    --           = let rec = lemma2 xs (replace {p = Any (All (x .res))}
    --                                          (sym p1 `trans` (cong (maybeMap (v ::)) (sym p4) `trans` cong distribListMaybe (sym p3)))
    --                                          (Yep y))
    --             in cong ([] ::) $ ?aaa_lemma2
    --     lemma2 ((x :: xs') :: xs) (Yep y) | Just z | (w :: s) = ?end_1K

  %unbound_implicits on
  -- diagram1 : Commutes with list join
  --
  -- List (List (Maybe x)) ----> List (Maybe (List x))
  --          |                       |
  --          |                       V
  --          |                 Maybe (List (List x))
  --          |                       |
  --          V                       V
  --     List (Maybe x) ------> Maybe (List x)

  0 square1 : (x : Container) -> let

      0 top : L (L (M x)) =%> L (M (L x))
      top = AllCFunctor (MaybeListDistrib x)

      0 topRight : L (M (L x)) =%> M (L (L x))
      topRight = MaybeListDistrib (AllC x)

      0 botRight : M (L (L x)) =%> M (L x)
      botRight = MaybeAnyIdrisFunctor Descriptions.List.join'

      0 left : L (L (M x)) =%> L (M x)
      left = join' {x = MaybeAnyIdris x}

      0 bot : L (M x) =%> M (L x)
      bot = MaybeListDistrib x

    in left ⨾ bot <%≡%> top ⨾ topRight ⨾ botRight
  square1 x = MkDepLensEq
      proofDistrib
      lemma2

maybeMapDistribNothing : {xs : _} ->
               maybeMap (\arg => Prelude.Just y :: arg) (distribListMaybe xs) = Nothing ->
               distribListMaybe xs = Nothing
maybeMapDistribNothing prf with (distribListMaybe xs)
  maybeMapDistribNothing prf | Nothing = Refl
  maybeMapDistribNothing prf | (Just x) = absurd prf

maybeJoinJust : (x : _) -> maybeJoin (Just x) === x
maybeJoinJust Nothing = Refl
maybeJoinJust (Just x) = Refl

%unbound_implicits off
namespace LMM2ML
  parameters {x : Container}
    top : L (M (M x)) =%> M (L (M x))
    top = MaybeListDistrib (M x)

    left : L (M (M x)) =%> L (M x)
    left = AllCFunctor (maybeAnyJoin x)

    topRight : M (L (M x)) =%> M (M (L x))
    topRight = MaybeAnyIdrisFunctor (MaybeListDistrib x)

    botRight : M (M (L x)) =%> M (L x)
    botRight = maybeAnyJoin (L x)

    bot : L (M x) =%> M (L x)
    bot = MaybeListDistrib x


    --  {x : Container}
    --  x' : Maybe (x .req)
    --  xs' : List (Maybe (Maybe (x .req)))
    --  val : List (Maybe (x .req))
    --  p2 : Just v = distribListMaybe val
    --  p : Just val = maybeMap (x' ::) (distribListMaybe xs')
    --  vs : All (x .res) v
    --  ys' : Any (All (x .res)) (maybeJoin (maybeMap distribListMaybe (maybeMap (\arg => x' :: arg) (distribListMaybe xs'))))
    end2 : {x' : Maybe (x .req)} -> (xs' : List (Maybe (Maybe (x .req)))) ->
           {val : List (Maybe (x .req))} ->
           {v : List x.req} ->
           {p2 : Just v = distribListMaybe val} ->
           {p1 : Just val = maybeMap (x' ::) (distribListMaybe xs')} ->
           {vs : All (x .res) v} ->
           {ys' : Any (All (x .res)) (maybeJoin (maybeMap distribListMaybe (maybeMap (\arg => x' :: arg) (distribListMaybe xs'))))} ->

           let scary := (maybeMap (\arg => x' :: arg) (distribListMaybe xs'))
               lol := (maybeJoinBwd (maybeMap distribListMaybe (maybeMap (\arg => x' :: arg) (distribListMaybe xs'))) ys')
               bbb := bwd {c1 = M (L (M x)), c2 = L (M (M x))}
                   ?thisisimpossible
                   (maybeMap (\arg => x' :: arg) (distribListMaybe xs'))
                   ?notmatchLol

               t1, t2 : Any (All (Any x.res)) (maybeMap (x' ::) (distribListMaybe xs'))
               t1 = replace {p = Any (All (Any x.res))} p1 (Yep (distribAny val (replace {p = Any (All x.res)} p2 (Yep vs))))
               t2 = ?hello -- bwd -- {dom = ((!>) (Maybe (List x.req)) (Any (All x.res)))}
                    --     -- {cod = ((!>) (List (Maybe x.req)) (All (Any (x .res))))}
                    --     (MkMorphism {c1 = L (M x)} distribListMaybe distribAny) ?hhh ?sss
                    --     -- (maybeMap (\arg => x' :: arg) (distribListMaybe xs'))
                        -- (maybeJoinBwd (maybeMap distribListMaybe (maybeMap (\arg => x' :: arg) (distribListMaybe xs'))) ys')
           in t1 === t2
    endGameView : {x : Container} ->
      (x' : Maybe x.req) ->
      (xs' : List (Maybe (Maybe x.req))) ->
      {val : List (Maybe (x .req))} ->
      {v : List x.req} ->
      {p1 : Just val = distribListMaybe xs'} ->
      {p2 : Just v = distribListMaybe val} ->
      {leftBit, rightBit : _} ->
      distribAny {x = M x} xs' leftBit ===
        distribAny {x = M x} xs' rightBit ->
        let tt := ?ddwhu
        in Type
      -- distribAnyView {x = M x} (distribListMaybe xs') x' xs' Refl leftBit ===
      -- distribAnyView {x = M x} (distribListMaybe {a = Maybe x.req} xs') x' xs' Refl ?arg

    endGame' : {xs' : List (Maybe (Maybe x.req))} ->
              {ys' : Any (All (x .res))
                (maybeJoin (maybeMap distribListMaybe (distribListMaybe xs')))} ->
              {val : List (Maybe x.req)} ->
              {v : List x.req} ->
              {vs : All x.res v} ->
              {p : Just val = distribListMaybe xs'} ->
              {p2 : Just v = distribListMaybe val} ->
              let
                  ll : All (Any (Any x.res)) xs'
                  ll = distribAny {x = M x} xs' (replace
                          {p = (Any (All (Any x.res)))} p
                          (Yep (distribAny val
                            (replace {p = Any (All x.res)} p2 (Yep vs)))))
              in ll === ((top ⨾ topRight) ⨾ botRight).bwd xs' ys'
    endGame' {xs'=[]} {ys'} {p} {val} {vs} = Refl
    endGame' {xs'= Nothing :: xs'} {ys'} {p} {p2} {val} {vs} = absurd p
    endGame' {xs'= Just x' :: xs'} {ys'} {p} {p2} {val} {vs} =
      let rec = endGame' {xs'}
      in ?hi

  -- diagram 2: commutes with maybe join
  --
  --                distrib_(Maybe x)
  --   List (Maybe (Maybe x)) ----> Maybe (List (Maybe x))
  --           |                       |
  --           |                       | Maybe distrib
  --           |                       V
  -- List join |                 Maybe (Maybe (List x))
  --           |                       |
  --           |                       | join (List x)
  --           V                       V
  --      List (Maybe x) ------> Maybe (List x)
  --                     distrib_x
  0 pentagon2 : (x : Container) ->
                ((top {x} ⨾ topRight {x}) ⨾ botRight {x}) === (left {x} ⨾ bot {x})
  pentagon2 x = depLensEqToEq $ MkDepLensEq prf1 eqBwd
    where
      prfJoinNothing : (xs : _) ->
                       maybeJoin (maybeMap distribListMaybe
                                     (maybeMap (\arg => Nothing :: arg) (distribListMaybe xs)))
                       === Nothing
      prfJoinNothing [] = Refl
      prfJoinNothing (Nothing :: xs) = Refl
      prfJoinNothing (Just y :: ys) with (distribListMaybe ys)
        prfJoinNothing (Just y :: ys) | Nothing = Refl
        prfJoinNothing (Just y :: ys) | (Just z) = Refl

      prfListMaybe : (xs : _) ->
                     distribListMaybe xs = Nothing ->
                     distribListMaybe (listMap maybeJoin xs) === Nothing
      prfListMaybe [] prf = absurd prf
      prfListMaybe (Nothing :: xs) prf = Refl
      prfListMaybe ((Just Nothing) :: xs) prf = Refl
      prfListMaybe ((Just (Just y)) :: xs) prf =
        let rec = prfListMaybe xs (maybeMapDistribNothing prf)
        in cong (maybeMap (\arg => y :: arg)) rec

      prf1  : (v : List (Maybe (Maybe (x .req)))) ->
              maybeJoin (maybeMap distribListMaybe (distribListMaybe v)) ===
              distribListMaybe (listMap maybeJoin v)
      prf1 [] = Refl
      prf1 (Nothing :: xs) = Refl
      prf1 ((Just Nothing) :: xs) = prfJoinNothing xs
      prf1 ((Just (Just y)) :: xs) with (distribListMaybe xs) proof p1
        prf1 ((Just (Just y)) :: xs) | Nothing = rewrite prfListMaybe xs p1 in Refl
        prf1 ((Just (Just y)) :: xs) | (Just z) =
          let
              prfn : distribListMaybe z === distribListMaybe (listMap maybeJoin xs)
              prfn = Calc $ |~ distribListMaybe z
                            ~~ maybeJoin (maybeMap distribListMaybe (Just z))
                            ...(sym $ maybeJoinJust (distribListMaybe z))
                            ~~ maybeJoin (maybeMap distribListMaybe (distribListMaybe xs))
                            ...(cong (\x => maybeJoin (maybeMap distribListMaybe x)) (sym p1))
                            ~~ distribListMaybe (listMap maybeJoin xs)
                            ...(prf1 xs)
          in Calc $ |~ maybeJoin (Just (maybeMap (y ::) (distribListMaybe z)))
                    ~~ maybeMap (y ::) (distribListMaybe z)
                    ...(maybeJoinJust (maybeMap (y::) (distribListMaybe z)))
                    ~~ maybeMap (y ::) (distribListMaybe (listMap maybeJoin xs))
                    ...(cong (maybeMap (y::)) prfn)

      0
      pdistrib : {a : Type} ->
                 {xs : List (Maybe (Maybe a))} ->
                 {w : List (Maybe a)} ->
                 distribListMaybe xs = Just w ->
                 ((listMap maybeJoin xs)) = w
      pdistrib {xs = []} Refl = Refl
      pdistrib {xs = (Nothing :: xs)} prf = absurd prf
      pdistrib {xs = ((Just y) :: xs)} {w = []} prf with (distribListMaybe xs) proof pdist
        pdistrib {xs = ((Just y) :: xs)} {w = []} prf | Nothing = absurd prf
        pdistrib {xs = ((Just y) :: xs)} {w = []} prf | (Just z) = absurd (injective prf)
      pdistrib {xs = ((Just y) :: xs)} {w = (z :: ys)} prf = let
        p1 = lemma_cons prf
        p2 = lemma_cons2 prf
        rec = pdistrib {xs = xs} p2
        in cong2 (::) (trans (maybeJoinJust _) p1) rec


      eqBwd : (v : (L (M (M x))).req) -> (y : (M (L x)).res (((top {x} ⨾ topRight {x}) ⨾ botRight {x}).fwd v)) ->
            let 0 p1 : (L (M (M x))).res v
                p1 = ((top {x} ⨾ topRight {x}) ⨾ botRight {x}).bwd v y
                0 p2 : (L (M (M x))).res v
                p2 = (left {x} ⨾ bot {x}).bwd v (replace {p = (M (L x)).res} (prf1 v) y)
            in p1 === p2
      eqBwd [] y = Refl
      eqBwd (Nothing :: xs) y = absurd y
      eqBwd ((Just Nothing) :: xs) y with (distribListMaybe xs)
        eqBwd ((Just Nothing) :: xs) y | Nothing = absurd y
        eqBwd ((Just Nothing) :: xs) y | (Just z) = absurd y
      eqBwd ((Just (Just z)) :: xs) y with (distribListMaybe xs) proof pxs
        eqBwd ((Just (Just z)) :: xs) y | Nothing = absurd y
        eqBwd ((Just (Just z)) :: xs) y | (Just w) with (distribListMaybe w) proof pz
          eqBwd ((Just (Just z)) :: xs) y | (Just w) | Nothing = absurd y
          eqBwd ((Just (Just z)) :: xs) (Yep (y :: ys)) | (Just w) | (Just v)
            = let
                  px : distribListMaybe (listMap maybeJoin xs) === Just v
                  px = trans (cong distribListMaybe (pdistrib pxs)) pz
                  pv : maybeJoin (maybeMap distribListMaybe (distribListMaybe xs)) = Just v
                  pv = trans (cong (maybeJoin . maybeMap distribListMaybe) pxs)
                       (trans (maybeJoinJust _) pz)

                  y2 : Any (All (x .res)) (maybeJoin (maybeMap distribListMaybe (distribListMaybe xs)))
                  y2 = replace {p = Any (All (x .res))} (sym pv) (Yep ys)
                  0 p1' : {xs' : List (Maybe (Maybe x.req))} ->
                          {ys' : Any (All (x .res))
                            (maybeJoin (maybeMap distribListMaybe (distribListMaybe xs')))} ->
                          (L (M (M x))).res xs'
                  p1' = ((top ⨾ topRight) ⨾ botRight).bwd xs' ys'
                  0 p2' : (L (M (M x))).res xs
                  p2' = (left ⨾ bot).bwd xs (replace {p = (M (L x)).res} (prf1 xs) y2)
                  rec2 : p1' {xs'=xs, ys'=y2} === p2'
                  rec2 = eqBwd xs y2

                  left' : {val : List (Maybe x.req)} -> {xs' : List (Maybe (Maybe x.req))} -> {p : Just val = distribListMaybe xs'} ->
                          {p2 : Just v = distribListMaybe val} ->
                          All (Any (Any x.res)) xs'
                  left' = distribAny {x = M x} xs' (replace
                                        {p = (Any (All (Any x.res)))} p
                                        (Yep (distribAny val
                                          (replace {p = Any (All (x .res))} p2 (Yep ys)))))
                  right : ?
                  right = mapProp {p = Any x.res , f = maybeJoin} maybeJoinBwd (Just (Just z) :: xs) $
                    distribAnyView
                      (distribListMaybe (listMap maybeJoin xs))
                      z
                      (listMap maybeJoin xs)
                      Refl
                      (replace {p = Any (All (x .res)) . (maybeMap (z ::))}
                          (sym px) (Yep (y :: ys)))

                  getTail : {xval, u : _} -> Any (All x.res) (distribListMaybe (xval :: u)) -> Any (All x.res) (distribListMaybe u)

                  endGame : {xs' : List (Maybe (Maybe x.req))} ->
                            {ys' : Any (All (x .res))
                              (maybeJoin (maybeMap distribListMaybe (distribListMaybe xs')))} ->
                            {val : List (Maybe x.req)} ->
                            {v : List x.req} ->
                            {vs : All x.res v} ->
                            {p : Just val = distribListMaybe xs'} ->
                            {p2 : Just v = distribListMaybe val} ->
                            let
                                ll : All (Any (Any x.res)) xs'
                                ll = distribAny {x = M x} xs' (replace
                                        {p = (Any (All (Any x.res)))} p
                                        (Yep (distribAny val
                                          (replace {p = Any (All (x .res))} p2 (Yep vs)))))
                            in ll === ((top {x} ⨾ topRight {x}) ⨾ botRight {x}).bwd xs' ys'
                  endGame {xs'} {ys'} {p} {val} with (p1' {xs', ys'} ) proof pp
                    endGame {xs' = []} {ys' = ys'} {p} {val} | [] = Refl
                    endGame {xs' = (Nothing :: xs)} {ys' = ys'} {p} {val} | (s :: t) = absurd ys'
                    endGame {xs' = (Just xval :: xns)} {ys' = ys'} {p} {val=[]} | (s :: t) with (distribListMaybe xns) proof pd
                      endGame {xs' = (Just xval :: xns)} {ys' = ys'} {p = p} {val=[]} | (s :: t) | Nothing = absurd p
                      endGame {xs' = (Just xval :: xns)} {ys' = ys'} {p = Refl} {val=[]} | (s :: t) | (Just u) impossible
                    endGame {xs' = (Just xval :: xns)} {ys' = ys'} {p} {val=val1 :: val2} | (s :: t) with (distribListMaybe xns) proof pq
                      endGame {xs' = (Just xval :: xns)} {ys' = ys'} {p = p} {val=val1 :: val2} | (s :: t) | Nothing = absurd p
                      endGame {xs' = (Just xval :: xns)} {ys' = ys'} {p = p} {val=val1 :: val2} | (s :: t) | (Just u) with (distribAny (val1 :: val2) (replace {p = Any (All x.res)} p2 (Yep vs))) proof pn
                        endGame {xs' = (Just xval :: xns)} {ys' = ys'} {p = p} {val=val1 :: val2} | (Yep s :: t) | (Just u) | (x1 :: y1)
                           = let ysq = replace {p = Any (All x.res)} (maybeJoinJust _) ys'
                                 rec = endGame {xs' = xns, ys'= replace {p = \xvs => Any (All (x .res)) (maybeJoin (maybeMap distribListMaybe xvs))} (sym pq)
                                                    (replace {p = Any (All (x .res))} (sym $ maybeJoinJust _) (getTail ysq)) }
                              in (trans ?nnnn_rhsn_1_rhs1_0 pp)

              in let gn = Calc $ |~
                          distribAnyView {x = M x}
                             (Just w)
                             (Just z)
                             xs
                             (sym pxs)
                             (Yep (distribAnyView {x} (distribListMaybe w) z w Refl
                                  (replace {p = Any (All (x .res)) . (maybeMap (\arg => z :: arg))}
                                      (sym pz)
                                      (Yep (y :: ys)))))
                        ~~ (Yep (Yep y) :: (left' {xs'=xs, p = sym pxs, p2 = sym pz}))
                        ...(rewrite pz in Refl)
                        ~~ (Yep (Yep y) :: p1' {xs'=xs, ys'=y2})
                        ...(cong (Yep (Yep y) ::) (endGame' {xs'=xs, ys' = y2, p = sym pxs, p2 = sym pz}))
                        ~~ (Yep (Yep y) :: p2')
                        ...(cong (Yep (Yep y) ::) rec2)
                        ~~ right
                        ...(rewrite px in Refl)
                in trans (rewrite UIP (sym pxs) (rewrite pxs in Refl) in ?huhh) gn

  %unbound_implicits on
  ----------------------------------------------------------------------------------
  -- General distributive law?
  ----------------------------------------------------------------------------------


  genFwd : {a, b, x : Container} ->
      (a #> (b ○ x)).req -> (b ○ (a #> x)).req

  genBwd : {a, b, x : Container} ->
      (v : (a #> (b ○ x)).req) ->
      (b ○ (a #> x)).res (genFwd {a, b, x} v) ->
      (a #> (b ○ x)).res v

  generalDistrib : (a, b, x : Container) ->
      (a #> (b ○ x)) =%> (b ○ (a #> x))
  generalDistrib a b x = genFwd <! genBwd


