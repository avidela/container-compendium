<!-- idris
module Data.Container.Cartesian.Category

import Data.Category
import Data.Category.NaturalTransformation
import Data.Category.Monoid
import Data.Category.Functor

import Data.Container.Definition
import Data.Container.Cartesian
import Data.Container.Category
import Data.Container.Morphism.Definition
import Data.Iso

import Data.Product

import Proofs

%hide Prelude.Ops.infixl.(|>)
--------------------------------------------------------------------------------
-- Cartesian lenses form a category
--------------------------------------------------------------------------------
-->

### Containers and Cartesian Morphisms Form a Category

With the previous definition, we build the required lemmas to prove that it forms a category, first we prove identity.


```idris
0 identityRight :
    (w, v : Container) -> (f : w =#> v) ->
    Cartesian.identity w |#> f ≡#>≡ f
identityRight w v (MkCartDepLens fwd bwd) = MkCartDepLensEq
    (\_ => Refl)
    (\f => isoIdRight (bwd f))

0 identityLeft :
    (w, v : Container) ->
    (f : w =#> v) ->
    f |#> Cartesian.identity v ≡#>≡ f
identityLeft w v (MkCartDepLens f1 bwd) = MkCartDepLensEq (\_ => Refl)
    (\f => isoIdLeft (bwd f))
```

Most of the heavy lifting is delegated to `isoIdLeft` which it itself part of the proofs that isomophisms form a category. We do the same for composition.

```idris

0 proofComposition :
    (f : a =#> b) -> (g : b =#> c) -> (h : c =#> d) ->
    f |#> (g |#> h) ≡#>≡ (f |#> g) |#> h
proofComposition (MkCartDepLens fwd1 bwd1) (MkCartDepLens fwd2 bwd2) (MkCartDepLens fwd3 bwd3) =
  MkCartDepLensEq (\_ => Refl) (\f =>
    symIsoEq $ transIsoAssoc (bwd3 (fwd2 (fwd1 f))) (bwd2 (fwd1 f)) (bwd1 f)
    )
```
Finally, using the above, we can write the definition of category in Idris of cartesian container morphisms.

```idris
||| Cartesian containers category, where objects are containers and morphisms are cartesian lense
public export
ContCart : Category Container
ContCart = MkCategory
  (=#>)
  (\_ => identity _)
  Cartesian.(|#>)
  (\_, _, f => cartEqToEq (identityLeft _ _ f))
  (\_, _, f => cartEqToEq (identityRight _ _ f))
  (\_, _, _, _, f, g, h => cartEqToEq (proofComposition f g h))
```

### Forgetful functor

I also define a forgetful map into $\Cont$ to easily extract the corresponding lens out of a cartesian morphism.

```idris
public export
toLens : (0 a, b : Container) -> a =#> b -> a =%> b
toLens a b x = x.cfwd <! (\y => to (x.cbwd y))
```

From this we can build the functor proper using the category of cartesian morphisms we just defined

```idris
toLensId : (v : Container) ->
           toLens v v (Container.Cartesian.identity v) ≡ Container.Morphism.Definition.identity v
toLensId v = Refl

0
toLensComp : (x, y, z : Container) -> (a : x =#> y) -> (b : y =#> z) ->
             toLens x z (a |#> b) ≡ toLens x y a ⨾ toLens y z b
toLensComp x y z a b = depLensEqToEq $ MkDepLensEq
    (\xs => Refl)
    (\xs, ys => Refl)

CartToCont : Functor ContCart Cont
CartToCont = MkFunctor
    Basics.id (\x, y => toLens x y) toLensId toLensComp
```
