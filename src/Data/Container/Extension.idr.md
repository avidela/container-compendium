<!-- idris
module Data.Container.Extension

import Data.Container.Definition
import Data.Container.Category
import Data.Container.Morphism.Definition

import Data.Category.Bifunctor
import Data.Category.NaturalTransformation
import Data.Category.Set
import Data.Category.FunctorCat

import Proofs

%hide Prelude.Ops.infixl.(*>)
%hide Prelude.(*>)

||| Extension of a container as a functor, Also interpreted as "existential" monads
public export
-->

The extension is crucial to understand containers as data descriptors. Given a container `c`, its extension `Ex c : Type -> Type` gives us the type constructor, in the host language, for the data structure that it represents. The typical example is
[Lists](src/Data/Container/Descriptions/List.idr.md) which gets its own section.

```idris
record Ex (cont : Container) (ty : Type) where
  constructor MkEx
  ex1 : cont.req
  ex2 : cont.res ex1 -> ty
```
<!-- idris
%pair Ex ex1 ex2
-->

To help with writing proofs about the extensions of containers, I've defined
the equality of containers as being the quality of the requests and of the
continuation.

<!-- idris
public export
-->
```idris
record ExEq {a : Container} {b : Type} (c1, c2 : Ex a b) where
  constructor MkExEq
  pex1 : c1.ex1 ≡ c2.ex1
  pex2 : (v : a.res c1.ex1) ->
         let 0 b1, b2 : b
             b1 = c1.ex2 v
             b2 = c2.ex2 (rewrite__impl a.res (sym pex1) v)
          in b1 ≡ b2
```
Like other analogous definition, we need a way to lift those bespoke equality
types into propositional equalities using functional extensionality.

<!-- idris
public export
-->
```idris
0 exEqToEq : ExEq c1 c2 -> c1 ≡ c2
exEqToEq {c1 = MkEx x1 x2} {c2 = MkEx y1 y2} (MkExEq x y) =
  cong2Dep' MkEx x (funExt y)
```

<!-- idris
public export
ExProj1 :
   (x : Container) ->
   (y : Type) ->
   (v2 : Ex x y) ->
   (v2.ex1 ≡ v2.ex1)
ExProj1 x y v2 = Refl

ExProj2 :
   (x : Container) ->
   (y : Type) ->
   (v2 : Ex x y) ->
   (a : x.res v2.ex1) ->
   (v2.ex2 a ≡ v2.ex2 a)
ExProj2 x y v2 a = Refl

export
inj1 : MkEx v1 v2 ≡ MkEx v1' v2' -> v1 ≡ v1'
inj1 Refl = Refl

export
inj2 : {0 c : Container} -> {0 t : Type} ->
       {0 v1 : c.req} -> {0 v2 : c.res v1 -> t} ->
       {0 v2' : c.res v1 -> t} ->
       (v : MkEx {cont = c} v1 v2 ≡ MkEx {cont = c} v1 v2') -> v2 ≡ v2'
inj2 Refl = Refl

export
exUniq : (x : Ex a b) -> MkEx x.ex1 x.ex2 = x
exUniq (MkEx ex1 ex2) = Refl
-->

```idris
public export
exFunc :
    {0 a, b : Type} -> {0 c : Container} ->
    (a -> b) -> Ex c a -> Ex c b
exFunc f x = MkEx x.ex1 (f . x.ex2)

||| F is a functor indeed
export
{0 c : Container} -> Functor (Ex c) where
  map f x = exFunc f x

export
exMap : {0 a, a' : Container} -> {0 b : Type} ->
        (f : a.req -> a'.req) ->
        ((x : a.req) -> a'.res (f x) -> a.res x) ->
        Ex a b -> Ex a' b
exMap fwd bwd y = MkEx (fwd y.ex1) (\z => y.ex2 (bwd y.ex1 z))

exMap2 : {0 a, a' : Container} -> {0 b, c : Type} ->
         (f : a.req -> a'.req) ->
         ((x : a.req) -> a'.res (f x) -> a.res x) ->
         (b -> c) ->
         Ex a b -> Ex a' c
exMap2 fwd bwd f x = map f (exMap fwd bwd x)


public export
exMap' : a =%> a' -> Ex a b -> Ex a' b
exMap' x y = MkEx (x.fwd y.ex1) (\z => y.ex2 (x.bwd y.ex1 z))
```

### The interpretation of containers is a functor from $Cont$ to $[Set, Set]$

The interpretation of container, also known as the _extensions_, called `Ex` maps
containers to `Type -> Type`. Categorically, we would like to know if this mapping
on `Type` is a functor. Because we are working with idris types and functions, we
replace $Set$ by the category `Set` of idris types and idris functions. In that
context, the same question amounts to providing an implementation for the
functor `Functor Cont (FunctorCat Set Set)` where `FunctorCat` is the _functor category_
given by the module [Data.Category.NaturalTransformation](../Category/NaturalTransformation.idr.md)


The first thing to do is to prove that `Ex` is an indexed functor, that is, for any
container we give it, we obtain a functor $[Set, Set]$.

<!-- idris
public export
-->
```idris
exBimap : a =%> a' -> (b -> b') -> Ex a b -> Ex a' b'
exBimap f g x = exFunc g (exMap' f x)

ExBifunctor : Bifunctor Cont Set Set
ExBifunctor = MkFunctor
    (uncurry Ex)
    (\x, y, m => exBimap m.π1 m.π2)
    (\v => funExt $ \z => exEqToEq $ MkExEq Refl (\_ => Refl))
    (\x, y, z, f, g => funExt $ \z =>
        exEqToEq $ MkExEq Refl (\_ => Refl))
```

First we prove that `Ex` is an indexed functor.

<!-- idris
public export
-->

```idris
exFunctor : (c : Container) -> Functor Set Set
exFunctor ty = MkFunctor
        (Ex ty)
        (\_, _, m => exFunc m)
        (\v => funExt $ \_ => exEqToEq $ MkExEq Refl (\_ => Refl))
        (\a, b, c, f, g => funExt $ \v => exEqToEq $ MkExEq Refl (\_ => Refl))
```

This functor is going to be our mapping of objects for our $Cont \to [Set, Set]$ functor.

We now need to provide the mapping on morphisms, because we are mapping between functors,
the morphisms in $[Set, Set]$ are natural transformations.

<!-- idris
public export
-->

```idris
mapFunctor : (a, b : Container) -> a =%> b -> exFunctor a =>> exFunctor b
mapFunctor a b m = MkNT (\v => exMap' m) (\v, w, z => Refl)
```

We now have a mapping on objects and a mapping on morphisms, it is left to prove
that they preserve identity and composition

<!-- idris
%unbound_implicits off
public export 0
-->
```idris
mapFunctorId : (a : Container) -> mapFunctor a a (identity a) === identity
mapFunctorId a = ntEqToEq $ MkNTEq (\v => funExt $ \w => Refl)

public export 0
mapFunctorComp : (a, b, c : Container) -> (f : a =%> b) -> (g : b =%> c) ->
    (mapFunctor a c (f ⨾ g)) ≡ (mapFunctor a b f !!> mapFunctor b c g)
mapFunctorComp a b c f g = ntEqToEq $ MkNTEq (\v => funExt $ \w => Refl)
```

Putting everything together, we build the proof that `Ex` is a functor from `Cont` to
`FunctorCat Set Set`, or rather, in mathematical terms, a functor $Cont \to [Set, Set]$

```idris
public export
ContInterp : Functor Cont (FunctorCat Set Set)
ContInterp = MkFunctor exFunctor mapFunctor mapFunctorId mapFunctorComp
```
