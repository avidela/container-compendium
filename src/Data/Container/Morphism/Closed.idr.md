# Closed lenses

A closed lens is isomorphic to a container morphism.

```idris
module Data.Container.Morphism.Closed

import Data.Container.Definition
import Data.Container.Extension
import Data.Container.Morphism
import Data.Container.Product
import Data.Container.Tensor
import Data.Container.Coproduct
import Data.Sigma
import Data.Coproduct
import Data.Product
import Data.Iso

import Proofs

import Optics.Prism
import Data.DPair
import Data.Maybe

%hide Prelude.(&&)
```

Closed lenses are also called _linear lenses_ in the existing literature. However, I will present another data structure that, I think, suits the "linear" moniker much better. The lenses here are "closed" in the sense of cartesian closure, since they are defined using a single function. Where before we had a dependent lens be defined by two map $fwd : a → b$ and $bwd : (x : a) → b' (fwd\ x) → a'\ x$,
closed lenses refactor the common argument and make use of  a continuation to
obtain the resulting $a'\ x$ value: $(x : a) → Σ (y : b). (b'\ y → a'\ x)$. Here is the idris implementation:

```idris
public export
record Closed (c1, c2 : Container) where
  constructor MkClosed
  fn : (x : c1.req) -> Σ c2.req (\y => c2.res y -> c1.res x)
```

Very importantly, they are in isomorphism with our previous definition of dependent lenses:

```idris
public export
toClosed : a =%> b -> Closed a b
toClosed mor = MkClosed $
  \x => mor.fwd x  ## (\y => mor.bwd x y)

public export
fromClosed : Closed a b -> a =%> b
fromClosed (MkClosed g) =
  (\x => (g x).π1) <!
  (\x, y => (g x).π2 y)

public export
closedIso : Closed a b `Iso` (a =%> b)
closedIso = MkIso
  fromClosed
  toClosed
  (\(x <! y) => Refl)
  (\(MkClosed fn) => cong MkClosed (funExtDep $ \nx => dpairUniq _))
```

This means that everything that worked before, works on closed lenses as well.
They are also morphisms of containers, they also have a unit and composition that forms a category, etc.

Closed lenses have one feature that dependent lenses do not have: An explicit
continuation. The type `(x : c1.req) -> Σ c2.req (\y => c2.res y -> c1.res x)` from the
definition above says that, given a `c1.req` we obtain a `c2.req` _and_ a continuation
that will give us the position of the container in the domain.

This continuation structure is its most important feature. The first thing to notice, is that the return type of the closed lens is isomorphic to the extension of containers:

- `(x : c1.req) -> Σ c2.req (\y => c2.res y -> c1.res x)`
- `(x : c1.req) -> Ex c2 (c1.res x)`

This information is crucial because we can reuse all the knowledge we have about the extension of containers for closed lenses. For example, we can use the fact that container morphisms are maps of extensions to build an infix operator that will "apply" an extension to a lens.

```idris
export infix 1 $-
export
applyEx : Closed a b -> {0 r : Type} -> Ex a r -> Ex b r
applyEx cont = exMap' (fromClosed cont)
```

```idris
public export
record ClosedEx (c1, c2 : Container) where
  constructor MkClosedEx
  fn : (x : c1.req) -> Ex c2 (c1.res x)

-- Apply a closed lens to a "value"
($-) : ClosedEx a b -> {0 r : Type} -> Ex a r -> Ex b r
($-) cls ex = let res : Ex b (a.res ex.ex1); res = cls.fn ex.ex1 in MkEx res.ex1 (ex.ex2 . res.ex2)

($*) : ClosedEx a (b ⊗ c) -> {0 r : Type} -> Ex a r -> Ex b r * Ex c r
($*) cls (MkEx ee ec) = let MkEx (r1 && r2) fr = cls.fn ee in MkEx r1 ?f2 && MkEx r2 ?end

-- lift an idris value into a lens "value"
liftEx : (x : a) -> {0 b : a -> Type} -> Ex ((y : a) !> b y) (b x)
liftEx x = MkEx x id

-- Smart constructor to build lenses from values
StartClosed : ({x : a} -> Ex ((y : a) !> b y) (b x) -> Ex c (b x)) -> ClosedEx ((!>) a b) c
StartClosed f = MkClosedEx $ \arg => f (MkEx arg id)
```

Using the above we can build more complex lenses without making use of combinators. For example here is how to build a lens that accesses the second element of a triple:

```idris

data Sqrt : Type -> Type where

merge : Sqrt x -> Sqrt x -> x

lens_fst : ClosedEx (a * b :- c * b) (a :- c)
lens_fst = MkClosedEx $ \arg => MkEx arg.π1 (&& arg.π2)

lens_snd : ClosedEx (a * b :- a * d) (b :- d)
lens_snd = MkClosedEx $ \arg => MkEx arg.π2 (arg.π1 &&)

splitupl : (v : Ex (a ⊗ b) r)
       -> Ex a (b.res v.ex1.π2 -> r)
        * Ex b (b.res v.ex1.π2)
splitupl (MkEx (x1 && x2) f) = MkEx x1 (\g, a => f (g && a)) && MkEx x2 id

splitup : (v : Ex (a ⊗ b) r)
       -> Ex a (Sqrt r)
        * Ex b (Sqrt r)
splitup (MkEx (x1 && x2) f) =
  MkEx x1 ?sqrt1 && MkEx x2 ?sqrt2

joinUp : Ex a (Sqrt r) ->
         Ex b (Sqrt r) -> Ex (a ⊗ b) r
joinUp (MkEx va sq1) (MkEx vb sq2) =
  MkEx (va && vb) (\(v1 && v2) => merge (sq1 v1) (sq2 v2))

joinEx : (v1 : Ex a r) -> (v2 : Ex b s) -> Ex (a ⊗ b) (r * s)
joinEx v1 v2 = MkEx (v1.ex1 && v2.ex1) (\v3 => v1.ex2 v3.π1 && v2.ex2 v3.π2)

splitupr : (v : Ex (a ⊗ b) r)
       -> Ex a (a.res v.ex1.π1)
        * Ex b (a.res v.ex1.π1 -> r)
splitupr (MkEx (x1 && x2) f) = MkEx x1 id && MkEx x2 (\x, y => f (y && x))

namespace Product
  splitupr : (v : Ex (a * b) r)
         -> Ex b (b.res v.ex1.π2)
          * Ex a ( r)
  splitupr (MkEx (x1 && x2) f) = MkEx x2 id && MkEx x1 (\x => f (<+ x))

namespace Coproduct
  splitupr : (v : Ex (a + b) r)
         -> Coproduct.choice (\va => Ex a (a.res va)) (\vb => Ex b (b.res vb)) v.ex1
  splitupr (MkEx (<+ x) q) = MkEx x id
  splitupr (MkEx (+> x) q) = MkEx x id

pairupl : Ex a (s -> r) -> Ex b s -> Ex (a ⊗ b) r
pairupl (MkEx v1 f1) (MkEx v2 f2) = MkEx (v1 && v2) (\(x1 && x2) => f1 x1 (f2 x2))

StartPair : {0 a, b, r : Container} ->
            ((x1 : a.req) ->  (x2 : b.req) -> Ex r (a.res x1 * b.res x2)) -> ClosedEx (a ⊗ b) r
StartPair f = MkClosedEx (\v => f v.π1 v.π2)

StartTriple : {0 a, b, c, r : Container} ->
            ((x1 : a.req) ->  (x2 : b.req) -> (x3 : c.req) -> Ex r (a.res x1 * b.res x2 * c.res x3)) -> ClosedEx ((a ⊗ b) ⊗ c) r
StartTriple f = MkClosedEx (\v => f v.π1.π1 v.π1.π2 v.π2)

pairupr : Ex a s -> Ex b (s -> r) -> Ex (a ⊗ b) r
pairupr (MkEx v1 f1) (MkEx v2 f2) = MkEx (v1 && v2) (\(x1 && x2) => f2 x2 (f1 x1))

--         ┌──────┐
--   A─────┤      │
--         │  F1  │       ┌──────┐
--   B─────┤      ├───E───┤      │
--         └──────┘       │  F2  ├── D
--   C────────────────────┤      │
--                        └──────┘
op : (f1 : ClosedEx (a ⊗ b) e) -> (f2 : ClosedEx (e ⊗ c) d) -> ClosedEx ((a ⊗ b) ⊗ c) d
op f1 f2 = StartTriple $ \v1, v2, v3 =>
           let eval = f1 $- joinEx (MkEx v1 id) (MkEx v2 id)
               dval = f2 $- joinEx eval (MkEx v3 id)
            in dval

--
--                                            ┌──────┐
--   A────────────────────────────────────────┤      │
--         ┌──────┐                           │  F3  │         ┌──────┐
--   B─────┤      ├───F──────────────╲ ╱──────┤G     ├────H────┤      │
--         │  F1  │       ┌──────┐    ╳       └──────┘         │  F4  ├─────I
--   C─────┤      ├───E───┤      │   ╱ ╲───────────────────────┤F     │
--         └──────┘       │  F2  ├──╱                          └──────┘
--   D────────────────────┤      │
--                        └──────┘
--

lens_middle : {0 a, b, c, d, e, f, g, h, i : Container} ->
              (f1 : ClosedEx (b ⊗ c) (e ⊗ f)) ->
              (f2 : ClosedEx (e ⊗ d) g) ->
              (f3 : ClosedEx (a ⊗ g) h) ->
              (f4 : ClosedEx (h ⊗ f) i) ->
              ClosedEx (((a ⊗ b) ⊗ c) ⊗ d) i
lens_middle f1 f2 f3 f4 = MkClosedEx $ \(((va && vb) && vc) && vd) => let
    ee && ef = splitup (f1 $- joinEx (MkEx vb id) (MkEx vc id))
    gn = f2 $- joinUp ee (MkEx vd ?reag)
    eh = f3 $- joinUp ?add ?badd
    ival = f4 $- joinUp ?aa ?bb
    in ?adoiio
--                        ┌──────┐
--                      ┌─┤      │
--   A──────────────────┘ │  F3 H├─────┐
--         ┌──────┐     ┌─┤      │     │ ┌──────┐
--   B─────┤      ├───F─┘ └──────┘     └─┤      │
--         │  F1  │       ┌──────┐       │  F4  ├────I
--   C─────┤      ├───E───┤      │     ┌─┤      │
--         └──────┘       │  F2 G├─────┘ └──────┘
--   D────────────────────┤      │
--                        └──────┘

interface Normalizable ty sy | ty where
  normalize : ty -> sy

Normalisable : Type -> Maybe Type
Normalisable (a * (b * c)) = Just ((a * b) * c)
Normalisable _ = Nothing

get : {t : Maybe e} -> IsJust t ->  e
get {t = Nothing} x = absurd x
get {t = (Just y)} (ItIsJust) = y

Assoc : {t : Type} -> t -> (ev : IsJust (Normalisable t)) => get ev
Assoc {t = t1 * (t2 * t3)} (x1 && (x2 && x3)) {ev = ItIsJust}  = (x1 && x2) && x3
Assoc {t = _} _ {ev = ev}  = ?impossible_

no_crossing : {a, b, c, d, e, f, g, h, i : Container} ->
              (f1 : ClosedEx (b ⊗ c) (e ⊗ f)) ->
              (f2 : ClosedEx (e ⊗ d) g) ->
              (f3 : ClosedEx (a ⊗ f) h) ->
              (f4 : ClosedEx (h ⊗ g) i) ->
              ClosedEx (((a ⊗ b) ⊗ c) ⊗ d) i
no_crossing f1 f2 f3 f4 = MkClosedEx $ \(((va && vb) && vc) && vd) => let
    ee && ff = splitupl (f1 $- joinEx (MkEx vb id) (MkEx vc id))
    hval = f3 $- joinEx (MkEx va id) ff
    gval = f2 $- joinEx ee (MkEx vd id)
    ival = f4 $- joinEx hval gval
    ival' = exFunc Assoc ival
    in ?adn

```

As you can see, the last function makes use of the application operator to "compute" the output of a lens and bind it to a variable name. That name
can subsequently used to be applied to another lens to obtain a final result and return it.

We can write the same program using combinators, but the core is much less elegant.

```idris
lens_middle_comb : (swap : forall x, y . (x * y) =%> (y * x)) ->
              (mapSnd : forall x, y, a . x =%> y -> (a * x) =%> (a * y)) ->
              (a * b * c) =%> (c * (b * a))
lens_middle_comb swap mapSnd =
        swap {x = a * b, y = c} ⨾ mapSnd swap
```

Next: [[Linear.idr|Linear lenses]]
