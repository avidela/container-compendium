
## Linear lenses

Linear lenses emerge as a special-case of Closed lenses. They enforce
the fact that the input and the contiuation of close lenses _should_ only
be used once. This is done by making use of Idris' quantitative type system
and only requires a little bit of setup.

The main motivation for this change is that closed lenses, when used as
a DSL for string diagrams, do not properly capture the constraints of our
algebra. This is because, while inherently linear, the language in which
the lenses are written admits arbitrary duplication and erasure. But by
making all the binders of a closed lens linear, we ensure that the code written
exactly matches the semantics we would expect from a string-diagram DSL.

```idris
module Data.Container.Morphism.Linear

import Data.Container
import Data.Linear
import Data.Product
import Data.Coproduct

-- linear sigma
infix 7 @@
record Σ1 (a : Type) (f : a -> Type) where
  constructor (@@)
  1 π1 : a
  1 π2 : f π1
```

Linear lenses are [Closed lenses](Closed.idr.md) with a linear domain and
linear continuation. We can express this in idris using the `1` quantity
and a bespoke definition for a linear Σ-type.

```idris
public export
record Linear (c1, c2 : Container) where
  constructor MkSLin
  1 fn : ((1 x : c1.req) -> Σ1 c2.req (\y => c2.res y -@ c1.res x))
```

Obviously, they compose and form a category but not in the same way as
closed lenses. Due to the nature of quantities in Idris, we would
need to write a bespoke implementation of category using linear quantities
for everything, so instead I am only going to write the laws without
instanciating them in a `Category` record

```idris
compose : Linear a b -> Linear b c -> Linear a c
compose (MkSLin fn) (MkSLin gn) = MkSLin $ \arg =>
    let (arg2 @@ kn) = fn arg
        (arg3 @@ ln) = gn arg2
    in arg3 @@ (\1 x => kn (ln x))

identity : Linear a a
identity = MkSLin $ \arg => arg @@ id

idLeft : (f : Linear a b) -> compose identity f = f

idRight : (f : Linear a b) -> compose f identity = f

presCompose : (f : Linear a b) -> (g : Linear b c) -> (h : Linear c d) ->
              f `compose` (g `compose` h) = (f `compose` g) `compose` h
```

Just like closed lenses and dependent lenses, you can use them with non-dependent
boundaries.

```idris
fromConst1 : (1 y : b) -> ((1 x : a) -> (1 _ : b' y) -> a' x) -> Linear ((!>) a a') ((!>) b b')
fromConst1 y f = MkSLin $ \1 arg => y @@ f arg

fromConst2 : (1 fwd : a -@ b) -> ((0 x : a) -> (1 _ : b' (fwd x)) -> a' x) -> Linear ((!>) a a') ((!>) b b')
fromConst2 fwd bwd = MkSLin $ \arg =>
  fwd arg @@ bwd arg

fstLens : Linear ((LPair a a :- LPair b a)) (a :- b)
fstLens = MkSLin weirdFst
  where
    weirdFst : ((1 x : LPair a a) -> Σ1 a (\y => (1 _ : b) -> LPair b a))
    weirdFst (fst # snd) = fst @@ (# snd)

-- linear tensor
public export
(×) : (c1, c2 : Container) -> Container
(×) c1 c2 = (x : LPair c1.req c2.req) !> case x of (x1 # x2) => LPair (c1.res x1) (c2.res x2)

data LEither : Type -> Type -> Type where
  (<+) : a -@ LEither a b
  (+>) : b -@ LEither a b

-- linear coproduct
(+) : (c1, c2 : Container) -> Container
(+) c1 c2 =
    (x : LEither c1.req c2.req) !>
    case x of
        (<+ x1) => c1.res x1
        (+> x2) => c2.res x2

parallel : Linear a b -@ Linear x y -@ Linear (a × x) (b × y)
parallel (MkSLin fn) (MkSLin gn) = MkSLin $ \(a1 # a2) =>
  let 1 (bv @@ fn') = fn a1
      1 (yv @@ gn') = gn a2
  in (bv # yv) @@ (\case (v1 # v2) => fn' v1 # gn' v2)

```
