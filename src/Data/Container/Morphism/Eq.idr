module Data.Container.Morphism.Eq

import Data.Container.Definition
import Data.Container.Morphism.Definition

import Proofs

import public Control.Relation
import public Control.Order
import Data.Iso

%hide Control.Relation.Rel

export infix 0 `DepLensEq`
export infix 0 <%≡%>

||| Two container morphisms are equal if their mapping on shapes are equal and their
||| mapping on positions are equal.
public export
record (<%≡%>) (a, b : dom =%> cod) where
  constructor MkDepLensEq
  0 eqFwd : (v : dom.req) -> a.fwd v === b.fwd v
  0 eqBwd : (v : dom.req) -> (y : cod.res (a.fwd v)) ->
          let 0 p1 : dom.res v
              p1 = a.bwd v y
              0 p2 : dom.res v
              p2 = b.bwd v (replace {p = cod.res} (eqFwd v) y)
          in p1 === p2


record (<≡≡>) (a, b : dom =%> cod) where
  constructor MkDepLensEq'
  0 eqFwd : (v : dom.req) -> a.fwd v === b.fwd v
  0 eqBwd : (v : dom.req) -> (y : cod.res (a.fwd v)) ->
          let 0 p1 : dom.res v
              p1 = a.bwd v y
              0 p2 : dom.res v
              p2 = b.bwd v (replace {p = cod.res} (eqFwd v) y)
          in p1 === p2


export
0 depLensEqToEq : a <%≡%> b -> a === b
depLensEqToEq {a = (fwd1 <! bwd1)} {b = (fwd2 <! bwd2)} (MkDepLensEq eqFwd eqBwd) =
  cong2Dep' (<!) (funExt eqFwd) (funExtDep $ \x => funExt $ \y => eqBwd x y)

public export
Transitive (dom =%> cod) (<%≡%>) where
  transitive a b = MkDepLensEq (\v => transitive (a.eqFwd v) (b.eqFwd v))
      (\v, w => transitive
           (a.eqBwd v w)
           (b.eqBwd v (replace {p = cod.res} (a.eqFwd v) w)))

public export
Reflexive (dom =%> cod) (<%≡%>) where
  reflexive = MkDepLensEq (\_ => Refl) (\_, _ => Refl)

public export
Preorder (dom =%> cod) (<%≡%>) where

public export
Symmetric (dom =%> cod) (<%≡%>) where
  symmetric (MkDepLensEq eqFwd eqBwd) =
    MkDepLensEq (\x => sym (eqFwd x)) (\a, b => let v = eqBwd a ?www in ?end)

||| An isomorphism of container morphisms
public export
ContIso : (x, y : Container) -> Type
ContIso = GenIso Container (=%>) (<%≡%>)

HRel : Type -> Type -> Type
HRel ty sy = ty -> sy -> Type

Rel : Type -> Type
Rel ty = HRel ty ty

0 HIRel : {x : Type} -> (x -> Type) -> (x -> Type) -> Type
HIRel p q = (i : x) -> HRel (p i) (q i)


-- interface ITransitive ty (rel : HRel ty ty) | rel where
--   constructor MkTransitive
--   transitive : {x, y, z : ty} -> rel x y -> rel y z -> rel x z

0 IRel : {x : Type} -> (x -> Type) -> Type
IRel p = (i : x) -> Rel (p i)

over : {f, g : x -> Type} -> HIRel f g  -> {i , j : x} -> i === j -> HRel (f i) (g j)
over a Refl {i = i, j = i} = a i

record IType (ty : Type) where
  constructor MkIType
  idx : ty -> Type

record DepLensRel (a, b : dom =%> cod)
                  (feq : (x, y : Type) -> Rel (x -> y))
                  (ieq : (x : dom.req) -> HRel (cod .res (a .fwd x) -> dom .res x)
                                               (cod .res (b .fwd x) -> dom .res x))
                  where
  constructor MkDepLensRel
  0 eqFwd : feq _ _ a.fwd b.fwd
  0 eqBwd : (v : dom.req) -> ieq v (a.bwd v) (b.bwd v)

-- rel2 x
--   (\x => f .bwd x (g .bwd (f .fwd x) (h .bwd (g .fwd (f .fwd x)) x)))
--   (\x => f .bwd x (g .bwd (f .fwd x) (h .bwd (g .fwd (f .fwd x)) x)))
attempt1 :
           (f : a =%> b) -> (g : b =%> c) -> (h : c =%> d) ->
           (rel1 : (x, y : Type) -> Rel (x -> y)) ->
           (rel2 : (x : a.req) -> HRel (d.res (((f ⨾ g) ⨾ h).fwd x) -> a.res x)
                                       (d.res ((f ⨾ (g ⨾ h)).fwd x) -> a.res x)) ->
           ((x, y : Type) -> Preorder (x -> y) (rel1 x y)) =>
           DepLensRel
               ((f ⨾ g) ⨾ h)
               (f ⨾ (g ⨾ h))
               rel1
               rel2
attempt1 f g h rel1 rel2 =
  MkDepLensRel
    reflexive
    (\x => ?attempt1_rhs)

