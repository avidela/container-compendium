<!-- idris
module Data.Container.Product

import Data.Container.Definition
import Data.Container.Category

import Data.Category.Product
import Data.Category.Bifunctor
import Data.Category.Notation

import Data.Coproduct
import Data.Product

import Proofs


public export
-->

### Cartesian Product on Containers

The cartesian product of container is distinct from the tensor product
in that the responses are combined with a coproduct, rather than a product.

In

```idris
(*) : (c1, c2 : Container) -> Container
(*) c1 c2 = (x : c1.req * c2.req) !> c1.res x.π1 + c2.res x.π2
```

The product is also a bifunctor using both the fact that both the product
and the coproduct on _types_ are bifunctors. That is why in the implementation
of `(~*~)`, we see `bimap` appear twice, but the first one operates on
`(*) : Type -> Type -> Type` and the second one operates on `(+) : Type -> Type -> Type`.

<!-- idris
public export
-->

```idris
(~*~) : a =%> a' -> b =%> b' -> a * b =%> a' * b'
(~*~) m1 m2 =
    (bimap m1.fwd m2.fwd) <!
    (\x => bimap (m1.bwd x.π1) (m2.bwd x.π2))
```

Building the proof that the cartesian product is a bifunctor in $\Cont$ is easy
but it must be done in order to implement more interesting structures.

```idris
public export
ProductBifunctor : Bifunctor Cont Cont Cont
ProductBifunctor = MkFunctor
    (uncurry (*))
    (\_, _ => uncurry (~*~))
    (\(x && y) => depLensEqToEq $ MkDepLensEq
        (\z => projIdentity z)
        (\z => bifunctorId')
    )
    (\(x1 && x2), (y1 && y2), (z1 && z2), (m1 && m2), (n1 && n2) =>
      depLensEqToEq $ MkDepLensEq
        (\(vx && vy) => Refl)
        (\(vz && vy), vz => bimapCompose vz)
    )
```

On

```idris
proj1 : {0 c1, c2 : Container} -> (c1 * c2) =%> c1
proj1 = π1 <! (\x => (<+))

proj2 : {0 c1, c2 : Container} -> (c1 * c2) =%> c2
proj2 = π2 <! (\x => (+>))

contProd : c =%> a -> c =%> b -> c =%> a * b
contProd x y = (\v => x.fwd v && y.fwd v) <!
               (\v => choice (x.bwd v) (y.bwd v))

mkMorphismInjL : a <! b = c <! d -> a = c
mkMorphismInjL Refl = Refl

mkMorphismInjR : a <! b = a <! d -> b = d
mkMorphismInjR Refl = Refl

mkMorphismPrf : {x : Type} -> {x' : x -> Type} ->
                {y : Type} -> {y' : y -> Type} ->
                {w : Type} -> {w' : w -> Type} ->
                {z : Type} -> {z' : z -> Type} ->
                {a : x -> y} -> {c : (v : x) -> y' (a v) -> x' v} ->
                {b : w -> z} -> {d : (v : w) -> z' (b v) -> w' v} ->
                a ~=~ b -> c ~=~ d ->
                the ((!>) x x' =%> (!>) y y') (a <! c) ~=~ the ((!>) w w' =%> (!>) z z') (b <! d)

mkMorphismInj : {a, b : Container} ->
                {f1 : a.req -> b.req} -> {b1 : (v : a.req) -> b.res (f1 v) -> a.res v} ->
                {f2 : a.req -> b.req} -> {b2 : (v : a.req) -> b.res (f2 v) -> a.res v} ->
                the (a =%> b) (f1 <! b1) ===
                the (a =%> b) (f2 <! b2) ->
                (prf : f1 === f2 ** (rewrite sym prf in b1) === b2)
mkMorphismInj Refl = (Refl ** Refl)
```

```idris
0 contProdUniq : {0 a, b, c : Container} ->
               {f1 : c =%> a} ->
               {f2 : c =%> b} ->
               {p : c =%> a * b} ->
               p ⨾ proj1 {c1 = a, c2 = b} = f1 -> p ⨾ proj2 {c1 = a, c2 = b} = f2 -> contProd {a} {b} {c} f1 f2 = p
contProdUniq {p = (fwd <! bwd)}
  {f1 = (g1 <! s1)} {f2 = (g2 <! s2)}
  prf1 prf2 =
  let p1 : (\x => (fwd x).π1) === g1
      p1 = mkMorphismInjL prf1
      p1' : (x : c.req) -> (fwd x).π1 === g1 x
      p1' x = rewrite sym p1 in Refl
      p2 : (\x => (fwd x).π2) === g2
      p2 = mkMorphismInjL prf2
      p2' : (x : c.req) -> (fwd x).π2 = g2 x
      p2' x = rewrite sym p2 in Refl
      p3 :  (x : c.req) -> (fwd x).π1 && (fwd x).π2 = (fwd x)
      p3 v = projIdentity (fwd v)
      0 p4 : (\x => (fwd x).π1 && (fwd x).π2) === fwd
      p4 = funExt p3
      sprf1 : (\z, x => bwd z ((<+) (rewrite (p1' z) in x))) === s1
      sprf1 = let (Refl ** v) =  (mkMorphismInj prf1) in v
      sprf2 : (\z, x => bwd z ((+>) (rewrite p2' z in x))) === s2
      sprf2 = let (Refl ** v) =  (mkMorphismInj prf2) in v

  in rewrite sym p1
  in rewrite sym p2
  in rewrite p4
  in cong (fwd <!) $
      funExt2Dep {b = \x => a .res ((fwd x).π1) + b .res ((fwd x).π2)} {c = c.res} $ \x, y =>
          case y of
               (+> v) => rewrite sym sprf2 in Refl
               (<+ v) => rewrite sym sprf1 in Refl

-- The category of containers has products with *
public export
ContProd : HasProduct Cont
ContProd = MkProd
  (*)
  proj1
  proj2
  contProd
  (\(m1 <! m1'), m2 => Refl)
  (\m1, (m2 <! m2') => Refl)
  contProdUniq
```
