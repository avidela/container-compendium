## Maybe Monad on containers

The main contribution of this work is in this module: Monads on containers.

While monads on containers aren't a surprising fact in itself, it is noteworthy
to see that they provide us with similar capabilities than monads on types and
functions, but in the realm of bidirectional programming.

```idris
module Data.Container.Maybe.Monad

import Data.Category.Monad
import Data.Category.Bifunctor
import Data.Category.Functor
import Data.Category.NaturalTransformation
import Data.Container.Morphism
import Data.Container.Descriptions.Maybe
import Data.Container.Maybe
import Data.Sigma

import Proofs.Void
import Proofs.Extensionality
import Proofs.Congruence
import Proofs.Sigma
import Proofs.Unit

%hide Data.Category.(|>)
%hide Prelude.Ops.infixl.(*>)
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)
```

We start by defining the `Maybe` Monad on container. This monad is given
by the `Maybe ○` functor that is defined in [Data.Container.Maybe](src/Data/Container/Maybe.idr.md).
We alias it to the name `MaybeAny : Container -> Container` which displays its functorial nature directly.
The use of `○` is meaningful since it is the composition operator on containers. The intuition behind the
composition operator is that all the holes of the container on the left of it are plugged by instances of
the container to the right of it. This way `Maybe ○ x` really means "fill up the blanks from `Maybe` with
`x`" which is exactly what we also do when we write `Maybe x` when `Maybe : Type -> Type` is a functor on
$\mathcal{Type}$.

Monads are identified by their `pure : 1 -> m a` and `join : m (m a) -> m a` operation. Here `m` is
`Maybe ○` and `1` is the identity functor on containers defined as `MkCont Unit (const Void)`.
With this knowledge, we build the unit mapping:

```idris
unit : (a : Container) -> a =%> MaybeAny a
```

If this signature looks familiar, it's because we've already defined it in
[Data.Container.Descriptions.Maybe](src/Data/Container/Descriptions/Maybe.idr.md), it's `just`.


```idris
unit _ = just
```

We're not done however, because we now need to prove that `just` is _natural_, that is, given any other morphism `m : a =%> b`, composing `unit` after `m` is the same as functorially mapping `m` after `unit`: $m ; \text{unit}_b = \text{unit}_a ; \text{MaybeAny}(m)$. Or graphically, the following
diagram commutes:


Programatically it means we need to implement the function:

```idris
%unbound_implicits off
MaybeAnyIsNatural : {0 a, b : Container} ->
                    (m : a =%> b) ->
                    m ⨾ unit b <%≡%> unit a ⨾ mapHom MaybeFunctor a b m
MaybeAnyIsNatural (fwd <! bwd) = MkDepLensEq
    (\arg => cong (MkEx True) Refl)
    (\arg, wrg => Refl)
```

Thankfully the proofs are quite simple.

Those two functions are enough to define the first part of our monad: the unit natural transformation:

```idris
-- there is a natural transformation between the identity functor in $Cont$ and the $Maybe$ functor
unitNT : Functor.idF Cont =>> MaybeFunctor
unitNT = MkNT
    unit
    (\_, _, arg => depLensEqToEq (MaybeAnyIsNatural arg))
```

The second part is the multiplication natural transformation, which states that for any monad $m$ we have $m (m\ a) \to m\ a$, in our case, this takes the form of a morphism
```idris
mult : (x : Container) -> MaybeAny (MaybeAny x) =%> MaybeAny x
```

Again, we've already seen this before, it's `join` from
[Data.Container.Descriptions.Maybe](src/Data/Container/Descriptions/Maybe.idr.md).

```idris
mult container = join {x = container}
```

It is left to prove that `join` is natural, we do the same as we did for unit:

```idris
JoinIsNatural : {0 a, b : Container} ->
    (m : a =%> b) ->
    mapHom MaybeFunctor (MaybeAny a) (MaybeAny b) (mapHom MaybeFunctor a b m) ⨾ mult b
    <%≡%> mult a ⨾ mapHom MaybeFunctor a b m
```

Which is the expected naturality square $\text{MaybeAny} (\text{MaybeAny}(m)) ; mult = mult ; (\text{MaybeAny}(m))$.

```tikz
\usepackage{tikz-cd}
\usepackage{amsfonts}
\begin{document}
\begin{tikzcd}
x && {Maybe(Maybe (x))} && {Maybe(x)} \\
\\ y && {Maybe(Maybe(y))} && {Maybe(y)}
\arrow["{mult_x}", from=1-3, to=1-5]
\arrow["{Maybe(Maybe(m))}"', from=1-3, to=3-3]
\arrow["{mult_y}"', from=3-3, to=3-5]
\arrow["{Maybe(m)}", from=1-5, to=3-5]
\arrow["m", from=1-1, to=3-1]
\end{tikzcd}
\end{document}
```

The proof of this square is a bit more involved:

```idris
JoinIsNatural m = MkDepLensEq
    fwdEq
    eqBwd
    where
      fwdEq : (c : MaybeType (MaybeType a.req)) ->
              (mapHom MaybeFunctor (MaybeAny a) (MaybeAny b) (mapHom MaybeFunctor a b m) ⨾ mult b).fwd c
          === (mult a ⨾ mapHom MaybeFunctor a b m).fwd c
      fwdEq (MkEx True f) with (f ())
        fwdEq (MkEx True f) | (MkEx True p) = Refl
        fwdEq (MkEx True f) | (MkEx False p) = cong (MkEx False) (allUninhabited _ _)
      fwdEq (MkEx False f) = cong (MkEx False) (allUninhabited _ _)


      0 eqBwd : (v : MaybeType (MaybeType a.req)) ->
                (y : (MaybeAny b).res ((mapHom MaybeFunctor (MaybeAny a) (MaybeAny b) (mapHom MaybeFunctor a b m) ⨾ mult b).fwd v)) ->
            let 0 p1 : (MaybeAny (MaybeAny a)).res v
                p1 = (mapHom MaybeFunctor (MaybeAny a) (MaybeAny b) (mapHom MaybeFunctor a b m) ⨾ mult b).bwd v y
                0 p2 : (MaybeAny (MaybeAny a)).res v
                p2 = (mult a ⨾ mapHom MaybeFunctor a b m).bwd v (replace {p = (MaybeAny b).res} (fwdEq v) y)
            in p1 === p2
      eqBwd (MkEx True f) y with (f ()) proof p'
        eqBwd (MkEx True f) (() ## y) | (MkEx True p) = cong (() ##) (rewrite p' in Refl)
        eqBwd (MkEx True f) y | (MkEx False p) = absurd y.π1
      eqBwd (MkEx False f) y = absurd y.π1
joinCommutes : (0 x, y : Container) -> (m : x =%> y) ->
  let 0 top : MaybeAny (MaybeAny x) =%> MaybeAny x
      top = mult x

      0 bot : MaybeAny (MaybeAny y) =%> MaybeAny y
      bot = mult y

      0 left : MaybeAny (MaybeAny x) =%> MaybeAny (MaybeAny y)
      left = mapHom (MaybeFunctor !*> MaybeFunctor) x y m

      0 right : MaybeAny x =%> MaybeAny y
      right = mapHom MaybeFunctor x y m

  in top ⨾ right <%≡%> left ⨾ bot
joinCommutes x y m = MkDepLensEq
  fwdEq
  bwdEq
    where
      0 top : MaybeAny (MaybeAny x) =%> MaybeAny x
      top = mult x

      0 bot : MaybeAny (MaybeAny y) =%> MaybeAny y
      bot = mult y

      0 left : MaybeAny (MaybeAny x) =%> MaybeAny (MaybeAny y)
      left = mapHom (MaybeFunctor !*> MaybeFunctor) x y m

      0 right : MaybeAny x =%> MaybeAny y
      right = mapHom MaybeFunctor x y m

      fwdEq : (c : MaybeType (MaybeType x.req)) ->
              (top ⨾ right).fwd c ===
              (left ⨾ bot).fwd c
      fwdEq (MkEx True c) with (c ())
        fwdEq (MkEx True c) | (MkEx True p) = cong (MkEx True) Refl
        fwdEq (MkEx True c) | (MkEx False p) = cong (MkEx False) (allUninhabited _ _)
      fwdEq (MkEx False c) = cong (MkEx False) (allUninhabited _ _)

      0
      bwdEq : (v : MaybeType (MaybeType x.req)) ->
              (w : (MaybeAny y).res ((top ⨾ right).fwd v)) ->
            let 0 p1 : (MaybeAny (MaybeAny x)).res v
                p1 = (top ⨾ right).bwd v w
                0 p2 : (MaybeAny (MaybeAny x)).res v
                p2 = (left ⨾ bot).bwd v
                       (rewrite sym $ fwdEq v in w)
            in p1 === p2
      bwdEq (MkEx True fn) yn with (fn ()) proof pf
        bwdEq (MkEx True fn) (() ## yn) | (MkEx True p1)
          = cong (() ##) (sigEqToEq $ MkSigEq Refl (rewrite pf in Refl))
        bwdEq (MkEx True fn) yn | (MkEx False p1) = absurd yn.π1
      bwdEq (MkEx False v) y = absurd y.π1

-- Join is a natural transformation
joinNT : (MaybeFunctor !*> MaybeFunctor) =>> MaybeFunctor
joinNT = MkNT
  mult
  (\a, b, m => depLensEqToEq (joinCommutes a b m))
```

Equipped with those two natural transformation, and the `MaybeAny` functor, we have proven that our functor is also a monad. We can make sure of that fact by building the monad record defined in
[Data.Category.Monad](src/Data/Category/Monad.idr.md):

```idris
MaybeAnyMonad : Monad Cont
MaybeAnyMonad = MkMonad
  { endo = MaybeFunctor
  , unit = unitNT
  , mult = joinNT
  }

```
