<!-- idris
module Data.Container.Sequence

import Data.Container.Definition
import Data.Container.Category
import Data.Container.Cartesian
import Data.Container.Cartesian.Category
import Data.Container.Extension
import Data.Container.Morphism.Definition
import Data.Container.Morphism.Eq

import Data.Sigma
import Data.Iso

import Proofs

import Data.Category.Bifunctor

public export
-->

### Existential composition

[[#^e44b25|Earlier]], I said that containers _describe_ data structures and the extension
is what makes the description take form as a type inside our programming language.
Again, the way this is used can be seen in
[Data.Container.Descriptions.List](src/Data/Container/Descriptions/List.idr.md). For
now, we use the extension of container to define the last two operation on containers
`○` and `#>`. The first one is well known and is called the "substitution product", or
"container composition", and allows one data definition to be embedded inside another.
Typically if we want to describe a list of lists, we would write this using `○`
`CList ○ CList` where `CList` is the container that describes lists. You can convince
yourself this does the right thing with the intuition that $List \circ List \cong List(List(x))$

```idris
(○) : Container -> Container -> Container
(○) c1 c2 =
  (x : Ex c1 c2.request) !>
  (Σ (c1.response x.ex1) (c2.response . x.ex2))
```

There is another interpretation of this operation as a "request/response" modifier. Indeed
the request part, we use the extension of the first container applied to the requests
of the second `Ex c1 c2.request`. This means that the requests of the compositions
are the datatype described by `c1` containing values of type `c2.request`. Concretely,
if `c1` is the container for lists, then the new requests are _lists of c2_.
Additionally, in the response part, we return a Σ-type where the first projection is a
_position in c1_ accompanied with a response of `c2`. Again, using lists as an example
it means that we get a pair of the position of the response in the original list, along with
the response to the query at that position.

![[IMG_0176.jpeg]]

In other words, if we have a container $c = (query \rhd response)$ and we apply it
to $ListC \circ c$ where $ListC$ is the list container, we obtain the container
$(List\ query \rhd Any\ response)$, indicating that given a list of queries, we obtain
exactly one response. This pattern holds for any container we give as first argument
$desc \circ c$ results in the container where the requests are values defined by
the polynomial functor $desc$ and the responses is a pointer inside the type given by
$desc$ analogous to the relationship between `List` and `List.Any`, or `Maybe`
and `Maybe.Any`.

This is going to come in very handy when using $○$ as the defining operator to build
[_monads_](../Category/Monad.idr.md) on containers.

<!-- idris
public export
-->
```idris
composeUnits : End ○ End =%> End
composeUnits = (const MkUnit) <! (const (const (MkUnit ## MkUnit)))
```

### Existential Composition is a Bifunctor

For this work, we will only rarely use the product on containers, although I felt like
it was important to showcase for illustrative purposes. Another fact that we would like to ensure
is that `(○)` is a proper bifunctor.

<!-- idris
public export
-->
```idris
(~○~) :
  {0 a, a', b, b' : Container} ->
  (a =%> a') -> (b =%> b') -> a ○ b =%> a' ○ b'
(~○~) m1 m2 = (<!)
  (\x => MkEx (m1.fwd x.ex1)
              (\y => m2.fwd (x.ex2 (m1.bwd x.ex1 y))))
  (\x, y => (m1.bwd x.ex1 y.π1)
         ## m2.bwd (x.ex2 (m1.bwd x.ex1 y.π1)) y.π2)


public export
ComposeContBifunctor : Bifunctor Cont Cont Cont
ComposeContBifunctor = MkFunctor
  (Product.uncurry (○))
  (\_, _, m => m.π1 ~○~ m.π2)
  (\x => depLensEqToEq $ MkDepLensEq
      (\v2 => exEqToEq $ MkExEq
          (ExProj1 x.π1 x.π2.req v2) (\_ => Refl))
      (\v, v2 => sigEqToEq $ MkSigEq Refl Refl))
  (\_, _, _, f, g => Refl)

public export
composeFunctor :
  {0 a, x, y : Container} ->
  (x =%> y) ->
  a ○ x =%> a ○ y
composeFunctor = (identity a ~○~)
```

Not incredibly surprising but necessary for the next step: Containers are monoidal in `○` and
`End` as neutral element.

### The Category of Container is Monoidal in `○` and `End`

To achieve this proof, we need a number of lemmas about composition:

<!-- idris
public export
-->

```idris
mapForward : (0 a, b : Container * Container)
    -> (a.π1 =#> b.π1) * (a.π2 =#> b.π2)
    -> Ex a.π1 a.π2.req
    -> Ex b.π1 b.π2.req
mapForward a b m x = MkEx (m.π1.cfwd x.ex1) (m.π2.cfwd . x.ex2 . (m.π1.cbwd x.ex1).to)
```
<!-- idris
public export
-->
```idris
mapBackward : (0 a, a', b, b' : Container) ->
    (m1 : a =#> a') -> (m2 : b =#> b') ->
    (x : (a ○ b).req) -> Iso
    ((a' ○ b').res (mapForward (a && b) (a' && b') (m1 && m2) x))
    ((a ○ b).res x)
mapBackward a a' b b' m1 m2 x = MkIso
    (\y => (m1.cbwd x.ex1).to y.π1
        ## (m2.cbwd $ x.ex2 ((m1.cbwd x.ex1).to y.π1)).to y.π2)
    (\y => (m1.cbwd x.ex1).from y.π1 ##
        rewrite__impl (b'.res . m2.cfwd . x.ex2)
          ((m1.cbwd (x .ex1)).toFrom y.π1)
          ((m2.cbwd (x.ex2 y.π1)).from y.π2))
    (\(a1 ## a2) => let
        pp : (m1.cbwd x.ex1).to ((m1.cbwd x.ex1).from a1) === a1
        pp = ((m1.cbwd x.ex1).toFrom a1)
        qq : (m2.cbwd (x.ex2 a1)).to ((m2.cbwd (x.ex2 a1)).from a2) === a2
        qq = (m2.cbwd (x.ex2 a1)).toFrom a2
        in
        cong2Dep (##) pp (rewrite pp in qq))
    (\(a1 ## a2) => rewrite (m1.cbwd x.ex1).fromTo a1
                 in rewrite (m2.cbwd (x.ex2 ((m1.cbwd x.ex1).to a1))).fromTo a2
                 in Refl)
```

<!-- idris
public export
-->
```idris
(~○#~) :
    {0 a, a', b, b': Container} ->
    (a =#> a') -> (b =#> b') ->
    a ○ b =#> a' ○ b'
(~○#~) m1 m2 = MkCartDepLens
    (mapForward (a && b) (a' && b') (m1 && m2))
    (mapBackward a a' b b' m1 m2)
```

<!-- idris
public export
-->
```idris
0 circBifunctorPreservesIdentity :
  (v : Container * Container) ->
  (identity v.π1) ~○#~ (identity v.π2)
  `CartDepLensEq` identity (uncurry (○) v)
circBifunctorPreservesIdentity v = MkCartDepLensEq
   (\xs => congDep (MkEx (xs .ex1)) (funExt $ \vn => cong xs.ex2 (idTo vn)) `trans` exUniq xs)
   (\_ => MkIsoEq dpairUniq dpairUniq)
```

<!-- idris
public export
-->
```idris
0 circBifunctorPreservesComposition :
    (c1, c2, c3 : Container * Container) ->
    (f : (c1.π1 =#> c2.π1) * (c1.π2 =#> c2.π2)) ->
    (g : (c2.π1 =#> c3.π1) * (c2.π2 =#> c3.π2)) ->
    (f.π1 |#> g.π1) ~○#~ (f.π2 |#> g.π2) ≡
       (f.π1 ~○#~ f.π2 |#> g.π1 ~○#~ g.π2)
circBifunctorPreservesComposition (c1 && c1') (c2 && c2') (c3 && c3')
    (fp1 && fp2) (gp1 && gp2) = cartEqToEq $ MkCartDepLensEq
    (\x => Refl)
    (\(MkEx x1 x2) => MkIsoEq
        (\y => Refl)
        (\(y1 ## y2) => sigEqToEq $ MkSigEq
            Refl
            (rewrite (fp1.cbwd x1).toFrom y1 in Refl)
        )
    )
```

<!-- idris
public export
-->
```idris
ComposeContCartBifunctor : Bifunctor ContCart ContCart ContCart
ComposeContCartBifunctor = MkFunctor
  (Product.uncurry (○))
  (\x, y, m => m.π1 ~○#~ m.π2)
  (\x => cartEqToEq (circBifunctorPreservesIdentity x))
  circBifunctorPreservesComposition
```
