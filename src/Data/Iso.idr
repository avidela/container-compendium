module Data.Iso

import Control.Relation

import Proofs

import Data.Product
import Data.Coproduct
import Data.Category

import Syntax.PreorderReasoning
import Control.Order

-- an abstract implementation of Iso for any preorder relation
public export
record GenIso (carrier : Type)
              (mor : Rel carrier)
              (rel : {0 x, y : carrier} -> Rel (mor x y))
              {auto pre : Preorder carrier mor}
              (left, right : carrier) where
  constructor MkGenIso
  to : mor left right
  from : mor right left
  0 toFrom : rel (transitive to from) Relation.reflexive
  0 fromTo : rel (transitive from to) Relation.reflexive

-- a special case of `GenIso` for functions and types with equality
public export
record Iso (left, right : Type) where
  constructor MkIso
  to : left -> right
  from : right -> left
  0 toFrom : (x : right) -> to (from x) ≡ x
  0 fromTo : (x : left) -> from (to x) ≡ x

public export
record IsoEq (i1, i2 : Iso left right) where
  constructor MkIsoEq
  0 eqTo : (x : left) -> i1.to x ≡ i2.to x
  0 eqFrom : (x : right) -> i1.from x ≡ i2.from x

export
symIsoEq : IsoEq a b -> IsoEq b a
symIsoEq (MkIsoEq et tf) = MkIsoEq (\x => sym (et x)) (\x => sym (tf x))

public export
identityIso : (0 x : Type) -> Iso x x
identityIso x = MkIso
  id
  id
  (\_ => Refl)
  (\_ => Refl)

export
idTo : (x : a) -> ((identityIso a).to x) = x
idTo x = Refl

export
idFrom : (x : a) -> ((identityIso a).from x) = x
idFrom x = Refl

public export
transIso : {0 x, y, z : Type} -> Iso x y -> Iso y z -> Iso x z
transIso iso1 iso2 =
  MkIso
    (iso2.to . iso1.to)
    (iso1.from . iso2.from)
    (\v => cong iso2.to (iso1.toFrom (iso2.from v)) `trans` iso2.toFrom v)
    (\v => trans (cong iso1.from (iso2.fromTo (iso1.to v))) (iso1.fromTo v))

public export
congIso : {0 t1, s1 : Type} ->
          {0 f1, f2 : t1 -> s1} ->
          {0 b1, b2 : s1 -> t1} ->
          {0 fb1 : (x : s1) -> f1 (b1 x) === x} ->
          {0 fb2 : (x : s1) -> f2 (b2 x) === x} ->
          {0 bf1 : (x : t1) -> b1 (f1 x) === x} ->
          {0 bf2 : (x : t1) -> b2 (f2 x) === x} ->
          (p1 : f1 === f2) ->
          (p2 : b1 === b2) ->
          (p3 : fb1 === (rewrite p1 in rewrite p2 in fb2)) ->
          (p4 : bf1 === (rewrite p2 in rewrite p1 in bf2)) ->
          MkIso f1 b1 fb1 bf1 === MkIso f2 b2 fb2 bf2
congIso Refl Refl Refl Refl = Refl

export
0 fromIsoEq : (a, b : Iso left right) -> IsoEq a b -> a === b
fromIsoEq (MkIso to1 from1 _ _) (MkIso to2 from2 _ _)
    (MkIsoEq eqTo eqFrom ) = congIso (funExt eqTo) (funExt eqFrom) (funExtDep $ \_ => UIP _ _) (funExtDep $ \_ => UIP _ _)

export
transIsoTo : (transIso iso1 iso2).to vx === iso2.to (iso1.to vx)
transIsoTo = Refl

export
transIsoAssoc : {0 a, b, c, d : Type} ->
                 (f : Iso a b) -> (g : Iso b c) -> (h : Iso c d) ->
                 (f `transIso` (g `transIso` h)) `IsoEq`
                 ((f `transIso` g) `transIso` h)
transIsoAssoc f g h = MkIsoEq (\x => Refl) (\_ => Refl)

export
symIso : Iso x y -> Iso y x
symIso (MkIso to from toFrom fromTo) = MkIso from to fromTo toFrom

export
isoIdRight : (f : Iso a b) -> transIso f (identityIso b) `IsoEq` f
isoIdRight (MkIso to from toFrom fromTo) = MkIsoEq (\_ => Refl) (\_ => Refl)

export
isoIdLeft : (f : Iso a b) -> transIso (identityIso a) f `IsoEq` f
isoIdLeft (MkIso to from toFrom fromTo) = MkIsoEq (\_ => Refl) (\_ => Refl)


export
Reflexive Type Iso where
  reflexive = identityIso _

export
Transitive Type Iso where
  transitive = transIso

export
Symmetric Type Iso where
  symmetric = symIso

export
Equivalence Type Iso where

public export
IsoCoprod : Iso a b -> Iso x y -> Iso (a + x) (b + y)
IsoCoprod iso1 iso2 = MkIso
  (bimap iso1.to iso2.to)
  (bimap iso1.from iso2.from)
  (\case (+> r) => cong (+>) (iso2.toFrom r)
         (<+ l) => cong (<+) (iso1.toFrom l))
  (\case (+> r) => cong (+>) (iso2.fromTo r)
         (<+ l) => cong (<+) (iso1.fromTo l))

public export
IsoProd : Iso a b -> Iso x y -> Iso (a * x) (b * y)
IsoProd iso1 iso2 = MkIso
  (bimap iso1.to iso2.to)
  (bimap iso1.from iso2.from)
  (\(x && y) => cong2 (&&) (iso1.toFrom x) (iso2.toFrom y))
  (\(x && y) => cong2 (&&) (iso1.fromTo x) (iso2.fromTo y))

||| Types and isomorphisms form a category
public export
IsoCat : Category Type
IsoCat = NewCat
  { objects = Type
  , morphisms = Iso
  , identity = (\x => identityIso _)
  , composition = transIso
  , identity_right = (\x => fromIsoEq _ _ (isoIdRight x))
  , identity_left = (\x => fromIsoEq _ _ (isoIdLeft x))
  , compose_assoc = (\x, y, z => fromIsoEq _ _ (transIsoAssoc x y z))
  }

liftProof : {0 x, y : a} -> (iso : Iso a b) -> x === y -> iso.to x === iso.to y
liftProof iso Refl = Refl


export
appIso : {0 a, b : Type} ->
         {0 g : b -> Type} ->
         (iso : Iso a b) ->
         ((val : a) -> g (to iso val)) ->
         (vx : b) -> (vz : a) -> vz === from iso vx ->
         g vx
appIso iso f vx (from iso vx) Refl =
  replace {p = g} (iso.toFrom vx) (f $ from iso vx)

export
appIsoId : {a : Type} -> {g : a -> Type} -> (vx : (x : a) -> g x) ->
           (gx : a) ->
           appIso {a, b = a, g} (identityIso a) vx gx gx Refl = vx gx
appIsoId vx gx = Refl

export
appIsoId' : {a : Type} -> {g : a -> Type} -> (vx : (x : a) -> g x) ->
           (gx : a) -> (gy : a) -> (prf : gy ≡ from (identityIso a) gx) ->
           (≡) (appIso {a, b = a, g} (identityIso a) vx gx gy prf) (vx gx)
               {a = g gx}
appIsoId' vx gx gx Refl = Refl

{-
appIsoTrans : {iso1 : Iso a b} -> {iso2 : Iso b c} ->
              {g : c -> Type} ->
              (vx : (val : a) -> g (to (transIso iso1 iso2) val)) ->
              (vz : a) ->
              (fn : _) ->
              fn vz (appIso {a, b = c, g} (transIso iso1 iso2) vx vz ((transIso iso1 iso2).from vz) Refl)
            ≡ appIso iso2 (\zn : b => fn zn (appIso iso1 vx zn (iso1.from zn) Refl)) vz (iso2.from vz) Refl

{-
bleu : (iso1 : Iso a b) ->
       (iso2 : Iso b c) ->
       (x : b) ->
       {gee : c -> Type} ->
       {b2 : (v : b) -> gee } ->
    (b2
      x
      (appIso {fx = a, f'x = c, g = gee}
        (transIso iso1 iso2)
        vx
        x
        ((transIso iso1 iso2).from x)
        Refl
      )
    ) =
    (appIso
      iso2
      (\zn  => b2
        (iso2.to zn)
        (appIso
          iso1
          vx
          zn
          (iso1.from zn)
          Refl
        )
      )
      x
      (iso2.from x)
      Refl
    )
