module Data.Product

import public Data.Ops
import Data.Vect

%default total
%hide Prelude.(&&)
%hide Prelude.Num.(*)

||| Pairs of types
public export
record (*) (a, b : Type) where
  constructor (&&)
  π1 : a
  π2 : b

%pair (*) π1 π2

public export
elim : (a -> a') -> (b -> b') -> (a' -> b' -> c) -> a * b -> c
elim f g m x = f x.π1 `m` g x.π2

public export
bi : (a -> x) -> (b -> y) -> a * b -> x * y
bi f1 f2 p = elim f1 f2 (&&) p

||| Map each element of the pair and combine the results into one
public export
merge : (a -> c) -> (b -> c) -> (c -> c -> c) -> a * b -> c
merge = elim

||| Map each element of the pair and combine the results into one using
||| the monoid on `c`
public export
merge' : Monoid c => (a -> c) -> (b -> c) -> a * b -> c
merge' f g = merge f g (<+>)

public export
Show a => Show b => Show (a * b) where
  show (a && b) = "\{show a} & \{show b}"

||| Swap the two elements of a product
public export
swap : a * b -> b * a
swap x = x.π2 && x.π1

||| Convert from a product to a pair
public export
toPair : a * b -> (a, b)
toPair x = (x.π1, x.π2)

||| Convert from a pair to a product
public export
fromPair : (a, b) -> (a * b)
fromPair x = fst x && snd x

||| Products have a bifunctor insttance
public export
Bifunctor (*) where
  bimap = bi

||| Duplicate an element
public export
dup : a -> a * a
dup x = x && x

public export
distribute : a * b -> c * d -> (a * c) * (b * d)
distribute x y = (x.π1 && y.π1) && (x.π2 && y.π2)

export
fork : (a -> b) -> (a -> c) -> a -> b * c
fork f g x = f x && g x

export
split : (a * b) * c -> (a * c) * (b * c)
split ((a && b) && c) = (a && c) && (b && c)

||| Like bimap but with two arguments
export
through : (a -> b -> c) -> (x -> y -> z) -> (a * x) -> (b * y) -> (c * z)
through f g x y = f x.π1 y.π1 && g x.π2 y.π2

||| From arity 2 to arity 1 with pair
public export
curry : (a * b -> c) -> a -> b -> c
curry f a b = f (a && b)

||| From arity 2 to arity 1 with pair
public export
uncurry : (a -> b -> c) -> a * b -> c
uncurry f x = f x.π1 x.π2

public export
shuffle : (a * x) * (b * y) -> (a * b) * (x * y)
shuffle ((a && x) && (b && y)) = (a && b) && (x && y)

public export
proj1Pair : (0 a, b : _) -> (a && b).π1 === a
proj1Pair _ _ = Refl

public export
proj2Pair : (0 a, b : _) -> (a && b).π2 === b
proj2Pair _ _ = Refl

public export
(^) : Type -> Nat -> Type
(^) a Z = Unit
(^) a (S 1) = a
(^) a (S n) = a * a ^ n

public export
assocL : (a * b) * c -> a * (b * c)
assocL x = x.π1.π1 && (x.π1.π2 && x.π2)

public export
assocR : a * (b * c) -> (a * b) * c
assocR x = (x.π1 && x.π2.π1) && x.π2.π2

public export
FreeProd : List Type -> Type
FreeProd [] = Unit
FreeProd (x :: []) = x
FreeProd (x :: (y :: xs)) = x * FreeProd (y :: xs)

namespace Pair
  -- Cartesian product of pairs
  public export
  cartesian : (a, b) -> (x, y) -> ((a, x), (b, y))
  cartesian (z, v) (w, s) = ((z, w), (v, s))

||| A product of n values of type a
public export
Prod : (n : Nat) -> (a : Type) -> Type
Prod Z x = Unit
Prod (S Z) x = x
Prod (S n) x = x * Prod n x

||| Convert a vector of n elements into a product of n values of type a
export
toProduct : Vect n a -> Prod n a
toProduct [] {n = Z} = ()
toProduct (x :: []) {n = S 0} = x
toProduct (x :: xs) {n = S (S k)} = x && toProduct xs

public export
projIdentity : (x : a * b) -> (x.π1 && x.π2) === x
projIdentity (a && b) = Refl
