<!-- idris
module Data.Category.Functor

import public Data.Category

import Syntax.PreorderReasoning

import Proofs

%hide Prelude.Functor
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)
%hide Prelude.Ops.infixl.(*>)

%unbound_implicits off

public export
-->

## Functors

Categories hold the compositional structure of objects and morphisms. But just
like any other structure, we are also interested in maps _between_ them. In this
case, a mapping between two categories $\cat{C}, \cat{D}$ is given by a map on objects
$\textit{mapObj} : |\cat{C}| → |\cat{D}|$ and a map on morphisms
$\textit{mapHom} : \forall x, y \in |\cat{C}|. \cat{C}(x, y) \to \cat{D}(\mathit{mapObj}(x), \mathit{mapObj}(y))$.
Such mapping is called a _functor_, we can represent it as a record
holding both maps.

```idris
record Functor {0 o1, o2 : Type} (c : Category o1) (d : Category o2) where
  constructor MkFunctor

  -- A way to map objects
  mapObj : o1 -> o2

  -- A way to map morphisms
  -- For each morphism in C between objects x and y
  -- we get a morphism in D between the corresponding objects x and y
  -- but mapped to their counterparts in d
  mapHom : (x, y : o1) -> x ~> y -> mapObj x ~> mapObj y
```

In addition to the maps themselves, we need to ensure the map on morphism preserves
identity. That is, an identity arrow in the source category needs to map to an
identity arrow in the target category. Using the categories $\cat{C}, \cat{D}$ defined
above, we need to ensure that $\textit{mapHom}(id_{\cat{C}}) \equiv id_{\cat{D}}$

```idris
  -- mapping the identity morphism in C results in the identity morphism in D
  0 presId : (v : o1) -> mapHom v v (c.id v) = d.id (mapObj v)
```

Finally, the map on morphism needs to preserve composition, such that, given
two maps $f : \cat{C}(x, y)$ and $g : \cat{C}(y, z)$ for three objects
$x, y, z \in |\cat{C}|$ we can compose their maps in $\cat{D}$
$\textit{mapHom}(g) \circ_{\cat{D}} \textit{mapHom}(f)$ in a way that coincides with composing the morphisms in
$\cat{C}$ and mapping the result into $\cat{D}$, $\textit{mapHom}(g \circ_{\cat{C}} f)$. We
can show this relation in the triangular diagram:

```tikz {label="fig:functor-hom" caption="The commutative diagram for the fact that mapHom preserves composition"}
\usepackage{tikz-cd}
\usepackage{amsfonts}
\begin{document}
\begin{tikzcd}
{mapObj(x)} && {mapObj(y)} \\
\\
& {mapObj(z)}
\arrow["{mapHom(f)}", from=1-1, to=1-3]
\arrow["{mapHom(g \circ f)}"', from=1-1, to=3-2]
\arrow["{mapHom(g)}", from=1-3, to=3-2]
\end{tikzcd}
\end{document}
```

We add it as a field to our `Functor` record:

```idris
  0 presComp :
    (x, y, z : o1) -> -- given three objects x, y, z
    (f : x ~> y) ->   -- a morphism x -> y
    (g : y ~> z) ->   -- and a morphism y -> z

    let 0 f1, f2 : mapObj x ~> mapObj z
        -- Mapping the morphism after composition
        f1 = mapHom x z (f |> g)
        -- And composing the maps of each morphism
        f2 = mapHom x y f |> mapHom y z g
    in f1 === f2 -- Are the same thing
```

Functors compose in the expected way, that is, if you have a functor $F : c \to d$ and $G : d \to e$ then you can compose them such that
their composite is $F ⨾⨾ G : c \to d$.
<!-- idris
public export
-->
```idris
(!*>) : {0 o1, o2, o3 : Type} -> {0 a : Category o1} -> {0 b : Category o2} -> {0 c : Category o3} ->
      Functor a b -> Functor b c -> Functor a c
(!*>) f1 f2 = MkFunctor
  (f2.mapObj . f1.mapObj)
  (\a, b, m => f2.mapHom (f1.mapObj a) (f1.mapObj b) (f1.mapHom a b m))
  (\x => cong (f2.mapHom (f1.mapObj x) (f1.mapObj x)) (f1.presId x)
        `trans` f2.presId (f1.mapObj x))
  (\a, b, c, h, j =>
      cong (f2.mapHom (f1.mapObj a) (f1.mapObj c)) (f1.presComp a b c h j) `trans`
      f2.presComp _ _ _ (f1.mapHom a b h) (f1.mapHom b c j))
```

While this definition is not strictly necessary for defining functors, we use it as a helper instead of `mapHom`.
The name is a reference to the function `fmap` in Haskell.

```idris
-- functorial mapping of morphisms
public export
fmap : {0 o1, o2 : Type} ->
      {c1 : Category o1} -> {c2 : Category o2} -> {f : Functor c1 c2} ->
      {a, b : o1} ->
      a ~> b -> f.mapObj a ~> f.mapObj b
fmap x = mapHom f a b x
```

With this basic definition we can build a couple of interesting functors, the first one is
the identity functor. This functor maps a category to itself.

```idris
public export
idF : {0 o : Type} -> (0 c : Category o) -> Functor c c
idF c = MkFunctor id (\_, _ => id) (\_ => Refl) (\_, _, _, _, _ => Refl)
```

Another interesting functor is the functor from the discrete category to
any category given one of its objects. Think of it as the function `() -> a`
given `x : a`.

```idris
public export
DiscreteObj : {0 o : Type} -> o -> (c : Category o) -> Functor DiscreteCat c
DiscreteObj x c = MkFunctor
    (const x) (\_, _, _ => c.id x) (\_ => Refl)
    (\_, _, _, _, _ => sym $ c.idLeft _ _ (c.id x) )
```

### Relation Between the `Functor` Interface and Functors in Category Theory

This is obvious to anyone familiar with functional programming and category theory, but I've seldom seen it written down explicitly so
I've decided to take this time to properly write it down. Some programming languages have a notion of `Functor`
that is chiefly given by the `map` function. `map` has different signatures for different types, for example `map` on lists has the type
`(a -> b) -> List a -> List b`. `map` on `Maybe` has the type `(a -> b) -> Maybe a -> Maybe b`.

If we abstract away the difference and put together what is in common
we obtain something of the shape: `map : (a -> b) -> m a -> m b`,
where `m` stands in for both `List` and `Maybe`. To appropriately write
this as a definition of a programming language, we need to explain what `m` is,
and that will depend on the programming environement. In Idris, `m` is a
function `Type -> Type`, indicating that it takes a `Type` in argument and
returns `Type`. In Haskell, or Scala, `m` is a _higher-kinded type_, that is, a
type that operates on types, the syntax for it in Haskell is `m : * -> *` or
`m: m[_]` in Scala. Once we have some well defined notion for `m` in each
language, we can write a programming interface, in Idris it's an `interface`,
in Haskell a `typeclass` or in Scala a `trait` that will define the common
wrapper around any `map` function, this is the definition of _Functor_ in a
programming language.

```scala
trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B]
}
```

```haskell
class Functor f where
    fmap :: (a -> b) -> f a -> f b
```

```{idris module=F}
interface Functor (f : Type -> Type) where
  map : (a -> b) -> f a -> f b
```

We just defined functors as a categorical construct in Idris, so surely there
is an obvious link between what we just did and those different programming
definitions?

Their relation lies in the signature of `f`. In all three definitions above,
`f` always has the same function: to stand in for types with a type parameter.
It was always an operation that manipulates _types_. This means that the
`Functor` interface in those languages is not actually a generic functor
between two categories, but it's _a template for endofunctors_ (functors with the same
source and target) of the category of types. Because we already defined the
category of types and functions (as the category `Set`), and we just defined
`Functor` above, we can define this template as the functor `Functor Set Set`.

Now we can relate our `List` and `Maybe` functors as _values_ of `Functor Set Set`:

```idris
ListF : Functor Set Set
ListF = MkFunctor
    { mapObj = List
    , mapHom = \_, _ => map
    , presId = \a => funExt listMapId
    , presComp = \a, b, c, f, g => funExt (listMapComp f g)
    }
  where
    listMapId : {a : Type} -> (xs : List a) -> map Basics.id xs === xs
    listMapId [] = Refl
    listMapId (x :: xs) = cong (x ::) (listMapId xs)

    listMapComp : {a, b, c : Type} ->
                  (f : a -> b) -> (g : b -> c) -> (xs : List a) ->
                  map (g . f) xs === map g (map f xs)
    listMapComp f g [] = Refl
    listMapComp f g (x :: xs) = cong (g (f x) ::) (listMapComp f g xs)

MaybeF : Functor Set Set
MaybeF = MkFunctor
    { mapObj = Maybe
    , mapHom = \_, _ => map
    , presId = \_ => funExt $ \case Nothing => Refl
                                    (Just x) => Refl
    , presComp = \_, _, _, f, g => funExt $ \case Nothing => Refl
                                                  (Just x) => Refl
    }
```

From this implementation we see two things: Firstly, the map on morphism for
both is given by the function `map : (a -> b) -> List a -> List b` and
`map : (a -> b) -> Maybe a -> Maybe b`, and secondly, using our above definition
requires us to _prove_ that our functor behaves properly, something the
programming languages mentioned above usually do not.

Finally we can confirm that the `map` function is indeed the map on morphism of
endofunctors on `Type` by projecting it out and writing their type explicitly:

```idris
parameters {a, b : Type}

  mapList : (a -> b) -> List a -> List b
  mapList = mapHom ListF a b

  mapMaybe : (a -> b) -> Maybe a -> Maybe b
  mapMaybe = mapHom MaybeF a b
```

