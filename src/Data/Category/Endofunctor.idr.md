<!-- idris
module Data.Category.Endofunctor

import Data.Category
import Data.Product
import Data.Category.NaturalTransformation as NT
import Data.Category.Monoid
import Data.Category.Functor
import Data.Category.FunctorCat
import Data.Category.Bifunctor
import Syntax.PreorderReasoning

import Pipeline.Equality
-->


We can use the idea that a natural transformation is a mapping between functors
to reproduce facts that we know ought to work. For example, since functor composition
is associative, we should be able to define a natural isomorphism between
$(f \circ g) \circ h$ and $f \circ (g \circ h)$.

```idris
parameters
  {o1, o2, o3, o4 : Type}
  {c1 : Category o1} {c2 : Category o2} {c3 : Category o3} {c4 : Category o4}
  (f : Functor c1 c2) (g : Functor c2 c3) (h : Functor c3 c4)
  public export
  funcCompAssocNT' : (f !*> (g !*> h)) =>> ((f !*> g) !*> h)
  funcCompAssocNT' = MkNT
      (\vx => c4.id _)
      (\x, y, m => c4.idLeft _ _ _ `trans` sym (c4.idRight _ _ _) )

  public export
  funcCompAssocNT : (f !*> g) !*> h =>> f !*> (g !*> h)
  funcCompAssocNT = MkNT
      (\vx => c4.id _)
      (\x, y, m => c4.idLeft _ _ _ `trans` sym (c4.idRight _ _ _) )

  public export
  funcCompAssocNI : (f !*> g) !*> h =~= f !*> (g !*> h)
  funcCompAssocNI = MkNaturalIsomorphism
      funcCompAssocNT
      (\vx => c4.id _)
      (\vx => c4.idLeft _ _ _)
      (\vx => c4.idLeft _ _ _)
```

There is still more we can do, for example, we can provide mappings from
any functor that is composed with the identity, to itself, essentially
proving that the identity functor is a neutral element with regards to
functor composition when the maps are natural transformations. Some of those
results will be used for monads.

```idris
parameters
  {o1, o2 : Type}
  {c : Category o1} {d : Category o2}
  {f : Functor c d}
  public export
  funcIdRNT : idF c !*> f =>> f
  funcIdRNT = MkNT (\vx => d.id _)
    (\x, y, m => d.idLeft _ _ _ `trans` sym (d.idRight _ _ _))

  public export
  funcIdLNT : f !*> idF d =>> f
  funcIdLNT = MkNT (\vx => d.id _)
    (\x, y, m => d.idLeft _ _ _ `trans` sym (d.idRight _ _ _))

  public export
  funcIdLNT' : f =>> f !*> idF d
  funcIdLNT' = MkNT (\vx => d.id _)
    (\x, y, m => d.idLeft _ _ _ `trans` sym (d.idRight _ _ _))

  public export
  funcIdRNT' : f =>> idF c !*> f
  funcIdRNT' = MkNT (\vx => d.id _)
    (\x, y, m => d.idLeft _ _ _ `trans` sym (d.idRight _ _ _) )
```

Actually the above help us define the monoidal category of endofunctors,
unlike `FunctorCat`, we fix one category $\mathcal{C}$ instead of two, and objects
are endofunctors $\mathcal{C}\to\mathcal{C}$

[[@cooperLinksWebProgramming2007]]

We just mentionned _endofunctors_ and quickly said that they were functors with the same
source and target. Here we make this a bit more precise by giving a function `Endo` that
will only build endofunctors. With this we can build the category `EndoCat` of endofunctors
as objects and natural transformations as morphisms

```idris
public export
Endo : Category o -> Type
Endo c = Functor c c

public export
EndoCat : (c : Category o) -> Category (Functor c c)
EndoCat c = FunctorCat c c
```

Because endofunctors compose on the nose, since their source and target are the same, we can
ask ourselves if the category of endofunctors is monoidal. This monoidal structure is particularly
interesting because the objects are endofunctor and the monoidal multiplication is the composition
of endofunctors. The neutral element of this monoidal structure is the identity endofunctor.

```idris

parameters {0 o : Type} {c : Category o}
  mapHomComp :
     (m1 : Functor c c * Functor c c) ->
     (m2 : Functor c c * Functor c c) ->
     (m3 : (m1.π1 =>> m2.π1) * (m1.π2 =>> m2.π2)) ->
     m1.π1 !*> m1.π2 =>> m2.π1 !*> m2.π2
  mapHomComp m1 m2 m3 = m3.π1 -*- m3.π2

  %hide Prelude.Ops.infixl.(|>)
  %hide Data.Category.Ops.infixl.(<~)

  0
  presIdComp : (v : Endo c * Endo c) ->
    (mapHomComp v v (NT.identity {f = v.π1} && NT.identity {f = v.π2}))
           === NT.identity {f = v.π1 !*> v.π2}
  presIdComp v = ntEqToEq $ MkNTEq $ \vx =>
    let cMor : o -> o -> Type ; cMor = (~:>) c
        (|>>) : forall x, y, z. x `cMor` y -> y `cMor` z -> x `cMor` z
        (|>>) = (|:>) c
        private infixl 1 |>>
        steps : ProofSpec ? (v.π2.mapObj (v.π1.mapObj vx) `cMor` v.π2.mapObj (v.π1.mapObj vx))
        steps = [ (NT.identity {f = v.π1} -*- NT.identity {f = v.π2}).component vx
                , v.π2.mapHom (v.π1.mapObj vx) (v.π1.mapObj vx) (c.id (v.π1.mapObj vx)) |>>
                  v.π2.mapHom (v.π1.mapObj vx) (v.π1.mapObj vx) (v.π1.mapHom vx vx (c.id vx))
                , c.id (v.π2.mapObj (v.π1.mapObj vx)) |>>
                  v.π2.mapHom (v.π1.mapObj vx) (v.π1.mapObj vx) (v.π1.mapHom vx vx (c.id vx))
                , v.π2.mapHom (v.π1.mapObj vx) (v.π1.mapObj vx) (v.π1.mapHom vx vx (c.id vx))
                , (v.π1 !*> v.π2).mapHom vx vx (c.id vx)
                , (NT.identity {f = v.π1 !*> v.π2}).component vx
                ]
    in Prove steps
      [ Refl
      , cong (|>> v.π2.mapHom (v.π1.mapObj vx) (v.π1.mapObj vx) (v.π1.mapHom vx vx (c.id vx)))
             (v.π2.presId (v.π1.mapObj vx))
      , c.idLeft (v.π2.mapObj (v.π1.mapObj vx)) (v.π2.mapObj (v.π1.mapObj vx)) ((v.π1 !*> v.π2).mapHom vx vx (c.id vx))
      , Refl
      , Refl
      ]


  0
  presCompComp : (x, y, z : Endo c * Endo c) ->
                 (f : (x.π1 =>> y.π1) * (x.π2 =>> y.π2)) -> (g : (y.π1 =>> z.π1) * (y.π2 =>> z.π2)) ->
                 (f.π1 !!> g.π1) -*- (f.π2 !!> g.π2) === (f.π1 -*- f.π2) !!> (g.π1 -*- g.π2)
  presCompComp x y z (f1@(MkNT f1c f1p) && f2@(MkNT f2c f2p)) (g1@(MkNT g1c g1p) && g2@(MkNT g2c g2p)) = ntEqToEq $ MkNTEq $ \vx =>
    let cMor : o -> o -> Type ; cMor = (~:>) c
        (|>>) : forall x, y, z. x `cMor` y -> y `cMor` z -> x `cMor` z
        (|>>) = (|:>) c
        private infixl 1 |>>
    in Calc $
        |~ ((f1 !!> g1) -*- (f2 !!> g2)).component vx
        ~~ ((f2c (x.π1.mapObj vx) |>> g2c (x.π1.mapObj vx)) |>>
               (z.π2.mapHom (x.π1.mapObj vx) (z.π1.mapObj vx) (f1c vx |>> g1c vx)))
            ...(Refl)
        ~~ (f2c (x.π1.mapObj vx) |>> (g2c (x.π1.mapObj vx) |>>
               (z.π2.mapHom (x.π1.mapObj vx) (z.π1.mapObj vx) (f1c vx |>> g1c vx))))
            ...(?assoc)
        ~~ (f2c (x.π1.mapObj vx) |>> (g2c (x.π1.mapObj vx) |>>
               (z.π2.mapHom (x.π1.mapObj vx) (z.π1.mapObj vx) (f1c vx |>> g1c vx))))
            ...(?assoc2)
        ~~ ((f2c (x.π1.mapObj vx) |>> y.π2.mapHom (x.π1.mapObj vx) (y.π1.mapObj vx) (f1c vx)) |>>
               (g2c (y.π1.mapObj vx) |>> z.π2.mapHom (y.π1.mapObj vx) (z.π1.mapObj vx) (g1c vx)))
               ...(?wha)
        ~~ ((f2c (x.π1.mapObj vx) |>> y.π2.mapHom (x.π1.mapObj vx) (y.π1.mapObj vx) (f1c vx)) |>>
               (g2c (y.π1.mapObj vx) |>> z.π2.mapHom (y.π1.mapObj vx) (z.π1.mapObj vx) (g1c vx)))
              ...(?lol)
        ~~ ((f1 -*- f2) !!> (g1 -*- g2)) .component vx ...(Refl)

  monMultCompose :
    Bifunctor (EndoCat c) (EndoCat c) (EndoCat c)
  monMultCompose = MkFunctor
      (uncurry (!*>))
      (\m1, m2, m3 => mapHomComp m1 m2 m3)
      presIdComp
      presCompComp

  compNeutral : Functor DiscreteCat (EndoCat c)
  compNeutral = DiscreteObj (idF c) _

  alpha : let f1, f2 : Functor (EndoCat c * (EndoCat c * EndoCat c)) (EndoCat c)
              f1 = (((idF (EndoCat c)) `pair` monMultCompose) !*> monMultCompose) {b = EndoCat c * EndoCat c}
              f2 = assocR {a = EndoCat c, b = EndoCat c, c = EndoCat c} !*> ((monMultCompose `pair` idF (EndoCat c)) !*> monMultCompose)
          in f1 =~= f2
  alpha = MkNaturalIsomorphism
      (MkNT ?compAlpha ?alpha_rhs)
      ?reverse
      ?prof1
      ?prof2

  EndoMonoidal : Monoidal (FunctorCat c c)
  EndoMonoidal = MkMonoidal
    { mult = monMultCompose
    , I = compNeutral
    , alpha = alpha
    , leftUnitor = ?moreLeft
    , rightUnitor = ?moreRight
    }
```
