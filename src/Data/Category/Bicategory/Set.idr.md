f1 -*- f2) .comp vn)


horizontalMorNT : (a, b, c : Σ Type Category) ->
                  Bifunctor (FunctorCat a.π2 b.π2) (FunctorCat b.π2 c.π2) (FunctorCat a.π2 c.π2)
horizontalMorNT (o1 ## c1) (o2 ## c2) (o3 ## c3) = MkFunctor
    (uncurry (*>))
    (\f1, f2, m => m.π1 -*- m.π2)
    (\(f1 && f2) => ntEqToEq $ horzId f1 f2)
    (\a, b, c, f, g => ntEqToEq $ horzComp a b c f g)

SetBicategory : Bicategory (Σ Type Category)
SetBicategory = MkBiCat
    (\x, y => Functor x.π2 y.π2)
    (\x, y => FunctorCat x.π2 y.π2)
    horizontalMorNT
```
