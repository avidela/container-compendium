module Data.Category.Bifunctor

import public Data.Category.Functor
import public Data.Category.Product

import Proofs

%hide Prelude.(&&)
%hide Prelude.Ops.infixl.(*>)

public export
Bifunctor : {0 o1, o2, o3 : Type} -> Category o1 -> Category o2 -> Category o3 -> Type
Bifunctor a b c = Functor (ProductCat a b) c

public export
assocR : Functor (ProductCat a (ProductCat b c)) (ProductCat (ProductCat a b) c)
assocR = MkFunctor
  Product.assocR
  (\k, l => Product.assocR)
  (\v => Refl)
  (\_, _, _, f, g => Refl)

public export
assocL : Functor (ProductCat (ProductCat a b) c) (ProductCat a (ProductCat b c))
assocL = MkFunctor
  Product.assocL
  (\k, l => Product.assocL)
  (\v => Refl)
  (\_, _, _, f, g => Refl)

public export
swap : {a : _} -> Functor (ProductCat a b) (ProductCat b a)
swap = MkFunctor
  Product.swap
  (\x, y => Product.swap)
  (\v => Refl)
  (\_, _, _, f, g => Refl)

-- given a bifunctor (a × b → x) and a unit category given by a single object ∈ a,
-- and identities on that object, return the functor (b → c)
public export
applyBifunctor : {0 o1, o2, o3 : Type} ->
                 {a : Category o1} ->
                 {0 b : Category o2} ->
                 {0 c : Category o3} ->
    o1 -> Bifunctor a b c -> Functor b c
applyBifunctor x mult = MkFunctor
  (curry mult.mapObj x)
  (\k, l, m => mult.mapHom (x && k) (x && l) (a.id x && m))
  (\v => mult.presId (x && v) )
  (\ax, bx, cx, f, g =>
      let k = mult.presComp (x && ax) (x && bx) (x && cx)
                          (a.id x && f) (a.id x && g)
      in cong (mult .mapHom (x && ?) (x && ?)) (
         cong (&& (|:>) b f g) (sym (a.idLeft _ _ (a.id x)))) `trans` k)

public export
applyBifunctor' : {0 a : Category o} -> {b : Category o} -> o -> Bifunctor a b c -> Functor a c
applyBifunctor' x mult = MkFunctor
  (\y => mult.mapObj (y && x))
  (\k, l, m => mult.mapHom (k && x) (l && x) (m && b.id x))
  (\v => mult.presId (v && x))
  (\ax, bx, cx, f, g => let k = mult.presComp (ax && x) (bx && x) (cx && x)
                                            (f && b.id x) (g && b.id x)
             in (cong (mult .mapHom (? && x) (? && x))
                $ cong ((|:>) a f g &&)
                $ sym $ b.idRight _ _ $ b.id x) `trans` k)

public export
pair : Functor a b -> Functor c d -> Functor (ProductCat a c) (ProductCat b d)
pair f1 f2 = MkFunctor
  (bimap f1.mapObj f2.mapObj)
  (\x, y, z => f1.mapHom x.π1 y.π1 z.π1 && f2.mapHom x.π2 y.π2 z.π2 )
  (\x => cong2 (&&) (f1.presId x.π1) (f2.presId x.π2))
  (\ax, bx, cx, f, g => cong2 (&&) (f1.presComp _ _ _ f.π1 g.π1) (f2.presComp _ _ _ f.π2 g.π2))

public export
unitL : Functor c (TrivialCat * c)
unitL = MkFunctor (() &&) (\_, _ => (() &&)) (\v => Refl) (\a, b, c, f, g => Refl)

public export
unitR : Functor c (c * TrivialCat)
unitR = MkFunctor (&& ()) (\_, _ => (&& ())) (\v => Refl) (\a, b, c, f, g => Refl)

public export
fromUnit : {0 o : Type} -> o -> {a : Category o} -> Functor DiscreteCat a
fromUnit x = MkFunctor
    (\_ => x) (\_, _, _ => a.id _) (\_ => Refl)
    (\_, _, _, _, _ => sym $ a.idLeft _ _ _)

public export
app1 : {0 o1, o2, o3 : Type} ->
       {a : Category o1} ->
       {0 b : Category o2} ->
       {0 c : Category o3} ->
       o1 -> Bifunctor a b c -> Functor b c
app1 x bif = unitL !*> pair (fromUnit x) (idF _) !*> bif

