## Natural transformations

Functors are the tools to relate two categories, in turn natural transformations are the tool to relate two functors.

<!-- idris
module Data.Category.NaturalTransformation

import public Data.Category.Functor
import public Data.Category.Iso
import public Data.Category.Notation
import public Data.Category.Proofs

import Syntax.PreorderReasoning

import Proofs.Congruence
import Proofs.Extensionality
import Proofs.UIP

%hide Prelude.Functor
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

%unbound_implicits off
-->

A natural transformation is a relation between two _functors_ between two categories. Let's call them $f$ and $g$ and say they are between categories $\mathcal{C}$ and $\mathcal{D}$. We're going to say that they have maps on objects $f : \mathcal{C} \to\mathcal{D}$ and $g : \mathcal{C} \to\mathcal{D}$, as well as maps on morphisms $F: \mathcal{C}(x, y) \to\mathcal{D}(f(x), f(y))$ and $G: \mathcal{C}(x, y) \to\mathcal{D}(g(x), g(y))$.

A natural transformation is defined primarily by its _component_ which says that for each object in $\mathcal{C}$ we obtain a morphism in $\mathcal{D}$ by using the map on object from functors $f$ and $g$, we call the component $\varphi$. More succinctly we write $\varphi : \forall x \in |\mathcal{C}|. \mathcal{D}(f(x), g(x))$.

We can already start writing this definition in Idris using a record, we are going to use the symbol `==>>` for natural transformations, it is meant to represent a thick arrow.

<!-- idris
public export
-->
```idris
record (=>>) {0 cObj, dObj : Type}
             {0 c : Category cObj} {0 d : Category dObj}
             (f, g : Functor c d) where
  constructor MkNT
  component : (v : cObj) -> f.mapObj v ~> g.mapObj v
```

In the above you see that each natural transformation needs two functors, and each of them require two categories. The component is defined as a morphism in the category `d` resulting from mapping the object `v` using both `f` and `g`.

The definition does not end here because we also need to make sure that the component respects the map on morphism from each functor. For this, we ensure that each morphism in $\mathcal{C}$ applied to $g$ can be pre-composed with the components, or post-composed and applied to the functor $f$, for the same result. That is $∀ m ∈ \mathcal{C}(x, y). G(m) \circ \varphi_x = \varphi_y\circ F(m)$, we can represent it with the following commutative diagram.

```tikz {caption="Naturality square" label="fig:nat-square"}
\usepackage{tikz-cd}
\usepackage{amsfonts}
\begin{document}
\begin{tikzcd}
x \ar[d, "m" left] & f(x) \ar[r, "\varphi_x" above]
         \ar[d, "F(m)" left] & g(x) \ar[d, "G(m)" right] \\
y & f(y) \ar[r, "\varphi_y" below] & g(y)\\
\end{tikzcd}
\end{document}
```

It is left to translate this diagram into a field for our idris definition. As the diagram suggests, we need two objects in $\mathcal{C}$ as the source and target of a morphism $m$ in $\mathcal{C}$, and using, we ensure that the equation holds.

```idris
  0 commutes : (0 x, y : cObj) -> (m : x ~> y) ->
      let -- We build each side of the naturality square
          0 top : f.mapObj x ~> g.mapObj x
          top = component x

          0 bot : f.mapObj y ~> g.mapObj y
          bot = component y

          0 left : f.mapObj x ~> f.mapObj y
          left = f.mapHom _ _ m

          0 right : g.mapObj x ~> g.mapObj y
          right = g.mapHom _ _ m

          -- And the compose them.
          0 comp1 : f.mapObj x ~> g.mapObj y
          comp1 = top |> right

          0 comp2 : f.mapObj x ~> g.mapObj y
          comp2 = left |> bot

      in comp1 === comp2
```

The simplest natural transformation we can build is the identity natural
transformation. Given a functor `f : c -> d` we can build the identity natural
transformation using `f` on the identity morphism in `c`.

<!-- idris
public export
-->
```idris
identity : {0 o1, o2 : Type} -> {c : Category o1} -> {d : Category o2} ->
           {f : Functor c d} -> f =>> f
identity = MkNT
  (\x => f.mapHom x x (c.id x))
  (\x, y, m => let 0 mh = presComp f x x y (c.id x) m
                   0 idr := c.idLeft _ _ m
                   0 idl := c.idRight _ _ m
                   0 mh2 := presComp f x y y m (c.id y)
                in Calc $
            |~ (fmap (c.id x)) |> fmap m
            ~~ fmap ((c.id x) |> m)     ..< mh
            ~~ fmap m                   ... (cong fmap idr)
            ~~ fmap (m |> c.id y)       ..< (cong fmap idl)
            ~~ fmap m |> fmap (c .id y) ... mh2)
```

We also define an equality relation on natural transformation, this is useful
to perform higher-level equalities for example when defining the category of
functors and natural transformations. In particular, two natural
transformations are equal when their components are equal for all inputs.

```idris
public export
record NTEq
  {0 o1, o2 : Type}
  {0 c : Category o1} {0 d : Category o2}
  {f, g : Functor c d} (n1, n2 : f =>> g) where
  constructor MkNTEq
  0 sameComponent : (v : o1) -> n1.component v === n2.component v

public export
0 ntEqToEq :
  {0 o1, o2 : Type} ->
  {c : Category o1} -> {d : Category o2} ->
  {f, g : Functor c d} -> {n1, n2 : f =>> g} ->
  NTEq n1 n2 -> n1 === n2
ntEqToEq (MkNTEq sameComponent) {n1 = MkNT n1 p1} {n2 = MkNT n2 p2} =
  cong2Dep0
    {t3 = f =>> g}
    MkNT
    (funExtDep $ sameComponent)
    (funExtDep0 $ \x => funExtDep0 $ \y => funExtDep $ \z => UIP _ _)
```

One of the main feature of natural transformation is that they can be composed in two different ways.

Vertical composition of natural transformations stiches two natural transformation `n : f =>> g` and `m : g =>> h`
and produces a single natural transformation `(n !!> m) : f =>> h`. Here `f`, `g`, and `h`, are all functors `c -> d`.

```idris
public export
(!!>) :
  {0 cObj, dObj : Type} ->
  {0 c : Category cObj} -> {d : Category dObj} ->
  {f, g, h : Functor c d} ->
  f =>> g -> g =>> h -> f =>> h
(!!>) nat1 nat2 =
  MkNT (\x => nat1.component x |> nat2.component x) compProof
  where
    0 compProof : (0 x, y : cObj) -> (m : x ~> y) ->
      let 0 n1, n2 : f.mapObj x ~> h.mapObj y
          n1 = f.mapHom x y m |> (nat1.component y |> nat2.component y)
          n2 = (nat1.component x |> nat2.component x) |> h.mapHom x y m
      in n2 === n1
    compProof x y m =
      sym (d.compAssoc {}) `trans`
      glueSquares (nat1.commutes x y m) (nat2.commutes x y m)
```

The second way of composing natural transformation requires 2 pairs of functors
that can compose end to end. For example, given functors $F_1 : \cat{C_1}\to\cat{D_1}$ and $G_1 : \cat{D_1}\to\cat{E_1}$ as well
as $F_2 : \cat{C_2}\to\cat{D_2}$ and  $G_2 : \cat{D_2}\to\cat{E_2}$ (where $\cat{C_1}, \cat{C_2},\cat{D_1},\cat{D_2},\cat{E_1},\cat{E_2}$ are all categories), and
two natural transformations $n : F_1 \Rightarrow F_2$ and $m : G_1 \Rightarrow G_2$, then we can compose them to obtain
the natural transformation $n * m : G_1\circ F_1 \Rightarrow G_2\circ F_2$ where $\circ$ is functor composition.

<!-- idris
public export
-->
```idris
-- Operator for horizontal composition
(-*-) : {0 o1, o2, o3 : Type} ->
        {0 a : Category o1} -> {0 b : Category o2} -> {c : Category o3} ->
        {l, k : Functor a b} ->
        {g, h : Functor b c} ->
        k =>> l ->
        g =>> h ->
        k !*> g =>> l !*> h
(-*-) nt1 nt2  =
  MkNT (\v => nt2.component (k.mapObj v) |> h.mapHom (k.mapObj v) (l.mapObj v) (nt1.component v))
    (\x, y, m => let
        0 H : {0 a : o2} -> {0 b : o2} ->
              a ~> b -> (h.mapObj a) ~> (h.mapObj b)
        H = h.mapHom _ _
        0 L : {0 a : o1} -> {0 b : o1} ->
              a ~> b -> (l.mapObj a) ~> (l.mapObj b)
        L = l.mapHom _ _
        0 G : {0 a : o2} -> {0 b : o2} ->
              a ~> b -> (g.mapObj a) ~> (g.mapObj b)
        G = g.mapHom _ _
        0 K : {0 a : o1} -> {0 b : o1} ->
              a ~> b -> (k.mapObj a) ~> (k.mapObj b)
        K = k.mapHom _ _
        n2 : ?
        n2 = nt1.component
        n1 : ?
        n1 = nt2.component
        0 n1p : ?
        n1p = nt2.commutes
        0 n2p : ?
        n2p = nt1.commutes
      in Calc $
      |~ ((n1 (k.mapObj x) |> H (n2 x)) |> H (L m))
      ~~ (n1 (k.mapObj x) |> (H (n2 x) |> H (L m))) ..<(c.compAssoc _ _ _ _ (n1 (k.mapObj x)) (H (n2 x)) (H (L m)))
      ~~ (n1 (k.mapObj x) |> (H (n2 x |> L m)))     ..<(cong (n1 (k.mapObj x) |> ) (h.presComp _ _ _ (n2 x) (L m)))
      ~~ (G (n2 x |> L m) |> n1 (l.mapObj y))       ...(n1p (k.mapObj x) (l.mapObj y) (n2 x |> L m))
      ~~ (G (K m |> n2 y) |> n1 (l.mapObj y))       ...(cong (\xn => G xn |> n1 (l.mapObj y)) (n2p _ _ m))
      ~~ ((G (K m) |> G (n2 y)) |> n1 (l.mapObj y)) ...(cong (\x => x |> n1 (l.mapObj y)) (g.presComp _ _ _ (K m) (n2 y)))
      ~~ (G (K m) |> (G (n2 y) |> n1 (l.mapObj y))) ..<(c.compAssoc _ _ _ _ (G (K m)) (G (n2 y)) (n1 (l.mapObj y)))
      ~~ (G (K m) |> (n1 (k.mapObj y) |> H (n2 y))) ..<(cong (G (K m) |> ) (n1p _ _ (n2 y))))
```

Whiskering is the ability to precompose, or postcompose, the same functor to the source
and target of a natural transformation. Precomposing a functor `f` to a natural transformation `g =>> h`
is called "left whiskering" and results in a natural transformation `(f !*> g) =>> (f !*> h)`.

Postcomposing a functor `f` to a natural transformation `g =>> h` is called "right whiskering" and results in
a natural transformation `(g !*> f) =>> (h !*> f)`. Both whiskerings are obtained as a special case of horizontal
composition, namely, horizontal composition with the identity natural transformation.

<!-- idris
public export
-->
```idris
(*-) : {a : Category _} -> {b : Category _} -> {c : Category _} ->
       (f : Functor a b) -> {g, h : Functor b c} ->
       g =>> h -> (f !*> g) =>> (f !*> h)
(*-) f n = identity {f}  -*- n
```
<!-- idris
public export
-->
```idris
(-*) : {0 b : Category _} -> {c : Category _} -> {d : Category _} ->
       {g, h : Functor b c} ->
       g =>> h -> (f : Functor c d) -> (g !*> f) =>> (h !*> f)
(-*) n f = n -*- identity {f}
```

Natural transformations give us a notion of "mapping" for functors, this notion can be extended to be stronger
requiring it to work in both directions. Such definition would require that the component of a natural transformation to be a bijection


```idris
public export
record (=~=)
  {0 o1, o2 : Type}
  {0 c : Category o1} {0 d : Category o2}
  (0 f, g : Functor c d) where
  constructor MkNaturalIsomorphism
  nat : f =>> g
  φ : (v : o1) -> (~:>) d (g.mapObj v) (f.mapObj v)
  -- use let-records?
  0 η_φ : (v : o1) ->
    (nat.component v |> φ v) {a = (f.mapObj v), b = (g.mapObj v), c = (f.mapObj v)}
       === d.id (f.mapObj v)
  0 φ_η : (v : o1) ->
    (φ v |> nat.component v) {a = (g.mapObj v), b = (f.mapObj v), c = (g.mapObj v)}
       === d.id (g.mapObj v)
```

For practical reasons we sometimes need to extract the isomorphism out of the natural isomorphism.

```idris
public export
getIso : {0 o1, o2 : Type} -> {0 c : Category o1} -> {0 d : Category o2} ->
         {f, g : Functor c d} ->
         f =~= g -> (v : o1) -> Iso d (f.mapObj v) (g.mapObj v)
getIso eq v = MkIso (eq.nat.component v) (eq.φ v) (eq.η_φ v) (eq.φ_η  v)
```
