module Data.Category.Distrib

import Data.Category.NaturalTransformation
import Data.Category.Functor
import Data.Category.Monad

%hide Prelude.Functor
%hide Prelude.Ops.infixl.(*>)

record DistributiveLaw
    {0 o1 : Type} {0 c1 : Category o1}
    (t, s : Monad c1) where
    constructor MkDistrib
    nat : (t.endo !*> s.endo) =>> (s.endo !*> t.endo)

    -- t (s (s x)) ---> s (t (s x0)) ------> s (s (t x))
    --     |                                    |
    --     |                                    |
    --     |                                    |
    --     v                                    v
    --  t (s x) ---------------------------> s (t x)
    0 sq1 : (x : o1) ->
            let 0 tμS : t.endo.mapObj ((s.endo !*> s.endo).mapObj x) ~> t.endo.mapObj (s.endo.mapObj x)
                tμS = t.endo.mapHom ((s.endo !*> s.endo).mapObj x)
                                (s.endo.mapObj x)
                                (s.mult.component x)
                0 tμS' : (t.endo !*> (s.endo !*> s.endo)).mapObj x ~> (t.endo !*> s.endo).mapObj x
                tμS' = ?arg tμS
                0 ll : (t.endo !*> s.endo).mapObj x ~> (s.endo !*> t.endo).mapObj x
                ll = nat.component x
            in Type

