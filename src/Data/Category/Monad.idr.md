## Monads

Like every category-theory construct, there are many ways of
defining a monad. You might have heard the following:

> A Monad is a monoid in the category of endofunctors

While a perfectly accurate definition, it does not help understand
what a monad is, if you are unfamiliar with monoids, categories
and endofunctors.

To define monads, we need to know what a [category](src/Data/Category.idr.md)
is, we need [functors](src/Data/Category/Functor.idr.md), and we need
[natural transformations](src/Data/Category/NaturalTransformation.idr.md).

<!-- idris
module Data.Category.Monad

import Data.Category
import Data.Category.NaturalTransformation
import Data.Category.Bifunctor
import Data.Category.Endofunctor
import Data.Category.Monoid
import Control.Relation

import Syntax.PreorderReasoning

%hide Prelude.Ops.infixl.(*>)
%hide Prelude.Monad
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)
-->

Being a monad is a property of _functors_, in particular, of functor with the same
source and target category (same domain and codomain). We call those
_endo-functors_.

A programming example of a functor is `List : Type -> Type`, a functor
in the category of types and functions. Mathematically, we write
$\text{List} : \mathbb{Ty} \to \mathbb{Ty}$ to say that the functor `List`
carries objects and morphisms from the category $\mathbb{Ty}$ to the category
$\mathbb{Ty}$. Because the source and target are the same, we call it
an endofunctor.

For an endofunctor to be a monad we need to provide two natural transformations.
One from the identity functor to our endofunctor, and one from the successive
application of the functor with itself, to the functor without any application.

Programmatically speaking, it means we have `unit : a -> List a` and
`mult : List (List a) -> List a`. We often call those properties the "unit"
and the "multiplication" of the monad.

More abstractly if we are given a functor $m : \mathcal{C} \to \mathcal{C}$ we need to provide the natural transformations:
- $unit : Id \Rightarrow m$ where $Id : \mathcal{C} \to\mathcal{C}$ is the identity functor on $\mathcal{C}$
- $mult : m \bullet m \Rightarrow m$ where $\bullet: (\mathcal{C} \to \mathcal{D})\to(\mathcal{C}\to\mathcal{C})\to(\mathcal{C}\to\mathcal{C})$ is the composition of functors (here specialised to endofunctors on $\mathcal{C}$)

We defined all those constructions in the imported modules. `idF : (c : Category o) -> Functor c c` builds the identity functor for the category `c`, `(*>) : Functor a b -> Functor b c -> Functor a c` composes functors in the forward direction, and `=>> : (f, g : Functor a b) -> Type` builds the natural transformation between functors `f` and `g`. We put all those pieces together to defined monads:

```idris
public export
record Monad (c : Category o) where
  constructor MkMonad
  endo : Functor c c
  unit : idF c =>> endo
  mult : endo !*> endo =>> endo

public export
record MonadProofs {o : Type} {c : Category o} (m : Monad c) where
  constructor MkMonadProofs
  idL : let
         0 ηT : m.endo =>> (m.endo !*> m.endo)
         ηT = funcIdRNT' !!> (m.unit -* m.endo)
         in (ηT !!> m.mult) `NTEq` identity {f = m.endo}
  idR : let
         0 Tη : m.endo =>> m.endo !*> m.endo
         Tη = funcIdLNT' !!> (m.endo *- m.unit)
         in (Tη !!> m.mult) `NTEq` identity {f = m.endo}
  comp : let
         0 whiskL : m.endo !*> (m.endo !*> m.endo) =>> m.endo !*> m.endo
         whiskL = (m.endo *- m.mult) {a = c, b = c, c = c, g = m.endo !*> m.endo, h = m.endo}
         0 whiskR : m.endo !*> (m.endo !*> m.endo) =>> m.endo !*> m.endo
         whiskR = funcCompAssocNT' _ _ _ !!> (m.mult -* m.endo)
         in whiskL `NTEq` whiskR

public export
record Monad' (c : Category o) where
  constructor MkMonad'
  endo : Functor c c
  unit : idF c =>> endo
  mult : (endo !*> endo) =>> endo
```

`unit` and `mult` are called `pure` and `join` in Idris, I've picked different names, the ones inherited from the monoidal nature of monads, to avoid confusion
with the functions `pure : a -> m a` and `join : m (m a) -> m a` in Idris. I will explain later how they are related.

Not only we need to define $unit$ and $mult$ but we also need to ensure they respect some properties, the first one is that
$mult$ can be applied in any order given a term $f(f(f(x)))$. Either it can be applied twice in succession, or it can be applied
first as the map on morphism from the functor $F$ such that $F(mult) : f(f(f(x))) \to f(f(x))$, and then applied directly on
the result. This property is similar to the associativity property of monoids.

```tikz
\usepackage{tikz-cd}
\usepackage{amsfonts}
\begin{document}
\begin{tikzcd}
{f(f(f(x)))} && {f(f(x))}
\\
\\ {f(f(x))} && {f(x)}
\arrow["{F(mult_x)}", from=1-1, to=1-3]
\arrow["{mult_{f(x)}}"', from=1-1, to=3-1]
\arrow["{mult_x}", from=1-3, to=3-3]
\arrow["{mult_x}"', from=3-1, to=3-3]
\end{tikzcd}
\end{document}
```

```idris
  0 square : (x : o) -> let
      top : endo.mapObj (endo.mapObj (endo.mapObj x)) ~> endo.mapObj (endo.mapObj x)
      top = endo.mapHom _ _ (mult.component x)
      bot, right : endo.mapObj (endo.mapObj x) ~> endo.mapObj x
      right = mult.component x
      bot = mult.component x
      left : endo.mapObj (endo.mapObj (endo.mapObj x)) ~> endo.mapObj (endo.mapObj x)
      left = mult.component (endo.mapObj x)
      0 arm2, arm1 : endo.mapObj (endo.mapObj (endo.mapObj x)) ~> endo.mapObj x
      arm1 = (top |> right) {cat = c}
      arm2 = left |> bot
      in arm1 === arm2
```

Next, we ensure that $unit$ and $mult$ interact appropriately with each other,
so that from $f(x)$ we can apply $unit$ on either the whole expression $f(x)$
and obtain $f(f(x))$, or apply it to the inner $x$ using the map on morphism
from the functor $F$ to obtain $f(f(x))$. Either way, applying $mult$ should
result in the same $f(x)$ in the end. We visualise this property with the
following commutative diagram.

  ```tikz
\usepackage{tikz-cd}
\usepackage{amsfonts}
\begin{document}
\begin{tikzcd}
{f(x)} && {f(f(x))} \\
\\
{f(f(x))} && {f(x)}
\arrow["unit_{f(x)}", from=1-1, to=1-3]
\arrow["{F(unit_x)}"', from=1-1, to=3-1]
\arrow[from=1-1, to=3-3, equals]
\arrow["mult", from=1-3, to=3-3]
\arrow["mult"', from=3-1, to=3-3]
\end{tikzcd}
\end{document}
```
```idris
  0 identityLeft : (x : o) -> let
      0 compose : endo.mapObj x ~> endo.mapObj x
      compose = (unit.component (endo.mapObj x) |> mult.component x) {cat = c}
      0 straight : endo.mapObj x ~> endo.mapObj x
      straight = c.id (endo.mapObj x)
      in compose === straight

  0 identityRight : (x : o) -> let
      0 compose : endo.mapObj x ~> endo.mapObj x
      compose = (endo.mapHom _ _ (unit.component x) |> mult.component x) {cat = c}
      0 straight : endo.mapObj x ~> endo.mapObj x
      straight = c.id (endo.mapObj x)
      in compose === straight

```

### Monad from Products

Given an object $i ∈ C$ and a product $(m : C → C → C)$ in a category C,
we can build a monad by attaching $i$ to each object using the product.

This gives rise to a monad with the morphism $pure : ∀ x ∈ C. x → m_x i$ and
$join : ∀ x ∈ C. → m (m_x(i)) i) \to m x i$

```idris
MonadFromProduct : (c : Category o) -> (mon : Monoidal c) -> MonoidObject {cat = c} mon -> Monad c
MonadFromProduct c mon obj = MkMonad
  (MkFunctor (\x => mon.mult.mapObj (obj.obj && x))
      (\x, y, m => mon.mult.mapHom _ _ (c.id obj.obj && m))
      ?cid
      ?aaa
  )
  (MkNT
      (\vx => ?comp)
      ?bbb
  )
  ?ccc
```

We can build an example of this using the category of types and functions, using the
coproduct on functions as the monoidal product and the `Unit` type as the object that
we will pair with every other object.
This gives rise to the `Maybe` monad as represented by the `Maybe x = 1 + x` operation on types.


