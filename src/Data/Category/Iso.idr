module Data.Category.Iso

import Data.Category

-- Isomorphisms for morphisms in a category
public export
record Iso {o : Type} (cat : Category o) (a, b : o) where
  constructor MkIso
  to : a ~> b
  from : b ~> a
  0 commutesA : (to |> from) {a, b, c=a} = cat.id a
  0 commutesB : (from |> to) {a=b, b=a, c=b} = cat.id b

public export
0 (~~~) : (c : Category o) => (a, b : o) -> Type
(~~~) = Iso c
