<!-- idris
module Data.Category.FunctorCat

import Data.Category
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.NaturalTransformation

import Syntax.PreorderReasoning

%hide Prelude.Ops.infixl.(|>)
-->

### The category of functors and natural transformations

Because vertical composition has the right shape `f =>> g -> g =>> h -> f =>> h`, we
can use it to define the category of functors as objects and natural transformations as morphisms
using vertical composition to stitch natural transformations together.
The objects of this category are functors, it is parameterised by two categories `c` and `d` that will
remain fixed such that every object is an instance of a functor from `c` to `d`. This pinning of the endpoints
of functors is necessary to ensure vertical composition remains valid.

To write down this category we first need to ensure vertical composition has the identity natural
transformation as left and right neutral, and that it is associative. The next three lemmas take
care of this.

```idris
parameters
  {0 o1, o2 : Type}
  {0 c : Category o1} {0 d : Category o2}
  {f, g : Functor c d}

  0
  (|>>) : {x, y, z : o2} -> x ~> y -> y ~> z -> x ~> z
  (|>>) = (|:>) d
  private infixr 2 |>>

  public export
  ntIdentityRight :
    (n : f =>> g) -> NTEq (n !!> identity {f = g}) n
  ntIdentityRight nt = MkNTEq $ \vx => Calc $
      |~ nt.component vx |>> g.mapHom vx vx (c.id vx)
      ~~ nt.component vx |>> d.id (g.mapObj vx)
         ...(cong (nt.component vx |>>) (g.presId vx))
      ~~ nt.component vx
         ...((d.idRight _ _ _))

  public export
  0 ntIdentityLeft :
    (n : f =>> g) -> NTEq (NaturalTransformation.identity !!> n) n
  ntIdentityLeft nt = MkNTEq $ \v =>
      Calc $ |~ f.mapHom v v (c.id v) |>> nt.component v
             ~~ d.id (f.mapObj v)     |>> nt.component v
                ...(cong (|>> nt.component v) (f.presId v))
             ~~ nt.component v
                ...(d.idLeft _ _ (nt.component v))

  public export
  0 ntAssoc :
      {h, i : Functor c d} ->
      (n1 : f =>> g) -> (n2 : g =>> h) -> (n3 : h =>> i) ->
      NTEq (n1 !!> (n2 !!> n3)) ((n1 !!> n2) !!> n3)
  ntAssoc (MkNT n1 p1) (MkNT n2 p2) (MkNT n3 p3) = MkNTEq $ \v =>
      (d.compAssoc _ _ _ _ (n1 v) (n2 v) (n3 v))

```

Using those lemma we can build the functor category parameterised over two
categories $\cat{C}$ and $\cat{D}$. The objects are functors $\cat{C} \to\cat{D}$
and morphisms are natural transformation.

```idris
parameters (c1 : Category a) (c2 : Category b)
  public export
  FunctorCat : Category (Functor c1 c2)
  FunctorCat = MkCategory
    (=>>)
    (\f => identity {f})
    (!!>)
    (\f, g, n => ntEqToEq $ ntIdentityRight n)
    (\f, g, n => ntEqToEq $ ntIdentityLeft n)
    (\f, g, h, i, nt1, nt2, nt3 =>
        ntEqToEq $ ntAssoc nt1 nt2 nt3
    )
```

<!--
export
-->

An interesting fact about functor categories is that we can use them to define
currying and uncurrying operation on them.

That is, from a functor $\cat{C} \to[\cat{D}, \cat{E}]$ we can obtain
the functor $\cat{C} * \cat{D} \to\cat{E}$. The proof is quite large so it is
left in appendix. *add link here*

```idris
curryFunctor : {0 o : Type} -> {c : Category o} ->
               Functor a (FunctorCat b c) -> Functor (a * b) c
```
<!-- idris
curryFunctor (MkFunctor mo mm pid pcomp) =
  let (|>>) : {x, y, z : o} -> x ~> y -> y ~> z -> x ~> z
      (|>>) = (|:>) c
  in MkFunctor
    (\(x && y) => (mo x).mapObj y)
    (\(x && x'), (y && y'), (m && m') =>
        (mo x).mapHom x' y' m' |>> (mm x y m).component y')
    (\(x && y) => let
        ff = (mo x).presId y
        fg = (mo x).presComp y y y
        0 nt = (mm x x (a.id x)).commutes y y (b.id y)
        cc : {v1, v2, v3 : o} -> v1 ~> v2 -> v2 ~> v3 -> v1 ~> v3
        cc = (|:>) c
        in Calc $
        |~ (((mo x).mapHom y y (b.id y))
            |>>
            ((mm x x (a.id x)).component y)
           )
        ~~ (((mm x x (a .id x)) .component y)
             |>>
            ((mo x) .mapHom y y (b .id y))
           ) ..<(nt)
        ~~ (c.id (mapObj (mo x) y) |>> (mo x).mapHom y y (b.id y))
            ...(cong
                   (\vx => cc vx ((mo x).mapHom y y (b.id y)))
                   (let vs = (mo x).presId y
                      ; pp = pid x
                    in (cong (\vx => vx.component y) pp) `trans` vs)
               )
        ~~ (mo x).mapHom y y (b.id y) ...(c.idLeft _ _ ((mo x).mapHom y y (b.id y)))
        ~~ c.id ((mo x).mapObj y) ...(ff)
    )
    (\(x1 && x2), (y1 && y2), (z1 && z2), (m1 && m2), (n1 && n2) =>
        Calc $
        |~ (((mo x1) .mapHom x2 z2 ((|:>) b m2 n2)) |>> ((mm x1 z1 ((|:>) a m1 n1)) .component z2))
        ~~ ((((mo x1).mapHom x2 y2 m2) |>> ((mo x1).mapHom y2 z2 n2)) |>>
            (((mm x1 y1 m1).component z2) |>> ((mm y1 z1 n1).component z2)))
          ...(cong2 (|>>)
                ((mo x1).presComp _ _ _ m2 n2)
                (cong (\vx => vx.component z2) (pcomp _ _ _ m1 n1))
             )
        ~~ (((mo x1).mapHom x2 y2 m2) |>> (((mo x1).mapHom y2 z2 n2) |>>
            (((mm x1 y1 m1) .component z2) |>> ((mm y1 z1 n1).component z2))))
            ..<(c.compAssoc _ _ _ _
                   ((mo x1).mapHom x2 y2 m2)
                   ((mo x1).mapHom y2 z2 n2)
                   (((mm x1 y1 m1) .component z2) |>> ((mm y1 z1 n1) .component z2)))
        ~~ (((mo x1).mapHom x2 y2 m2) |>>
            ((((mo x1).mapHom y2 z2 n2) |>> ((mm x1 y1 m1) .component z2)) |>>
            ((mm y1 z1 n1) .component z2)))
            ...(cong (((mo x1).mapHom x2 y2 m2) |>>) (c.compAssoc {}))
        ~~ (((mo x1).mapHom x2 y2 m2) |>>
            ((((mm x1 y1 m1).component y2) |>> ((mo y1).mapHom y2 z2 n2)) |>>
            ((mm y1 z1 n1) .component z2)))
            ..<(cong
                 (\vx => ((mo x1).mapHom x2 y2 m2) |>> (vx |>> ((mm y1 z1 n1) .component z2)))
                 ((mm x1 y1 m1).commutes y2 z2 n2))
        ~~ (((mo x1).mapHom x2 y2 m2) |>> (((mm x1 y1 m1).component y2) |>>
            (((mo y1).mapHom y2 z2 n2) |>> ((mm y1 z1 n1).component z2))))
            ..<(cong ((mo x1).mapHom x2 y2 m2 |>>) (c.compAssoc {}))
        ~~ ((((mo x1).mapHom x2 y2 m2) |>> ((mm x1 y1 m1).component y2)) |>>
            (((mo y1).mapHom y2 z2 n2) |>> ((mm y1 z1 n1).component z2)))
           ...(c.compAssoc {})
    )

export
-->
Similarly, we can uncurry bifunctors $\cat{C} * \cat{D} \to \cat{E}$ as
a functor $\cat{C} \to [\cat{D}, \cat{E}]$. Again, the implementation is quite large so it is left in appendix.

```idris
uncurryFunctor :
    {0 o, p, q : Type} ->
    {a : Category p} -> {b : Category q} -> {c : Category o} ->
    Functor (a * b) c -> Functor a (FunctorCat b c)
```

<!--
uncurryFunctor (MkFunctor mo mm pid .component) = MkFunctor
    (\vx => MkFunctor
        (\vy => mo (vx && vy))
        (\vy, vz, m =>
            mm (vx && vy) (vx && vz) (a.id vx && m))
        (\vy => Calc $
             |~ mm (vx && vy) (vx && vy) (a.id vx && b.id vy)
             ~~ c.id (mo (vx && vy))
             ...(pid (vx && vy))
        )
        (\vy, vz, vw, f, g => let
            gn = .component (vx && vy) (vx && vz) (vx && vw)
                   (a.id vx && f) (a.id vx && g)
            in Calc $
            |~ mm (vx && vy) (vx && vw) (a.id vx && (|:>) b f g)
            ~~ mm (vx && vy) (vx && vw)
                ((|:>) a (a.id vx) (a.id vx) && (|:>) b f g)
            ..<(cong (mm (vx && vy) (vx && vw)) $
                cong2 (&&) (a.idRight _ _ _) Refl)
            ~~ (|:>) c
                  (mm (vx && vy) (vx && vz) (a.id vx && f))
                  (mm (vx && vz) (vx && vw) (a.id vx && g))
            ...(gn)
        )
    )
    (\vx, vy, m => MkNT
        (\vz => mm (vx && vz) (vy && vz) (m && b.id vz))
        (\vz, vw, mx => let
               pq = .component (vx && vz) (vx && vw) (vy && vw)
                          (a.id vx && mx) (m && b.id vw)
               pp = .component (vx && vz) (vy && vz) (vy && vw)
                          (m && b.id vz) (a .id vy && mx)
            in Calc $
            |~ (|:>) c
               (mm (vx && vz) (vy && vz) (m && b.id vz))
               (mm (vy && vz) (vy && vw) (a .id vy && mx))

            ~~ mm (vx && vz) (vy && vw)
                  ((|:>) a m (a.id vy) && (|:>) b (b.id vz) mx)
               ..<(pp)
            ~~ mm (vx && vz) (vy && vw)
                  (m && mx)
               ...(cong (mm (vx && vz) (vy && vw)) $
                   cong2 (&&) (a.idRight _ _ m) (b.idLeft _ _ mx))
            ~~ mm (vx && vz) (vy && vw)
                  ((|:>) a (a.id vx) m && (|:>) b mx (b.id vw))
               ..<(cong (mm (vx && vz) (vy && vw)) $
                   cong2 (&&)
                       (a.idLeft _ _ m)
                       (b.idRight _ _ mx))
            ~~ (|:>) c
               (mm (vx && vz) (vx && vw) (a.id vx && mx))
               (mm (vx && vw) (vy && vw) (m && b.id vw))
               ...(pq)
        )
    )
    (\vx => ntEqToEq $ MkNTEq $ \vy => Refl)
    (\vx, vy, vz, f, g => ntEqToEq $ MkNTEq $ \vw => let
          qq = .component (vx && vw) (vy && vw) (vz && vw) (f && b.id vw) (g && b.id vw)
          in Calc $
          |~ (mm (vx && vw) (vz && vw) ((|:>) a f g && b.id vw))
          ~~ (mm (vx && vw) (vz && vw) ((|:>) a f g && (|:>) b (b.id vw) (b.id vw)))
          ..<(cong (mm (vx && vw) (vz && vw)) $
              cong2 (&&) Refl (b.idLeft _ _ _))
          ~~ (|:>) c
              (mm (vx && vw) (vy && vw) (f && b.id vw))
              (mm (vy && vw) (vz && vw) (g && b.id vw))
          ...(qq)
    )
-->
