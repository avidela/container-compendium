
```idris
module Data.Category.Action

import Data.Product
import Data.Category
import Data.Category.Product
import Data.Category.Monoid
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.NaturalTransformation
import Syntax.PreorderReasoning
%hide Prelude.(&&)
%hide Prelude.Ops.infixl.(*>)

%unbound_implicits off
```

## Categorical action

The term _action_ comes from mathematic, I have first been exposed to it through
_group actions_ which have the form $act : C \times D \to D$ where $D$ is a group and $C$ is a
set. We say that $C$ is _acting_ on $D$ of that $act$ is an _action_ of $C$ on $D$.

We can write this property programmatically, the same way monoids are implemented
in haskell for example
```
interface Monoid c => Action c d where
  act : c -> d -> d
```

An action require a monoid structure on the set performing the "action", this
monoid interacts with the `act` function in a way that preserves the following
properties:

- `∀ x : d. act unit x = x`
- `∀ x y : c. ∀ z : d .  act (x * y) z = act x (act y z)`

Where the monoid operations are given by `(* : c -> c -> c, unit : c)`.

This definition only works for types and functions, but we can generalise it
for containers and container morphisms. In fact, we can generalise it for any
category $D$ with a monoidal category $C$ using a bifunctor $Act : C \otimes D \to D$ and
natural isomorphisms given by the diagrams:


```idris
public export
record Action {0 o1, o2 : Type} (c : Category o1) (d : Category o2) where
  constructor MkAction
  mon : Monoidal c
  action : Bifunctor c d d
  m : let f1 : Functor (c * (c * d)) d
          f1 = pair (idF c) action !*> action
          f2 : Functor (c * (c * d)) d
          f2 = assocR !*> pair mon.mult (idF d) !*> action
       in f1 =~= f2
  u : idF d =~= Bifunctor.applyBifunctor {a=c} (mon.I.mapObj ()) action

```

In this definition, I use natural isomorphisms, but we can also define a notion of _lax action_
that makes use of a natural transformation, ensuring the conversion only goes one way.

```idris
public export
record LaxAction {0 o1, o2 : Type} (c : Category o1) (d : Category o2) where
  constructor MkLaxAction
  mon : Monoidal c
  action : Bifunctor c d d
  m : let 0 f1, f2 : Functor (c * (c * d)) d
          f1 = pair (idF c) action !*> action
          f2 = assocR !*> pair mon.mult (idF d) !*> action
       in f1 =>> f2
  u : idF d =>> Bifunctor.applyBifunctor {a=c} (mon.I.mapObj ()) action
```

It is this definition of action that we are going to use for Containers.

Finally, the following helper function extract the action morphism from the `Action` record.

```idris
public export
getMor : {0 o1, o2 : Type} ->
         {c : Category o1} ->
		 {d : Category o2} ->
		 (act : Action c d) ->
		 (x : o1) -> (y : o1) -> (z : o2) ->
		 (~:>) d (act.action.mapObj (x && (act.action.mapObj (y && z))))
               (act.action.mapObj (act.mon.mult.mapObj (x && y) && z))
getMor (MkAction
         (MkMonoidal (MkFunctor m m' _ _) i _ _ _)
         (MkFunctor f f' _ _)
         n _)
       x y z = n.nat.component (x && (y && z))
```
