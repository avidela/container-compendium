module Data.Category.Ops

export infix 0 ≡     -- equality
export infixr 1 ~>   -- Morphisms
export infixl 1 <~   -- backward Morphisms
export infix 1 <!   -- Container morphism constructor
export infixl 5 |>   -- forward Composition
export infixl 5 ⨾    -- forward Composition
export infixr 5 •    -- reverse Composition
export infixr 7 !*> -- Functor composition
export infixr 7 !!> -- natural transformation composition
export infixr 5 #>   -- Action
export infix 3 <>
export infixr 7 ><    -- Product
export infix 5 :-

export infixr 4 >>-
export infixr 4 -<<

export infix 1 =~= -- Isomorphisms

export infixr 7 -*> -- Functor
export infixr 4 =>> -- Natural Transformations

-- container things
export infixl 6 ○
export infixr 10 • -- application
export infixl 6 ~○~
export infixl 6 ~○#~
export infixl 7 ⊗
export infixl 7 ~⊗~
export infixl 7 ~*~
export infixl 7 ~#>~
export infixl 7 ~▷~
export infixl 6 ×
export typebind infixr 0 !>
-- export typebind infixr 0 ▷
export infixr 0 ◁
export infixl 9 -*- -- horizontal composition
export infixl 6 ~+~
export infixl 1 -.-
export infixl 1 -*
export infixl 1 *-
-- cartesian containers
export infixr 0 =#> -- morphisms
export infixl 1 |#> -- composition

export infixr 1 =%> -- morphisms
export infixl 1 |%> -- composition

