module Data.Category.Product

import Data.Category
import Data.Category.Functor
import Data.Category.Iso
import Data.Category.Notation

import public Data.Product
import Syntax.PreorderReasoning

import Proofs


%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

-- reads as ×
private infixl 5 >:<

public export
record HasProduct {0 o : Type} (cat : Category o) where
  constructor MkProd

  (>:<) : o -> o -> o
  pi1 : {0 a, b : o} -> a >:< b ~> a
  pi2 : {0 a, b : o} -> a >:< b ~> b

  --              c
  --            ⟋ │ ⟍
  --       f  ⟋   │   ⟍  g
  --        ⟋     │!    ⟍
  --      ⟋       │       ⟍
  --    ↙︎         ↓         ↘︎
  --  a ←────── a × b ──────→ b
  --       π₁            π₂
  --
  prod : {0 a, b, c: o} ->
         c ~> a ->
         c ~> b ->
         c ~> (a >:< b)

  --              c
  --            ⟋ │
  --       f  ⟋   │
  --        ⟋     │!
  --      ⟋       │
  --    ↙︎         ↓
  --  a ←────── a × b
  --       π₁
  --
  -- composing the unique morphism from c to a × b with pi1
  -- is the same as going through f
  prodLeft : {0 a, b, c : o} -> (f : c ~> a) -> (g : c ~> b) ->
             (|>) {a=c} {b=(a >:< b)} {c=a}
                  (prod {a} {b} {c} f g)
                  (pi1 {a} {b})
                = f

  --    c
  --    │ ⟍
  --    │   ⟍  g
  --    │!    ⟍
  --    │       ⟍
  --    ↓         ↘︎
  --  a × b ──────→ b
  --         π₂
  -- composing the unique morphism from c to a × b with π₂
  -- is the same as going through
  prodRight : {0 a, b, c : o} -> (f : c ~> a) -> (g : c ~> b) ->
              (|>) {a=c} {b=(a >:< b)} {c=b}
                   (prod {a} {b} {c} f g)
                   (pi2 {a} {b})
                 = g

  0 uniq : {0 a, b, c : o} ->
         {f1 : c ~> a} ->
         {f2 : c ~> b} ->
         {p : c ~> a >:< b} ->
         Start (c -< p >- (a >:< b)
                  -< pi1 {a} {b}>- End a) === f1 ->
         Start (c -< p >- (a >:< b)
                  -< pi2 {a} {b} >- End b) === f2 ->
         prod {a} {b} {c} f1 f2 = p

public export
(><) : (cat : Category o) => (prod : HasProduct cat) => o -> o -> o
(><) = (>:<) prod

public export
swap' : (cat : Category o) => (prod : HasProduct cat) => (a, b : o) -> a >< b ~> b >< a
swap' a b = prod.prod (prod.pi2) (prod.pi1)

public export
swap : (cat : Category o) => (prod : HasProduct cat) => {a, b : o} -> a >< b ~> b >< a
swap = prod.prod (prod.pi2) (prod.pi1)

public export
(-.-) : (cat : Category o) => (prod : HasProduct cat) =>
        {a, b, c, d : o} ->
        a ~> b -> c ~> d -> a >< c ~> b >< d
(-.-) m1 m2 = prod.prod (prod.pi1 {a=a, b=c} |> m1) (prod.pi2 {a=a, b=c} |> m2)

public export
0 (-*-) : (cat : Category o) => (prod : HasProduct cat) =>
        {a, b, c : o} ->
        a ~> b -> a ~> c -> a ~> b >< c
(-*-) = prod.prod

public export 0
Prod : (cat : Category o) => (prod : HasProduct cat) =>
       (a, b, c : o) ->
       a ~> b -> a ~> c -> a ~> b >< c
Prod _ _ _ = prod.prod


public export
mapSnd : (cat : Category o) -> (prod : HasProduct cat) -> (left, source, target : o) -> (f : source ~> target) ->
         left >< source ~> left >< target
mapSnd (MkCategory m id comp idl idr ass) (MkProd pair p1 p2 mkp x y uniq) a b c f =
  mkp p1 (comp p2 f)

public export
mapFst : (cat : Category o) -> (prod : HasProduct cat) -> (source, right, target : o) -> (f : source ~> target) ->
         source >< right ~> target >< right
mapFst (MkCategory m id comp idl idr ass) (MkProd pair p1 p2 mkp x y uniq) a b c f =
  mkp (comp p1 f) p2

public export
associative : (cat : Category o) -> (prod : HasProduct cat) ->
              (a, b, c : o) -> a >< (b >< c) ~> (a >< b) >< c
associative (MkCategory m id comp idl idr ass) (MkProd pair p1 p2 mkp x y uniq) a b c =
  mkp (mkp (p1 {a} {b = pair b c}) (comp (p2 {a} {b = pair b c}) (p1 {a=b, b=c})))
      (comp (p2 {a} {b=(pair b c)}) (p2 {a=b, b=c}))

public export
associative2 : (cat : Category o) -> (prod : HasProduct cat) ->
               (a, b, c : o) -> (a >< b) >< c ~> a >< (b >< c)
associative2 (MkCategory m id comp idl idr ass) (MkProd pair p1 p2 mkp x y uniq) a b c =
  mkp (p1 `comp` p1)
      (mapFst (MkCategory m id comp idl idr ass) (MkProd pair p1 p2 mkp x y uniq) (pair a b) c b
        (p2 {a} {b}))

-- TODO : proof that associative ○ associative2 = id

-- comp
--   (mkp (mkp (p1 a (pair b c)) (comp (p2 a (pair b c)) (p1 b c))) (comp (p2 a (pair b c)) (p2 b c)))
--   (mkp (comp (p1 (pair a b) c) (p1 a b)) (mkp (comp (p1 (pair a b) c) (p2 a b)) (p2 (pair a b) c)))
-- = mkp ?fff ?ggg

0 assocCommute1 :
  (cat : Category o) => (prod : HasProduct cat) =>
  (associative cat prod a b c |> associative2 cat prod a b c)
    {a = a >< (b >< c), b = (a >< b) >< c, c = a >< (b >< c)} =
  cat.id (a >< (b >< c))
assocCommute1 {cat = MkCategory m id comp idl idr ass}
              {prod = MkProd pair p1 p2 mkp x y uniq}
  = let u = uniq {a} {b} {c = a `pair` b} {f1 = ?fff} {f2 = ?ggg}
    in ?ending

associativeIso : (cat : Category o) => (prod : HasProduct cat) =>
                 {a, b, c : o} ->
                 Iso cat (a >< (b >< c)) ((a >< b) >< c)
associativeIso = MkIso
  (associative cat prod a b c)
  (associative2 cat prod a b c)
  assocCommute1
  ?c2

--  d1:
--    a
--    │ ⟍
--    │   ⟍  h
--   k│     ⟍
--    │       ⟍
--    ↓         ↘︎
--    b ───────→  c
--         f
--
--  d2:
--    a
--    │ ⟍
--    │   ⟍  l
--   k│     ⟍
--    │       ⟍
--    ↓         ↘︎
--    b ───────→  d
--         g
--  result:
--    a
--    │ ⟍
--    │   ⟍  h -*- l
--   k│     ⟍
--    │       ⟍
--    ↓         ↘︎
--    b ───────→  c >< d
--       f -*- g
-- export 0
-- natPairing : (cat : Category o) => (prod : HasProduct cat) =>
--              {a, b, c : o} ->
--              {k : a ~> b} ->
--              {f : b ~> c} ->
--              {g : b ~> d} ->
--              {h : a ~> c} ->
--              {l : a ~> d} ->
--              (d1 : Compose a b c k f = h) ->
--              (d2 : Compose a b d k g = l) ->
--              Compose a b (c >< d) k (Prod b c d f g) = (Prod a c d h l)
-- natPairing Refl Refl =
--   let p = congMor2 k (prod.prodLeft f g)
--       q = congMor2 k (prod.prodRight f g)
--    in sym $ prod.uniq (trans (sym (cat.compAssoc _ _ _)) p)
--                       (trans (sym (cat.compAssoc _ _ _)) q)

-- Given two categories we can build the product category which objects
-- are pairs of objects from each category and the morphisms are pairs
-- of morphisms.
public export
ProductCat : Category o -> Category p -> Category (o * p)
ProductCat x y = MkCategory
  (\a, b => ((~:>) x a.π1 b.π1) * ((~:>) y a.π2 b.π2))
  (\v => x.id v.π1 && y.id v.π2)
  (\f, g => (|:>) x f.π1 g.π1 && (|:>) y f.π2 g.π2)
  (\_, _, idl => cong2 (&&) (x.idRight _ _ idl.π1) (y.idRight _ _ idl.π2) `trans` Data.Product.projIdentity idl)
  (\_, _, idr => cong2 (&&) (x.idLeft  _ _ idr.π1) (y.idLeft  _ _ idr.π2) `trans` Data.Product.projIdentity idr)
  (\_, _, _, _, f, g, h =>
      let c1 = compAssoc x _ _ _ _ f.π1 g.π1 h.π1
          c2 = compAssoc y _ _ _ _ f.π2 g.π2 h.π2
      in  cong2 (&&) c1 c2)

public export
(*) : Category o -> Category p -> Category (o * p)
(*) = ProductCat


-- If our category has products then there is a functor `C × C -> C`
public export
ProductFunctor : {c : Category o} -> HasProduct c -> Functor (ProductCat c c) c
ProductFunctor (MkProd pair pi1 pi2 prod prodLeft prodRight uniq) = MkFunctor
  (uncurry pair)
  (\_, _, (m1 && m2) => prod ((|:>) c pi1 m1)
                             ((|:>) c pi2 m2))
  (\(v1 && v2) => uniq (c.idLeft _ _ pi1 `trans` sym (c.idRight _ _ pi1))
                       (c.idLeft _ _ _ `trans` sym (c.idRight _ _ _)))
  (\_, _, _, (f1 && f2), (g1 && g2) => uniq
             -- we start with
             -- ((prod (π1 ; f1) (π2 ; f2)) ; (prod (π1 ; g1) (π2 ; g2))) ; π1
     (Calc $ |~ ((prod (pi1 |> f1) (pi2 |> f2)) |> (prod (pi1 |> g1) (pi2 |> g2))) |> pi1
             -- we re-associate this expression to put the final π1 next to a `prod`
             -- prod (π1 ; f1) (π2 ; f2) ; (prod (π1 ; g1) (π2 ; g2) ; π1)
             ~~ (prod (pi1 |> f1) (pi2 |> f2)) |> ((prod (pi1 |> g1) (pi2 |> g2)) |> pi1)
                ... (sym (compAssoc c _ _ _ _ _ _ _))
             -- then we use the product axiom on the second part to get
             -- prod (π1 ; f1) (π2 ; f2) ; (π1 ; g1)
             ~~ ((prod (pi1 |> f1) (pi2 |> f2)) |> (pi1 |> g1))
                ... (cong ((prod (pi1 |> f1) (pi2 |> f2)) |>) (prodLeft _ _))
             -- then we re-associate again
             -- (prod (π1 ; f1) (π2 ; f2) ; π1) ; g1
             ~~ (((prod (pi1 |> f1) (pi2 |> f2)) |> pi1) |> g1)
                ... (compAssoc c _ _ _ _ _ _ _)
             -- Then we use our product axiom on the left part to get
             -- (π1 ; f1) ; g1
             ~~ ((pi1 |> f1) |> g1)
                ... (cong (|> g1) (prodLeft _ _))
             -- finally we reassociate to obtain
             -- π1 ; (f1 ; g1)
             ~~ pi1 |> (f1 |> g1) ...(sym (compAssoc c _ _ _ _ _ _ _))
     )

             -- ((prod (π1 ; f1) (π2 ; f2)) ; (prod (π1 ; g1) (π2 ; g2))) ; π2
     (Calc $ |~ ((prod (pi1 |> f1) (pi2 |> f2)) |> (prod (pi1 |> g1) (pi2 |> g2))) |> pi2
             -- First we re-associate to get
             -- prod (π1 ; f1) (π2 ; f2) ; ((prod (π1 ; g1) (π2 ; g2)) ; π2)
             ~~ (prod (pi1 |> f1) (pi2 |> f2))
                |>
                ((prod (pi1 |> g1) (pi2 |> g2)) |> pi2)
                ... (sym (compAssoc c _ _ _ _ _ _ _))
             -- Then we use the fact that taking the product and projecting
             -- is the same a just using the second component
             -- prod (π1 ; f1) (π2 ; f2) ; (π2 ; g2)
             ~~ (prod (pi1 |> f1) (pi2 |> f2)
                |>
                (pi2 |> g2))
                ...(cong
                    ((prod (pi1 |> f1) (pi2 |> f2)) |>)
                    (prodRight _ _))
             -- now we re-associate again to get another `prod ; π2` pattern
             -- (prod (π1 ; f1) (π2 ; f2) ; π2) ; g2
             ~~ ((prod (pi1 |> f1) (pi2 |> f2) |> pi2) |> g2)
                ...(compAssoc c _ _ _ _ _ pi2 g2)
             -- Using the same property as before we eliminate our product
             -- (π2 ; f2) ; g2
             ~~ ((pi2 |> f2) |> g2)
                ...(cong (|> g2) (prodRight _ _))
             -- We re-associate one last time to conclude the proof
             -- π2 ; (f2 ; g2)
             ~~ pi2 |> (f2 |> g2)
                ...(sym (compAssoc c _ _ _ _ _ _ _))))

