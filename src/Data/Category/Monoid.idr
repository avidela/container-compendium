module Data.Category.Monoid

import Data.Category
import Data.Category.Bifunctor
import Data.Category.Iso
import Data.Category.Product
import Data.Category.NaturalTransformation


%hide Prelude.Ops.infixl.(*>)
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

%default total

||| A monoidal category
public export
record Monoidal {0 o : Type} (c : Category o) where
  constructor MkMonoidal

  mult : Bifunctor c c c

  I : Functor DiscreteCat c

  alpha : let f1, f2 : Functor (c * (c * c)) c
              f1 = ((idF _) `pair` mult) !*> mult
              f2 = assocR !*> ((mult `pair` idF _) !*> mult)
          in f1 =~= f2

  -- apply unit to the left
  leftUnitor :
      let leftAppliedMult : Functor c c
          leftAppliedMult = unitL !*> pair I (idF c) !*> mult
      in leftAppliedMult =~= idF c

  -- apply unit to the right
  rightUnitor :
      let rightAppliedMult : Functor c c
          rightAppliedMult = unitR !*> pair (idF c) I !*> mult
      in rightAppliedMult =~= idF c

public export
(×)  : {auto 0 cat : Category o} -> (mon : Monoidal cat) => o -> o -> o
(×) a b = mon.mult.mapObj (a && b)

public export
(⊗)  : {auto 0 cat : Category o} -> {x, y, a, b : o} -> (mon : Monoidal cat) =>
       x ~> y -> a ~> b -> x × a ~> y × b
(⊗) m1 m2 = mon.mult.mapHom (x && a) (y && b) (m1 && m2)

unit : {auto 0 cat : Category o} -> (mon : Monoidal cat) => o
unit = mon.I.mapObj ()

%unbound_implicits off
||| a monoid in a monoidal category
public export
record MonoidObject {0 o : Type} {cat : Category o} (mon : Monoidal cat) where
  constructor MkMonObj
  obj : o
  η : unit ~> obj
  mult : obj × obj ~> obj

  --            η ⊗ id
  --  unit × x ─────────> x ⊗ x
  --      │                │
  --      │                │ μ
  --      │                │
  --      │ λ              v
  --      └──────────────> x
  0 left : let 0 η_id_μ, λ : unit × obj ~> obj
               0 topMorphism : unit × obj ~> obj × obj
               topMorphism = η ⊗ cat.id obj
               η_id_μ = topMorphism |> mult
               λ = mon.leftUnitor.nat.component obj
            in η_id_μ === λ

  --            id × η
  --  x × unit ─────────> x × x
  --      │                │
  --      │                │ μ
  --      │                │
  --      │ ρ              v
  --      └──────────────> x
  0 right : let 0 id_η_μ, ρ : obj × unit  ~> obj
                0 topMorphism : obj × unit  ~> obj × obj
                topMorphism = cat.id obj ⊗ η
                id_η_μ = topMorphism |> mult
                ρ  = mon.rightUnitor.nat.component obj
             in id_η_μ === ρ
  --                 assoc
  --    (x × x) × x ───────> x × (x × x)
  --          │                  │
  --          │                  │  id × µ
  --          │                  v
  --   μ × id │                x × x
  --          │                  │
  --          │                  │ µ
  --          │                  v
  --        x × x ─────────────> x
  --                    µ
  0 assoc : let
                0 botLeft : (obj × obj) × obj ~> obj
                botLeft = mult ⊗ cat.id obj |> mult
                0 assoc : ((obj × obj) × obj) ~> obj × (obj × obj)
                assoc = mon.alpha.φ (obj && (obj && obj))
                0 topRight : ((obj × obj) × obj) ~> obj
                topRight = assoc |> cat.id obj ⊗ mult |> mult
            in botLeft === topRight


public export
record ComonoidObject {0 o : Type} {cat : Category o} (mon : Monoidal cat) where
  constructor MkCoMonObj
  obj : o
  counit : obj ~> unit
  comult : obj ~> obj × obj

  --            comult
  --      x ───────────> x × x
  --      │                │
  --      │                │ id × counit
  --      │                │
  --      │ λ⁻¹            v
  --      └────────────> unit × x
  0 left : let 0 rightMorphism : obj × obj ~> unit × obj
               rightMorphism = counit ⊗ cat.id obj
               0 comult_id_counit : obj ~> unit × obj
               comult_id_counit = comult |> rightMorphism
               0 λ⁻¹ : obj ~> unit × obj
               λ⁻¹ = mon.leftUnitor.φ  obj
            in comult_id_counit  === λ⁻¹

  --            comult
  --      x ───────────> x × x
  --      │                │
  --      │                │ counit × id
  --      │                │
  --      │ ρ⁻¹            v
  --      └────────────> x × unit
  0 right : let 0 comult_id_counit, ρ⁻¹ : obj ~> obj × unit
                0 rightMorphism : obj × obj ~> obj × unit
                rightMorphism = cat.id obj ⊗ counit
                comult_id_counit = comult |> rightMorphism
                ρ⁻¹ = mon.rightUnitor.φ obj
             in comult_id_counit  === ρ⁻¹

  --                 comult
  --          x ────────────> (x × x)
  --          │                  │
  --          │                  │  id ⊗ comult
  --          │                  v
  --   comult │              x × (x × x)
  --          │                  │
  --          │                  │ assoc
  --          │                  v
  --        x × x ────────> (x × x) × x
  --              comult ⊗ id
  0 assoc : let 0 topRight : obj ~> (obj × obj) × obj
                topRight = comult |> cat.id _ ⊗ comult |> mon.alpha.nat.component (obj && (obj && obj))
                0 botLeft : obj ~> (obj × obj) × obj
                botLeft = comult |> comult ⊗ cat.id _
            in topRight === botLeft
