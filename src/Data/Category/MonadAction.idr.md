### Monads From Actions

After studying both monads and actions, we can build the next result: how to
produce monadsa given a categorical action.

To produce a monad we need to give its functor and its two natural
transformations. For this, we first make precise what we have available before
we define the functor. We will need:

- A cateogory $\mathbb{D}$
- A monoidal category $\mathbb{C} (I ∈ C, e : Id(x) → C, ⊗ : C × C → C)$
- A monoid $M$ in $\mathbb{C}$
- An action $\rhd : \mathbb{C} \times \mathbb{D} \to \mathbb{D}, $

Using the above, we can define a functor $T : \mathbb{D} →\mathbb{D}$ by
partially applying our action to our monoid $M$, in other words, $T (x) = M \rhd
x$.

This allows us to write explicitly what the natural tranformations of a suitable
monad should be. The monadic unit $\eta : Id(x) ⇒ T(x)$ can be written as $Id(x)
⇒ M \rhd x$ and the monadic multiplication $μ : T (T(x)) ⇒ T(x)$ can be written as
$(M \rhd (M \rhd x)) ⇒ (M \rhd x)$.

Now that we know the type signature of the natural transformation it remains to
define them. $η$ is defined by composing the

With those two natural transformations comes a number of properties that we need
to prove. Each monad

```idris
module Data.Category.MonadAction

import Data.Category
import Data.Category.Action
import Data.Category.Monad
import Data.Category.Monoid
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.NaturalTransformation

%hide Prelude.Ops.infixl.(*>)

applyNT : {0 o1, o2, o3 : Type} -> {c1 : Category o1} -> {c2 : Category o2} -> {0 c3 : Category o3} ->
          {x, y : o1} -> (f : Bifunctor c1 c2 c3) ->
          (x ~> y) -> applyBifunctor {a = c1} x f ==>> applyBifunctor {a = c1, b = c2} y f
applyNT z f = MkNT (\vx => z.F' (x && vx) (y && vx) (f && c2.id vx) ) (\a, b, f => let qx = z.mapHom _ _ _ ?fn in ?ahsudia)


MonadFromAction : {0 o1, o2: Type} ->
    (cat1 : Category o1) ->
    (cat2 : Category o2) ->
    (act : LaxAction cat1 cat2) ->
    (m : MonoidObject {o=o1} {cat=cat1} {mon=act.mon}) ->
    Monad cat2
MonadFromAction cat1 cat2 act m = MkMonad func pure mult
  where
    %unbound_implicits off
    func : cat2 -*> cat2
    func = applyBifunctor m.obj act.action

    applyUnit : applyBifunctor {a = cat1} (act.mon.I.F ()) act.action ==>> func
    applyUnit = ?applyUnit_rhs

    pure : idF cat2 ==>> func
    pure = act.u !!> applyUnit

    f1 : (cat1 * (cat1 * cat2)) -*> cat2
    f1 = pair (idF cat1) act.action *> act.action

    mult : func *> func ==>> func
    mult = let vvv = act.m in ?bwuiebiw
```
