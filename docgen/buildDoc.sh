#!/bin/bash


MDNAME="markdown"
output_tex="output.tex"
intermediate_tex="concat.tex"

# Change behaviour if we are running in the command line
cli=false

# Initialize an array
filepaths=()

executables=("katla-pandoc" "xetex" "sed")

rm -rf $output_tex
rm -rf $MDNAME
rm -rf $intermediate_tex
rm -rf output.aux
rm -rf output.log
rm -rf output.out

# Loop through each executable and check if it exists
for cmd in "${executables[@]}"; do
  if ! command -v "$cmd" &> /dev/null; then
    echo "$cmd not found"
    exit 1;
  fi
done


for arg in "$@"; do
  case $arg in
    --cli)
      cli=true
      shift # Remove the argument from the list
      ;;
    *)
      file_name=$arg
      ;;
  esac
done

# Check if file_name is set (since it's required)
if [ -z "$file_name" ]; then
  echo "Usage: $0 [--cli] <file_name>"
  exit 1
fi

# Read file line by line into the array
while IFS= read -r line; do
    echo $line;
    filepaths+=("$line");
done < $file_name

cat "${filepaths[@]}" > $MDNAME

pandoc -f markdown+tex_math_dollars+fenced_code_attributes --lua-filter "tikz.lua" --filter "katla-pandoc" $MDNAME -o $intermediate_tex

cat begin $intermediate_tex end >> $output_tex

latexmk -pdfxe $output_tex

if [ "$cli" = false ]; then
    open output.pdf;
fi

echo word count:
./wordcount.sh $output_tex

# rm -rf $output_tex
rm -rf $MDNAME
rm -rf $intermediate_tex
rm -rf output.aux
rm -rf output.log
rm -rf output.out
rm -rf output.fdb_latexmk
rm -rf output.xdv
rm -rf output.toc
