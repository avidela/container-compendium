function CodeBlock(elem)
  -- Debug printing
  -- print("Classes:", pandoc.utils.stringify(pandoc.utils.type(elem.classes)))
  -- print("First class:", elem.classes[1])

  -- Print all attributes
  -- print("Attributes table contents:")
  -- for k, v in pairs(elem.attributes) do
  --   print(string.format("  %s: %s", k, v))
  -- end

  -- Print the raw attr table
  -- print("Raw attr table:")
  -- print(pandoc.utils.stringify(elem.attr))

  if elem.classes[1] == "tikz" then
    local content = elem.text

    -- Debug print the label and caption we're trying to access
    print("Attempting to access label:", elem.attributes.label)
    print("Attempting to access caption:", elem.attributes.caption)

    content = content:gsub("\\usepackage.-\n", "")
    content = content:gsub("\\begin{document}\n?", "")
    content = content:gsub("\\end{document}\n?", "")

    local label = elem.attributes.label or "fig:tikz-" .. pandoc.sha1(content):sub(1,8)
    local caption = elem.attributes.caption or ""

    -- Print what we ended up using
    print("Final label used:", label)
    print("Final caption used:", caption)

    local figure_content = string.format(
      "\\begin{figure}[htbp]\n\\centering\n%s\n\\caption{%s}\n\\label{%s}\n\\end{figure}",
      content,
      caption,
      label
    )

    return pandoc.RawBlock("latex", figure_content)
  end
end
