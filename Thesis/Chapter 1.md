# Container, Categories, and Theorem Proving
Before we delve into the depths of programming with containers, we 
need to set the scene with the decor that will permeate all aspects of this work: 
dependently types programming & Category Theory. First, I will talk about 
the programming environment I used, Idris, and explain what is special and
different about it. Then, I will quickly present some category theory in Idris,
those definitions will be reused throughout the thesis, so it's important we 
get them out early. Finally I will finally show definitions of containers in Idris and
some of their properties. 