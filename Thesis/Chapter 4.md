
## Abstract 



## Introduction

The study of containers spans many areas of computer science and type theory. We've already seen how to use them for APIs and design complex software out of smaller building blocks. This section will reuse all the tool and intuition developed until now but apply to a different domain: Theorem proving and tactics.

Theorem provers based on tactics provide a programming environment in which statements are proven given a series of steps that rewrite the initial statement into smaller ones. Those statements are called "goals" and a big part of the interaction model of a tactical theorem prover is based on rewriting the goals until the proof is trivial.

Theorem provers such as Coq/Rock and Isabelle feature tactics as their primary mode of interaction for writing proofs. The latter is particularly interesting since its history involves a core written in ML that verifies statements written using tactics. In that environment. Tactics break down a goal into a list of sub-goals, depending on the nature of the tactic, each sub-goal needs to be solved using other tactics, or maybe only one needs to be. Whatever the case, the solution for the sub-goals is then translated back into a solution for the original goal. 

## Containers as Claim, Morphisms as Tactics

The above infrastructure conspicuously looks like a _lens_, what is more, the fact that solutions need to match their original problem statements reinforces the idea that they are _dependent lenses_. The benefits of this observation is that we can now make the abstract notion of "theorem", "goal" and "subgoal" from ML precise.

Here, a goal is a problem statement, a theorem is a proof for that goal, or a _solution_. A sub-goal is merely the forward result of applying a lens to a problem to obtain a different problem.

To make this relation with tactics evident we show how we can use the category of containers ane their morphisms to encode the same structure as the LCF theorem prover. This encoding will already provide better semantics with regards to errors handling and expected return types, I'm addition to the LCF encoding, we provide even stronger types for a tactical system based on equations on natural numbers as well as a natural deduction system.

## LCF tactics 

Lcf tactics were first described in CITE in which we can find the following definition 

CODE


The above type signature does not make any claims about the relationship between the claim and its proof `thm`

Rewriting this expression a little bit we can see that it is isomorphic to our definition of plain lenses, and indexing the result of the computation with the input results in our definition of dependent lenses.

This definition suggests that containers represent a query-response pair, where the query, in the theorem proving jargon, is a proposition, and the response is a proof that the proposition holds.

