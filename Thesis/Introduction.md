# One of the Introduction of All Time

This thesis is about the data that sits at the boundary between programs, and
how to manipulate those boundaries to write programs.

Programming, in its most naive interpretation, is about communicating human
intent to a computer, in a way that other humans can understand, and in a way
that a computer can translate into basic instructions to run on a CPU.

This activity is guided by the programming language the user is employing, and
the domain for which the program is written. The programming language gives the tools to talk about abstract ideas in computing. For example, writing in assembly gives access to the notion of register, memory, bit vector, pointer, interrupts, syscalls, etc. Using those notions we can build programs for a specific domain. In the example of assembly, the domain is  CPU architecture.

The domain a language targets is both a blessing and a curse.
It makes writing programs in the intended domain intuitive and easy. But reading
a program written outside its intended domain creates unease and confusion.
Because of this, nowadays, we have very many programming languages to chose from,
and they all have their own goals as to what programming should look like and what
it is for. A language like Java eases the writing of programs using objects and
interfaces. A language like Haskell eases the writing of programs using
higher-order functions and typeclasses. A language like Idris eases the writing
of programs using first-order types and proofs. It is this last programming
language that I will be using in this thesis, to study this notion of "boundary"
between programs.

## The Idris Programming Language

Programming Language design has never been more exciting than today. We are
building language that solve problems that were with us since the dawn of programming
and they keep improving. Let's look at a few examples to better appreciate how
fortunate we are to witness such a prolific time in computer science research.

Rust for example, takes seriously the notion of ownership and
enable generating very efficient _and_ safe code by ensuring that data is not
arbitrarily aliased. Coupled with modern programming patterns such as traits,
higher-order functions, and algebraic data types, Rust offers a complete package for
modern systems-programming. Its large community ensures packages are maintained, bugs
are fixed, and support for editors is good.

Other programming contexts require to express \emph{what} a program does rather than \emph{how} it will
perform a task using pointers and bytes. In this context, languages like Unison, or Koka
provide the user with \emph{effect handlers}. Effects handlers enable the programmer to
select, restrict, or combine what effects a program is allowed to perform.
Writing programs always involve some notion of effect, for example writing a string to
a command line buffer, or reading some bytes from the network. 
Typically, programming languages
will put no restriction on the effects that one can perform. But Unison and Koka make
it possible to seamlessly combine multiple programs which effects are resctrited because
their effects they are expected to perform are explicitly marked. This can be used in
any sorts of way like only allowing some folders to be read or written. Only allowing
some parts of the program to access some secure enclave, or only
access the network in some environment.


The Idris programming language combines two things: First-class types, and linearity.
First-class types, or _dependent types_, give the programmer a way to treat types as values, write programs that manipulate types and return types from functions. This ability to compute types enables new avenues for programming that are only accessible via meta-programming in languages without it. For example, one can write a JSON serialiser/deserialiser library entirely without meta-programming and dynamically infer types and manipulate JSON objects safely, without relying on a template or a macro.

Linearity comes with its implementation of Quantitative Type Theory (QTT), a dependent type theory based around a semiring of resources. The resource semiring of Idris is a set of 3 elements: 0, 1 and $\omega$, representing runtime-erased, linear, and unrestricted resources, respectively

# Intro2

Programming intrface are large, nebulous concepts that permeate all layers for computer sofware. They often do not have any single specific and unique object identifying them, and instead, purely emerge from the implemetation of a programs. When they do have a proper represtentation
