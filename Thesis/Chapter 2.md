# Lenses, Optics and Container morphisms

Now that we know about containers and some operations on them, let
us study what are the morphisms in between containers and how they
relate to interactive systems.

A primitive form of container and morphism are lenses and their 
_boundaries_. I've decided to call the object of the category of lenses
_boundaries_ because, in this work, they are the boundaries around a
program, giving it means to interact with it via its API. In the industry
we often talk about "API Boundary" to indicate the limit at which one
program stops, and the next begins. 


