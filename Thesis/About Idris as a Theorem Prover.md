## Idris as a Theorem Prover

Idris is good at matching expectations with regards to the runtime performance of programs. The
Chez Scheme runtime is extremely fast, the implementation of QTT makes erasure reliable and easy to use, the strict
semantics of the code makes performance predictable in a way that matches the intuition of most commercial programming
languages. Most importantly, Idris has a fully featured package manager and a number of binding to important libraries to write 
software, like networking, user interfaces, web servers, databases and more.

Idris is _not_ well known for its theorem proving capabilities. It is, in the mind of the community members I've talked to, something
it _can_ do but is not _designed_ to do. I don't think I can pretend that I'm able to change the mind of people who came to this
conclusion, but I think I can shed light into what makes Idris a _different_ experience than Agda when proving theorems.

### Case statements

Much like Haskell, Idris has `case` statements that allow to pattern match on a value as part of an expression.
This has multiple benefits as it allows to match directly on argument on a lambda without having to lift an expression
to the top-level like would be required in Agda

*example of case*

This can be further simplified with the used of lambda-case

*example of lambda-case*
### With abstraction

With abstraction differs mostly in the ways it _does not_ work either in [Idris](https://github.com/idris-lang/Idris2/issues?q=is%3Aissue+is%3Aopen+with+label%3A%22language%3A+with%22) and [Agda](https://github.com/agda/agda/labels/with).
Both Idris and Agda suffer from the fact that with-abstraction generates an amount of syntax sugar that the compiler
has trouble matching back to surface level syntax, in a way that makes using `with`-abstraction in proofs brittle.

Idris suffers from the extra difficulty that `with` cannot handle multiplicity reliably, making it unsuitable for program
using [linear](https://github.com/idris-lang/Idris2/issues/331) binders. 

### Let-binding

Let bindings come in two forms in Idris: Lifted binding and inlined binding.

And inlined binding is what you would expect from most programming languages, it binds a value to a name and
the name can be reused multiple times without recomputing the value:

```
let x = 3 * 7*
in x + x
```

This however causes issues when writing proofs, because inlined binder are not evaluated in expressions, to 
be consistent with the runtime semantics of the program

*place example of proof that fails because of a let binder*

This is where lifted binding come up. To tell Idris that it's ok to evaluate a bound expression multiple times
one can write it in the same way they would write top-level definitions:

```
main = let x : Nat
           x = 3 * 7
       in printLn (x + x)
```

The `x` binder will now be lifted into a top-level definition and behave like a function being called multiple times.

```
x : Nat
x = 3 * 7

main = printLn (x + x)
```

This might not be what you want for runtime performance reasons but when you are writing proofs, then each
occurrence of `x` will be substituted by its implementation, enabling some proofs to evaluate down to simpler
statements. The fact that `x` is evaluated multiple times has no impact on runtime performance since most
proofs are erased from the runtime.

### Unicode and mixfix

Agda is notorious for its mixfix and unicode syntax. "Write-only" software is a meme often repeated to criticise the 
fact that Agda code is often illegible to newcomers. Idris aims to provide a much more familiar experience by
implementing a syntax that looks like Haskell, albeit with single colons for types rather than double colon.  Of course
this is not to say that Idris code is immediately legible to a newcomer either. Quantity annotations, rules around
case, implicit resolution and arbitrary overloading easily confuse users, even when they are accustomed to Haskell. Like all discussions about syntax, there is no winner here, only opinions. And in my efforts to keep code as
enjoyable to read as possible, I've taken the best of both languages to write the code that you will see here, and
used my own fork of Idris with the following changes:

- Support for binding syntax, it enables the use of syntax like `(x : a) * b x` rather than `a * (\x => b x)`
- Support for a limited subset of unicode operators, namely : `•×⊗⊕∈∋○≡¬⊃⊂√∫⨾▷◁≈≅`
- Change rules about capital letters to handle lower and upper-case greek letters.
- Namespaced fixity declarations

Binding operators and namespace fixities are changes that have made it to the main branch of Idris, and so you 
can use them yourself. Change involving unicode are directly inspired from Agda, and are going to remain private.
While bringing together the syntax of Idris and Agda has been helpful to me, I do not believe they are helping
 Idris become a more compelling programming language.

### Replace/Subst

The biggest difference for me, formalising containers and category theory, was that `replace` in idris is treated differently
than `subst` in Agda. 

```agda
-- agda
subst : ∀ {A : Set} {x y : A} (P : A → Set)
  → x ≡ y
  → P x → P y
subst P refl px = px
```

```idris
-- idris
replace : forall x, y, p . (0 rule : x = y) -> (1 _ : p x) -> p y
replace Refl prf = prf
```

`replace` states that 
given a proof `x = y` and a predicate `p`, any value `p x` can be converted into a value `p y`.  The same function is called
`subst` in Agda and performs the same functionality.

This makes some proofs much easier to interact with in Idris, since the layers of `replace` are hidden. On the flipside, 
once proofs become  more complex, hiding `replace` results in types that seem completely wrong, and do not
typecheck. It is not that they are actually wrong, it's that the have been applied to multiple instances of `replace` that
are now impossible to inspect, and therefore, writing a proof about those terms become exceedingly difficult, because
it involved rebuilding, in your mind, the multiple level of `replace` that were applied.

This problem does not exist in Agda since all `subst` are kept around, unless the proof it uses is matched upon. This make
proving simple statements sightly less approchable, but enable the ceiling of proof complexity to be much higher since
the user can _see_ what `substs` are required to be matched in order to make progress in the proof.

### Implicit arguments

Both languages feature implicit arguments but Idris only has named arguments, wheras Agda has both named and positional arguments.
What's more, Agda allows to bind implicit arguments in Lambda, something Idris can only do in top-level definitions.

This makes a number of things in Idris akward, like the fact that implicit arguments are automatically given to function in unapplied position when they shouldn't

*example of automatically applied argument*

But because there is no syntax for writing a lambda with an implicit argument, this cannot be resolved without writing a top-level definition, or by replacing implicit arguments by explicit ones.

### Conclusion

All those points make Idris a markedly different environment for theorem proving, although it still aims to be total, and free of proofs of void
the ultimate reality of research software is that it is only as good as the amount of resources poured into it. And unfortunately, Idris does
not benefit from any commercial or academic backing, and is only used and maintained by volunteers. Despite these challenges, the idris programming environement still strikes the right balance between easy-to-deploy software and theorem proving.