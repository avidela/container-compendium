# We all start somewhere

Dear reader,

I hope you are well, I am quite happy that you found this page and are interested in this work.

I've made this section to help you bring you up to speed with what you need to know and why.
Everything I have put here is there to help you, unfortunately, I cannot accomodate every possible
combination of circumstances, and therefore, there will be rough times and unpleasant parts.

If you encounter one of those rough patches where things do not make sense, or aren't clear
enough, it could be that the project needs some help and could be improved. In that case,
feel free to open an [issue](https://gitlab.com/avidela/container-compendium/-/issues).

It is possible that you also need more time to digest the material, or that you need more
hands-on experience. If you are stuck, I encourage you to look through other source
material. For this, I have compiled a source of [references](References.md) that I used
myself and found helpful.

# What is this and who is this for?

This is a compilation of my work on _containers_, a data structure with wonderful mathematical
properties. In particular I've worked on containers with the goal to design and implement
_interactive programs_. Because most programs in use nowadays are interactive, if you are
a software developer, computer scientist or engineering student, there is a good chance you
will find something interesting.

However, those categorisation can span a large range of backgrounds and skills, and because
of that, they might not be as helpful as one can hope. Instead, I think it is more useful
to describe the target for this book as having two of the following characteristics:

- Has written software before, wants to write better software and has heard about functional programming before.
- Is interested in understanding the ins-and-out of programs.
- Has heard about "polynomial functors" and wants to see some applications.
- Is familiar with abstract algebra but for whom category theory is one step too far.

The first two are aimed at curious programmers interested in knowing more about the
fundamentals of programs. The last two are for mathematicians interested in applications
of ideas they've heard before but never experienced.
